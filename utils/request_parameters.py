"""
Utils functions for the requests parameters
"""


def sanitise_parameter(param_value):
    """
    Makes the parameter null if it is 'null' or 'undefined', in some cases javascript produces those values
    :param param_value: value of the parameter
    :return: null if param_value in ('null', 'undefined'), the actual value otherwise
    """
    if param_value in ('null', 'undefined'):
        return None
    return param_value
