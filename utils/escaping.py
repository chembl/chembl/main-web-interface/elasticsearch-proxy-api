"""
Module with helper functions to fix escaping bugs out of our control
"""


def unsanitise_slash(raw_text):
    """
    Nginx escapes unescapes the / and this causes errors with the urls. So the frontend replaces / with __SLASH__
    this function undoes that
    :param raw_text: text to unsanitise
    :return: text with / instead of __SLASH__
    """
    return raw_text.replace('__SLASH__', '/')
