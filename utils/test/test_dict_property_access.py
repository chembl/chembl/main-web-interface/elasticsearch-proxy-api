"""
Module to test the dict property access
"""
import unittest

from utils import dict_property_access


class TestDictPropertyAccess(unittest.TestCase):
    """
    Class to test the property access module
    """

    def test_gets_simple_property(self):
        """
        Tests that it gets a simple property
        """
        value_must_be = 'hello'
        prop_path = 'prop1'

        the_dict = {
            'prop1': value_must_be
        }

        value_got = dict_property_access.get_property_value(the_dict, prop_path)

        self.assertEqual(value_must_be, value_got, msg=f'The value of {prop_path} was not obtained correctly!')

    def test_gets_nested_property(self):
        """
        Tests that it gets a nested property
        """
        value_must_be = 'hello'
        prop_path = 'prop1.prop2.prop3'

        the_dict = {
            'prop1': {
                'prop2': {
                    'prop3': value_must_be
                }
            }
        }

        value_got = dict_property_access.get_property_value(the_dict, prop_path)

        self.assertEqual(value_must_be, value_got, msg=f'The value of {prop_path} was not obtained correctly!')

    def test_gets_property_in_array(self):
        """
        Tests that it gets a nested property
        """

        value_must_be = [1, 2, 3]
        prop_path = 'prop1.prop2.prop3.prop4'

        the_dict = {
            'prop1': {
                'prop2': {
                    'prop3': [
                        {'prop4': 1},
                        {'prop4': 2},
                        {'prop4': 3}
                    ]

                }
            }
        }

        value_got = dict_property_access.get_property_value(the_dict, prop_path)

        self.assertEqual(value_must_be, value_got, msg=f'The value of {prop_path} was not obtained correctly!')
