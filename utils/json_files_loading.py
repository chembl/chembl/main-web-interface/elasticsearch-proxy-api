"""
Module with utils for json files
"""
import json


def load_json_from_path(file_path):
    """
    Loads the json contained in the path indicated as parameter
    :param file_path: path to load
    :return: dict with the loaded content
    """
    with open(file_path) as json_file:
        json_dict = json.load(json_file)
        return json_dict
