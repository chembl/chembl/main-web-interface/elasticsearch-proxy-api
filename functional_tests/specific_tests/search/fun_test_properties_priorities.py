# pylint: disable=import-error
"""
Functional tests for the properties priorities
"""

from specific_tests import utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests the properties priorities give a response
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('Starting properties priorities tests...')
    print(delayed_jobs_server_base_path)

    url = f'{server_base_url}/search/properties_priorities/Compound'
    utils.assert_get_request_succeeds(url)
