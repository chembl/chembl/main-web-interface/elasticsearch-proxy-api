# pylint: disable=import-error
"""
Functional tests for the chembl autocomplete
"""

from specific_tests import utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests the autocomplete gives a response
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('Starting chembl autocomplete tests...')
    print(delayed_jobs_server_base_path)

    url = f'{server_base_url}/search/autocomplete/Asp'
    utils.assert_get_request_succeeds(url)
