# pylint: disable=import-error
"""
Module that tests an es_query with context
"""
import json
import os

from specific_tests import utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests that a simple query to elasticsearch
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('-------------------------------------------')
    print('Testing a es_query with direct similarity search context')
    print('-------------------------------------------')

    print('server_base_url: ', server_base_url)
    print('delayed_jobs_server_base_path: ', delayed_jobs_server_base_path)

    smiles = 'COc1cc2c(c(OC)c1OC)-c1ccc(Br)c(=O)cc1[C@@H](NC(C)=O)CC2'
    similarity_threshold = 70

    context_obj = {
        "context_type": "DIRECT_SIMILARITY",
        "context_id": smiles,
        "web_services_base_url": os.environ.get("FUN_TESTS_WS_SERVER_BASE_PATH"),
        'similarity_threshold': f'{similarity_threshold}'
    }

    es_query = {
        "size": 24,
        "from": 0,
        "query": {
            "bool": {
                "must": [{"query_string": {"analyze_wildcard": 'true', "query": "*"}}],
                "filter": []
            }
        },
        "sort": []
    }

    payload = {
        'index_name': 'chembl_molecule',
        'es_query': json.dumps(es_query),
        'context_obj': json.dumps(context_obj),
        'contextual_sort_data': {},
        'is_test': True,
    }

    num_items_must_be = utils.get_num_items_in_ws_similarity_search_results(smiles, similarity_threshold)
    utils.assert_es_data_request_succeeds(payload, num_items_must_be)
