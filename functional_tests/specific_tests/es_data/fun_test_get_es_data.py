# pylint: disable=import-error,unused-argument
"""
Module that tests getting data from elasticsearch
"""

from specific_tests import utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests that getting the data of a context works
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('-------------------------------------------')
    print('Testing getting the data from elasticsearch')
    print('-------------------------------------------')

    url = f'{server_base_url}/es_data/get_es_data/ewogICJpbmRleF9uYW1lIjogImNoZW1ibF9ldWJvcGVuX21vbGVjdWxlIiwKICAiZX' \
          f'NfcXVlcnkiOiB7CiAgICAicXVlcnkiOiB7CiAgICAgICJib29sIjogewogICAgICAgICJmaWx0ZXIiOiBbXSwKICAgICAgICAic2hvdW' \
          f'xkIjogW10sCiAgICAgICAgIm11c3Rfbm90IjogW10KICAgICAgfQogICAgfSwKICAgICJ0cmFja190b3RhbF9oaXRzIjogdHJ1ZSwKIC' \
          f'AgICJzaXplIjogMjAsCiAgICAiZnJvbSI6IDAsCiAgICAiX3NvdXJjZSI6IFsKICAgICAgIm1vbGVjdWxlX2V1Ym9wZW5faWQiLAogIC' \
          f'AgICAicHJlZl9uYW1lIiwKICAgICAgIm1vbGVjdWxlX3N5bm9ueW1zIiwKICAgICAgIl9tZXRhZGF0YS5ldWJvcGVuLmluX3Zpdm9fdX' \
          f'NlIiwKICAgICAgIm1vbGVjdWxlX2NsYXNzIiwKICAgICAgIm1vbGVjdWxlX3N0cnVjdHVyZXMuY2Fub25pY2FsX3NtaWxlcyIsCiAgIC' \
          f'AgICJtb2xlY3VsZV9zdHJ1Y3R1cmVzLnN0YW5kYXJkX2luY2hpX2tleSIsCiAgICAgICJfbWV0YWRhdGEuZXVib3Blbi5pc19wcm9iZS' \
          f'IsCiAgICAgICJfbWV0YWRhdGEuZXVib3Blbi5pc19jb250cm9sIiwKICAgICAgIl9tZXRhZGF0YS5ldWJvcGVuLmlzX2NoZW1vZ2Vub2' \
          f'1pY19wcm9iZSIKICAgIF0sCiAgICAic29ydCI6IFtdCiAgfQp9'
    utils.assert_get_request_succeeds(url)
