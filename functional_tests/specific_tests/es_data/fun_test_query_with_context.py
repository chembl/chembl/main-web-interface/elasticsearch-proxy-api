# pylint: disable=import-error
"""
Module that tests an es_query with context
"""
import json

from specific_tests import utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests that a simple query to elasticsearch
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('-------------------------------------------')
    print('Testing a es_query with context')
    print('-------------------------------------------')

    print('server_base_url: ', server_base_url)
    print('delayed_jobs_server_base_path: ', delayed_jobs_server_base_path)

    job_id = utils.submit_similarity_search_job(delayed_jobs_server_base_path)
    utils.wait_until_job_finished(delayed_jobs_server_base_path, job_id)
    print('Now going to make a request with this context')

    es_query = {
        "size": 24,
        "from": 0,
        "track_total_hits": True,
        "query": {
            "bool": {
                "must": [{"query_string": {"analyze_wildcard": 'true', "query": "*"}}],
                "filter": []
            }
        },
        "sort": []
    }

    context_obj = {
        "delayed_jobs_base_url": delayed_jobs_server_base_path,
        "context_type": "SIMILARITY",
        "context_id": job_id,
    }

    payload = {
        'index_name': 'chembl_molecule',
        'es_query': json.dumps(es_query),
        'context_obj': json.dumps(context_obj),
        'contextual_sort_data': {},
        'is_test': True,
    }

    num_items_must_be = utils.get_num_items_in_results_for_structure_search_job(job_id)
    utils.assert_es_data_request_succeeds(payload, num_items_must_be)
