# pylint: disable=import-error,unused-argument
"""
Module that tests a group config
"""
from specific_tests import utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests getting the configuration of a group
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('-------------------------------------------')
    print('Testing getting a document by id')
    print('-------------------------------------------')

    url = f'{server_base_url}/es_data/get_es_document/chembl_eubopen_target/CHEMBL4524126'

    params = {
        'source': 'pref_name',
        'custom_id_property': 'target_chembl_id'
    }

    utils.assert_get_request_succeeds(url, params)
