# pylint: disable=import-error
"""
Functional tests for the heatmap cols headers
"""
import base64
import json

from specific_tests import utils
from specific_tests.heatmap import shared


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests doing a join among different entities selecting all ids
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('Starting heatmap tests...')
    print(delayed_jobs_server_base_path)

    raw_descriptor = {
        'xAxis': {
            'entityID': 'EubopenCompound',
            'initialQuery': {}
        },
        'yAxis': {
            'entityID': 'EubopenTarget',
            'initialQuery': {},
        },
    }

    descriptor = base64.b64encode(json.dumps(raw_descriptor).encode()).decode().replace('+', '-').replace('/', '_')

    url = f'{server_base_url}/visualisations/heatmap/{descriptor}/x_axis/summary'
    utils.assert_get_request_succeeds(url)

    print('now trying with a heatmap id')
    heatmap_id = shared.get_heatmap_id(server_base_url, raw_descriptor)
    url = f'{server_base_url}/visualisations/heatmap/HEATMAP_ID:{heatmap_id}/x_axis/summary'
    print('url: ', url)
    utils.assert_get_request_succeeds(url)
