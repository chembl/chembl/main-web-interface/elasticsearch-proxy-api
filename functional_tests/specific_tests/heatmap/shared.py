# pylint: disable=import-error
"""
Shared functions for heatmap functional tests
"""
import base64
import json

from specific_tests import utils


def get_heatmap_id(server_base_url, raw_descriptor):
    """
    :paran server_base_url: base of the url of the server being used
    :param raw_descriptor: dict description of the heatmap for which to get the id
    :return: the id corresponding tho the description
    """

    descriptor = base64.b64encode(json.dumps(raw_descriptor).encode()).decode().replace('+', '-').replace('/', '_')
    id_url = f'{server_base_url}/visualisations/heatmap/get_id/{descriptor}'
    the_request = utils.assert_get_request_succeeds(id_url)

    response = the_request.json()

    heatmap_id = response['hash']
    return heatmap_id
