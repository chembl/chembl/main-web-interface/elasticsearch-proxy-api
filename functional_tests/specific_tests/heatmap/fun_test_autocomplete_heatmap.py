# pylint: disable=import-error
"""
Functional tests for the heatmap autocomplete
"""

from specific_tests import utils
from specific_tests.heatmap import shared


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests doing the autocomplete performed on the heatmap
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('Starting heatmap tests...')
    print(delayed_jobs_server_base_path)

    raw_descriptor = {
        "cells": {
            "entityID": "Activity",
            "properties": [
                "num_activities",
                "fake_property"
            ],
            "properties_links_types": {
                "num_activities": "activities_from_compounds_and_targets"
            }
        },
        "xAxis": {
            "entityID": "Compound",
            "footers": {
                "label_properties": [
                    "molecule_type",
                    "molecule_properties.mw_freebase"
                ]
            },
            "headers": {
                "additional_properties": [
                    "molecule_chembl_id"
                ],
                "label_property": "pref_name",
                "mini_report_card_properties": [
                    "molecule_chembl_id",
                    "pref_name",
                    "molecule_synonyms",
                    "molecule_type",
                    "max_phase",
                    "molecule_properties.full_mwt"
                ]
            },
            "initialQuery": {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "query_string": {
                                    "query": "_metadata.related_targets.all_chembl_ids:(\"CHEMBL612411\" "
                                             "OR \"CHEMBL2367287\")"
                                }
                            }
                        ]
                    }
                }
            }
        },
        "yAxis": {
            "entityID": "Target",
            "footers": {
                "label_properties": [
                    "organism",
                    "target_type"
                ]
            },
            "headers": {
                "additional_properties": [
                    "target_chembl_id"
                ],
                "label_property": "pref_name",
                "mini_report_card_properties": [
                    "target_chembl_id",
                    "pref_name",
                    "target_synonyms",
                    "target_type",
                    "organism"
                ]
            },
            "initialQuery": {
                "query": {
                    "bool": {
                        "filter": [
                            {
                                "ids": {
                                    "values": [
                                        "CHEMBL2367287",
                                        "CHEMBL612411"
                                    ]
                                }
                            }
                        ],
                        "must_not": [],
                        "should": []
                    }
                },
                "sort": [],
                "track_total_hits": True
            }
        }

    }

    heatmap_id = shared.get_heatmap_id(server_base_url, raw_descriptor)
    url = f'{server_base_url}/visualisations/heatmap/HEATMAP_ID:{heatmap_id}/autocomplete_in_axis/x_axis/pendi'
    print('url: ', url)
    utils.assert_get_request_succeeds(url)
