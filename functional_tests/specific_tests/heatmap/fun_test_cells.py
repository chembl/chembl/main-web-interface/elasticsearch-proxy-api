# pylint: disable=import-error
"""
Functional tests for the heatmap cells
"""
import base64
import json

from specific_tests import utils
from specific_tests.heatmap import shared


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests doing a join among different entities selecting all ids
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('Starting heatmap tests...')
    print(delayed_jobs_server_base_path)

    raw_descriptor = {
        'xAxis': {
            'entityID': 'EubopenCompound',
            'initialQuery': {
                "sort": [{"_metadata.related_targets.count": "desc"}]
            },
            'headers': {
                'label_property': 'pref_name'
            }
        },
        'yAxis': {
            'entityID': 'EubopenTarget',
            'initialQuery': {
                # "sort": [{"_metadata.related_compounds.count": "desc"}]
            },
            'headers': {
                'label_property': 'pref_name'
            }
        },
        'cells': {
            'entityID': 'Activity',
            'properties': ['num_activities', 'fake_property'],
        },
    }

    descriptor = base64.b64encode(json.dumps(raw_descriptor).encode()).decode().replace('+', '-').replace('/', '_')
    print('descriptor: ', descriptor)
    x_items_from = 0
    x_size = 20

    y_items_from = 0
    y_size = 20

    url = f'{server_base_url}/visualisations/heatmap/{descriptor}/cells/{x_items_from}/{x_size}/{y_items_from}/{y_size}'
    print('url: ', url)
    utils.assert_get_request_succeeds(url)

    print('now trying with a heatmap id')
    heatmap_id = shared.get_heatmap_id(server_base_url, raw_descriptor)

    url = f'{server_base_url}/visualisations/heatmap/HEATMAP_ID:{heatmap_id}/cells/' \
          f'{x_items_from}/{x_size}/{y_items_from}/{y_size}'
    print('url: ', url)
    utils.assert_get_request_succeeds(url)
