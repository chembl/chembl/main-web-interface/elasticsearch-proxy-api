# pylint: disable=import-error
"""
Module with utils for entities join tests
"""
import json
import requests

from specific_tests import utils


def test_entities_join(dataset_query, selection_description, server_base_url, entity_from, entity_to, context_obj=None):
    """
    Performs the testing of the entities join endpoint given the parameters
    :param dataset_query: query of the current dataset
    :param selection_description: dict describing the selection
    :param server_base_url: base url of the server being used for the tests
    :param entity_from: entity from which to do the join
    :param entity_to: destination entity of the join
    :param context_obj: context object to be included in the join request
    """

    join_params = {
        'destination_entity_browser_state_template':
            'wwwdev.ebi.ac.uk/chembl/g/#browse/<BROWSER_NAME>/full_state/<GENERATED_STATE>',
        'entity_from': entity_from,
        'entity_to': entity_to,
        'es_query': json.dumps(dataset_query),
        'selection_description': json.dumps(selection_description),
        'context_obj': json.dumps(context_obj),
        'is_test': True,
    }

    # Test that the join gives a valid response
    url = f'{server_base_url}/entities_join/get_link_to_related_items'
    response = utils.assert_post_request_succeeds(url, join_params)

    url_hash_got = response['tiny_hash']

    expansion_url = f'{server_base_url}/url_shortening/expand_url/{url_hash_got}'
    request = requests.get(expansion_url)

    status_code = request.status_code
    response_text = request.text
    utils.print_es_response(response_text)
    assert status_code == 200, 'The request failed!'


def test_entities_join_query(dataset_query, selection_description, server_base_url, entity_from, entity_to,
                             context_obj=None):
    """
    Performs the testing of the entities that generate the query for the join given the parameters
    :param dataset_query: query of the current dataset
    :param selection_description: dict describing the selection
    :param server_base_url: base url of the server being used for the tests
    :param entity_from: entity from which to do the join
    :param entity_to: destination entity of the join
    :param context_obj: context object to be included in the join request
    """

    join_params = {
        'entity_from': entity_from,
        'entity_to': entity_to,
        'es_query': json.dumps(dataset_query),
        'selection_description': json.dumps(selection_description),
        'context_obj': json.dumps(context_obj),
        'is_test': True,
    }

    # Test that the join gives a valid response
    url = f'{server_base_url}/entities_join/get_query_to_related_items'
    utils.assert_post_request_succeeds(url, join_params)
