# pylint: disable=import-error
"""
Module that runs all the tests related to the entities join
"""
from specific_tests import utils
from specific_tests.entities_join import utils as entities_join_utils

TESTS_TO_RUN = [
    {
        'entity_from': 'CHEMBL_DRUG_WARNINGS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_0.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_WARNINGS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_0.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": ['CHEMBL2107495', 'CHEMBL340978']}
    },
    {
        'entity_from': 'CHEMBL_DRUG_WARNINGS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_0.json',
        'selection_description': {"selectionMode": "noItemsExcept", "exceptions": ['CHEMBL2107495', 'CHEMBL340978']}
    },
    {
        'entity_from': 'CHEMBL_DRUG_WARNINGS',
        'entity_to': 'CHEMBL_COMPOUNDS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_0.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_WARNINGS',
        'entity_to': 'CHEMBL_DRUGS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_0.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_WARNINGS',
        'entity_to': 'CHEMBL_DRUG_INDICATIONS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_0.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_WARNINGS',
        'entity_to': 'CHEMBL_DRUG_MECHANISMS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_0.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_COMPOUNDS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_1.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_COMPOUNDS',
        'entity_to': 'CHEMBL_DRUGS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_1.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_COMPOUNDS',
        'entity_to': 'CHEMBL_DRUG_MECHANISMS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_1.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_COMPOUNDS',
        'entity_to': 'CHEMBL_DRUG_INDICATIONS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_1.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUGS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_2.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUGS',
        'entity_to': 'CHEMBL_DRUG_MECHANISMS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_2.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUGS',
        'entity_to': 'CHEMBL_DRUG_INDICATIONS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_2.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_MECHANISMS',
        'entity_to': 'CHEMBL_DRUGS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_3.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_MECHANISMS',
        'entity_to': 'CHEMBL_COMPOUNDS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_3.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_MECHANISMS',
        'entity_to': 'CHEMBL_TARGETS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_3.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_INDICATIONS',
        'entity_to': 'CHEMBL_DRUGS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_4.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DRUG_INDICATIONS',
        'entity_to': 'CHEMBL_COMPOUNDS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_4.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_ACTIVITIES',
        'entity_to': 'CHEMBL_COMPOUNDS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_5.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_ACTIVITIES',
        'entity_to': 'CHEMBL_TARGETS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_5.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_ACTIVITIES',
        'entity_to': 'CHEMBL_ASSAYS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_5.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_ACTIVITIES',
        'entity_to': 'CHEMBL_DOCUMENTS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_5.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_TARGETS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_6.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_TARGETS',
        'entity_to': 'CHEMBL_DRUG_MECHANISMS',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_6.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_ASSAYS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_7.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_DOCUMENTS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_8.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_CELL_LINES',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_9.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
    {
        'entity_from': 'CHEMBL_TISSUES',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_10.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}
    },
]


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests doing a join among different entities selecting all ids
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('Starting entities join tests...')
    print(delayed_jobs_server_base_path)
    for test_description in TESTS_TO_RUN:
        dataset_query = utils.load_json_data(test_description['dataset_query_path'])
        selection_description = test_description['selection_description']
        entity_from = test_description['entity_from']
        entity_to = test_description['entity_to']

        entities_join_utils.test_entities_join(dataset_query, selection_description, server_base_url,
                                               entity_from=entity_from, entity_to=entity_to)

        entities_join_utils.test_entities_join_query(dataset_query, selection_description, server_base_url,
                                                     entity_from=entity_from, entity_to=entity_to)

    print(f'Ran {len(TESTS_TO_RUN)} tests of entities join successfully')
