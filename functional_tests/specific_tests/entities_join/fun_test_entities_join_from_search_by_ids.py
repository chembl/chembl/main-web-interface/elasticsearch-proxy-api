# pylint: disable=import-error
"""
Module that runs the tests related to entities join with a search by ids context
"""
from specific_tests import utils
from specific_tests.entities_join import utils as entities_join_utils

TESTS_TO_RUN = [
    {
        'search_by_ids': {
            'from': 'MOLECULE_CHEMBL_IDS',
            'to': 'CHEMBL_COMPOUNDS',
            'separator': '__AUTO__',
            'raw_items_ids': 'CHEMBL1905569,CHEMBL3659411,CHEMBL4104861,CHEMBL1502197,CHEMBL3931303,CHEMBL466111,'
                             'CHEMBL323959,CHEMBL1255073,CHEMBL4288214,CHEMBL3480964',
            'dl__ignore_cache': True
        },
        'entities_join': {
            'entity_from': 'CHEMBL_COMPOUNDS',
            'entity_to': 'CHEMBL_ACTIVITIES',
            'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_11.json',
            'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}

        }
    }
]


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests doing a join among different entities  with a search by ids context
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('Starting entities join tests from a search by ids context...')

    for test_description in TESTS_TO_RUN:
        search_by_ids_params = test_description['search_by_ids']
        print('search_by_ids_params: ', search_by_ids_params)
        job_id = utils.submit_search_by_ids_job(delayed_jobs_server_base_path, search_by_ids_params)
        utils.wait_until_job_finished(delayed_jobs_server_base_path, job_id)

        entities_joins_params = test_description['entities_join']
        dataset_query = utils.load_json_data(entities_joins_params['dataset_query_path'])
        print('dataset_query: ', dataset_query)

        context_obj = {
            "context_type": "TO_COMPOUNDS",  # When the search is to targets it must be TO_TARGETS
            "context_id": job_id,
            "delayed_jobs_base_url": delayed_jobs_server_base_path
        }

        entities_join_utils.test_entities_join(dataset_query, entities_joins_params['selection_description'],
                                               server_base_url,
                                               entity_from=entities_joins_params['entity_from'],
                                               entity_to=entities_joins_params['entity_to'], context_obj=context_obj)
