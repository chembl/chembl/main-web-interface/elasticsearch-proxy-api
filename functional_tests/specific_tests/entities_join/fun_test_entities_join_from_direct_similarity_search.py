# pylint: disable=import-error
"""
Module that runs the tests related to entities join with a direct similarity search context
"""
import os

from specific_tests import utils
from specific_tests.entities_join import utils as entities_join_utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests doing a join among different entities with a direct similarity search context
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """
    print('Starting entities join tests from a direct similarity search context...')

    entities_joins_params = {
        'entity_from': 'CHEMBL_COMPOUNDS',
        'entity_to': 'CHEMBL_ACTIVITIES',
        'dataset_query_path': 'functional_tests/specific_tests/test_data/entities_join_query_11.json',
        'selection_description': {"selectionMode": "allItemsExcept", "exceptions": []}

    }

    dataset_query = utils.load_json_data(entities_joins_params['dataset_query_path'])
    print('dataset_query: ', dataset_query)

    smiles = 'COc1cc2c(c(OC)c1OC)-c1ccc(Br)c(=O)cc1[C@@H](NC(C)=O)CC2'
    similarity_threshold = 70

    context_obj = {
        "context_type": "DIRECT_SIMILARITY",  # When the search is to targets it must be TO_TARGETS
        "context_id": smiles,
        "delayed_jobs_base_url": delayed_jobs_server_base_path,
        "web_services_base_url": os.environ.get("FUN_TESTS_WS_SERVER_BASE_PATH"),
        'similarity_threshold': f'{similarity_threshold}'
    }

    entities_join_utils.test_entities_join(dataset_query, entities_joins_params['selection_description'],
                                           server_base_url,
                                           entity_from=entities_joins_params['entity_from'],
                                           entity_to=entities_joins_params['entity_to'], context_obj=context_obj)
