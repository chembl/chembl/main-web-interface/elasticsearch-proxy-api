"""
Module with utils functions for the tests
"""
import json
import time
import sys
import os
import urllib.parse

import requests


def get_url_for_get_es_data(server_base_url=None):
    """
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :return: url for the get_es_data_endpoint
    """
    if server_base_url is None:
        server_base_url = os.environ.get("FUN_TESTS_SERVER_BASE_PATH")
    return f'{server_base_url}/es_data/get_es_data'


def get_url_for_job_status(delayed_jobs_base_url, job_id):
    """
    :param delayed_jobs_base_url: base url for the delayed jobs.
    E.g. https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs
    :param job_id: job_id
    :return: url for getting a job status
    """
    if delayed_jobs_base_url is None:
        delayed_jobs_base_url = os.environ.get("FUN_TESTS_DJ_SERVER_BASE_PATH")
    return f'{delayed_jobs_base_url}/status/{job_id}'


def get_url_for_similarity_job_submission(delayed_jobs_base_url):
    """
    :param delayed_jobs_base_url: base url for the delayed jobs.
    E.g. https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs
    :return: url for submitting a similarity search job
    """
    return f'{delayed_jobs_base_url}/submit/structure_search_job'


def get_url_for_search_by_ids_job_submission(delayed_jobs_base_url):
    """
    :param delayed_jobs_base_url: base url for the delayed jobs.
    E.g. https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs
    :return: url for submitting a search by ids job
    """
    return f'{delayed_jobs_base_url}/submit/search_by_ids_job'


def get_url_for_separator_identifier(server_base_url):
    """
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :return: url for the identify_separator
    """
    return f'{server_base_url}/utils/identify_separator'


def print_es_response(response_text, max_chars=200):
    """
    prints the response text passed as parameter up to max_chars
    :param response_text: response to print
    :param max_chars: max chars to print
    """
    print('response_text:')

    too_long = len(response_text) > max_chars

    if too_long:
        print(f'{response_text[0:max_chars]}...')
    else:
        print(response_text)


def assert_get_request_succeeds(url_to_test, params=None):
    """
    tests that doing a get to the url returns a 200 code
    :param url_to_test: url to test
    :param params: url params to include
    """

    print('url: ', url_to_test)
    print('params: ', params)

    the_request = requests.get(url_to_test, params=params)
    print('full url: ', the_request.request.url)

    status_code = the_request.status_code
    print(f'status_code: {status_code}')

    response_text = the_request.text
    print_es_response(response_text)
    assert status_code == 200, 'The request failed!'

    return the_request


def assert_post_request_succeeds(url_to_test, payload=None):
    """
    tests that doing a get to the url returns a 200 code
    :param url_to_test: url to test
    :param payload: payload to include
    :return: the json response of the request
    """

    request = requests.post(url_to_test, data=payload)

    status_code = request.status_code
    response_text = request.text
    print_es_response(response_text)
    assert status_code == 200, 'The request failed!'

    return request.json()


def assert_es_data_request_succeeds(payload, num_items_must_be):
    """
    Tests that a request to the ES data succeeds and returns the amount of hits indicated
    :param payload: payload to send for the request
    :param num_items_must_be: number of items that the responde must have
    """
    url = get_url_for_get_es_data()

    print('assert_es_data_request_succeeds')
    print('url: ', url)
    print('payload: ', payload)

    response_json = assert_post_request_succeeds(url, payload)
    num_hits_got = response_json['es_response']['hits']['total']['value']
    print('num_hits_got: ', num_hits_got)
    print('num_items_must_be: ', num_items_must_be)
    assert num_hits_got == num_items_must_be, 'The number of item is not what it must be!'
    print('---')


def get_num_items_in_results_for_structure_search_job(job_id):
    """
    :param job_id: id of the job
    :return: number of items in the results of the job
    """
    status_url = get_url_for_job_status(delayed_jobs_base_url=None, job_id=job_id)

    job_status_dict = requests.get(status_url).json()
    results_file_url = f"http://{job_status_dict['output_files_urls']['results.json']}"
    print('results_file_url: ', results_file_url)

    num_results = len(requests.get(results_file_url).json()['search_results'])
    print('num_results: ', num_results)

    return num_results


def get_num_items_in_ws_similarity_search_results(smiles, threshold):
    """
    :param smiles: smiles for which to search similarity
    :param threshold: similarity threshold
    :return: the number of items
    """
    similarity_search_url = f'{os.environ.get("FUN_TESTS_WS_SERVER_BASE_PATH")}/similarity/' \
                            f'{urllib.parse.quote(smiles)}/{threshold}.json?only=molecule_chembl_id'
    print('similarity_search_url: ', similarity_search_url)
    results_dict = requests.get(similarity_search_url).json()
    num_results = results_dict['page_meta']['total_count']
    print('num_results: ', num_results)
    return num_results


def submit_similarity_search_job(delayed_jobs_server_base_path):
    """
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :return: job_id
    """
    print('Launching a similarity search job...')
    submission_url = get_url_for_similarity_job_submission(delayed_jobs_server_base_path)
    print('submission_url: ', submission_url)

    payload = {
        'search_type': 'SIMILARITY',
        'search_term': 'NCCc1ccc(O)c(O)c1',
        'threshold': 40,
        'dl__ignore_cache': True
    }
    print('payload: ', payload)

    submit_request = requests.post(submission_url, data=payload)
    submission_status_code = submit_request.status_code
    print(f'submission_status_code: {submission_status_code}')
    assert submission_status_code == 200, 'Job could not be submitted!'

    submission_response = submit_request.json()
    job_id = submission_response.get('job_id')
    print('job_id: ', job_id)

    return job_id


def submit_search_by_ids_job(delayed_jobs_server_base_path, custom_params=None):
    """
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :param custom_params: custom parameters for the job if not specified, some default parameters will be used.
    :return: job_id
    """
    print('Launching a search by ids job...')
    submission_url = get_url_for_search_by_ids_job_submission(delayed_jobs_server_base_path)
    print('submission_url: ', submission_url)

    if custom_params is not None:
        payload = custom_params
    else:
        payload = {
            'from': 'MOLECULE_CHEMBL_IDS',
            'to': 'CHEMBL_COMPOUNDS',
            'separator': ',',
            'raw_items_ids': 'CHEMBL27193,CHEMBL4068896,CHEMBL332148,CHEMBL2431212,CHEMBL4303667',
            'dl__ignore_cache': True
        }
    print('payload: ', payload)

    submit_request = requests.post(submission_url, data=payload)
    submission_status_code = submit_request.status_code
    print(f'submission_status_code: {submission_status_code}')
    assert submission_status_code == 200, 'Job could not be submitted!'

    submission_response = submit_request.json()
    job_id = submission_response.get('job_id')
    print('job_id: ', job_id)

    return job_id


def wait_until_job_finished(delayed_jobs_server_base_path, job_id):
    """
    Waits until the job finished
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :param job_id: id of the job
    """
    print('Waiting until job finishes...')

    status_url = get_url_for_job_status(delayed_jobs_server_base_path, job_id)
    print('status_url: ', status_url)

    job_status = None
    queued_times = 0
    queued_timeout = 120

    while job_status != 'FINISHED':
        status_request = requests.get(status_url)
        print('Status request response code: ', status_request.status_code)

        status_response = status_request.json()
        job_status = status_response.get('status')
        job_progress = status_response.get('progress')

        print('job_status: ', job_status)
        print('job_progress: ', job_progress)
        if job_status == 'ERROR':
            print('job failed!')
            sys.exit(1)
        if job_status in ['QUEUED', 'CREATED', 'UNKNOWN']:
            queued_times += 1
            if queued_times == queued_timeout:
                print('job seems to be stuck!')
                sys.exit(1)
        time.sleep(1)


def load_json_data(file_path):
    """
    :param file_path: path of the json file to load
    :return: a dict with the data loaded
    """
    with open(file_path) as the_file:
        return json.loads(the_file.read())
