# pylint: disable=import-error
"""
Module that tests a simple es query
"""

import requests

from specific_tests import utils


def run_test(server_base_url, delayed_jobs_server_base_path):
    """
    Tests that the separator identifier works
    :param server_base_url: base url of the running server. E.g. http://127.0.0.1:5000
    :param delayed_jobs_server_base_path: base path for the delayed_jobs
    """

    print('-------------------------------------------')
    print('Testing the seprarator identifier')
    print('-------------------------------------------')
    print('delayed_jobs_server_base_path: ', delayed_jobs_server_base_path)

    url = utils.get_url_for_separator_identifier(server_base_url)
    print('url: ', url)

    type_of_ids = 'MOLECULE_CHEMBL_IDS'
    print('type_of_ids: ', type_of_ids)
    text = 'CHEMBL3548189GLADOSCHEMBL3041241GLADOSCHEMBL1198499GLADOSCHEMBL1616766GLADOSCHEMBL3552333' \
           'GLADOSCHEMBL3040769GLADOSCHEMBL1179177GLADOSCHEMBL1208242GLADOSCHEMBL2105295GLADOS' \
           'CHEMBL1623649GLADOSCHEMBL3990326GLADOSCHEMBL3551968GLADOSCHEMBL3815178GLADOSCHEMBL1624921' \
           'GLADOSCHEMBL1621119GLADOSCHEMBL1183111GLADOSCHEMBL354497GLADOSCHEMBL546869GLADOSCHEMBL1789284' \
           'GLADOSCHEMBL1739771GLADOSCHEMBL3553885GLADOSCHEMBL3140231GLADOSCHEMBL3991129'
    print('text: ', text)

    payload = {
        'type_of_ids': type_of_ids,
        'text': text
    }

    print('payload: ', payload)

    request = requests.post(url, data=payload)

    status_code = request.status_code
    print(f'status_code: {status_code}')
    response_text = request.text
    utils.print_es_response(response_text)
    assert status_code == 200, 'The request failed!'

    response_json = request.json()
    separator_got = response_json['separator']

    assert separator_got == 'GLADOS', 'The identified separator is not correct!'
