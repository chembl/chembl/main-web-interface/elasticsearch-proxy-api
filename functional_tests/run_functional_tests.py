#!/usr/bin/env python3
# pylint: disable=import-error
"""
    Script that runs the functional tests for the app
"""
import argparse
import os

from specific_tests import fun_test_simple_query, fun_test_property_config, \
    fun_test_group_config, fun_test_facets_group_config, fun_test_id_properties, \
    fun_test_url_shortening, fun_test_element_usage, \
    fun_test_go_slim_target_classification, fun_test_in_vivo_assay_classification, \
    fun_test_drug_indications_by_phase, \
    fun_test_organism_taxonomy_target_classification, fun_test_protein_target_classification, \
    fun_test_covid_entities_records, fun_test_database_summary, fun_test_entities_records, \
    fun_test_get_all_properties, fun_test_identify_separator
from specific_tests.entities_join import fun_test_entities_join_all_tests, fun_test_entities_join_from_search_by_ids, \
    fun_test_entities_join_from_direct_similarity_search
from specific_tests.eubopen import fun_test_eubopen_autocomplete, fun_test_eubopen_search
from specific_tests.heatmap import fun_test_col_headers, fun_test_summary, fun_test_cells, fun_test_autocomplete_heatmap
from specific_tests.es_data import fun_test_get_context_data, \
    fun_test_get_document_by_custom_id_prop, fun_test_get_document, fun_test_get_es_data, fun_test_query_with_context, \
    fun_test_query_with_search_by_ids_context, fun_test_query_with_direct_similarity_search_context
from specific_tests.search import fun_test_autocomplete, fun_test_properties_priorities

PARSER = argparse.ArgumentParser()
PARSER.add_argument('server_base_path', help='server base path to run the tests against',
                    default='http://127.0.0.1:5000', nargs='?')
PARSER.add_argument('delayed_jobs_server_base_path', help='The base path of the delayed jobs server used',
                    default='https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs', nargs='?')
PARSER.add_argument('web_services_base_path', help='The base path of the web serveces',
                    default='https://www.ebi.ac.uk/chembl/api/data', nargs='?')
ARGS = PARSER.parse_args()


def run():
    """
    Runs all functional tests
    """
    print(f'Running functional tests on {ARGS.server_base_path}')

    os.environ["FUN_TESTS_SERVER_BASE_PATH"] = str(ARGS.server_base_path)
    os.environ["FUN_TESTS_DJ_SERVER_BASE_PATH"] = str(ARGS.delayed_jobs_server_base_path)
    os.environ["FUN_TESTS_WS_SERVER_BASE_PATH"] = str(ARGS.web_services_base_path)

    for test_module in [fun_test_simple_query, fun_test_query_with_context,
                        fun_test_query_with_search_by_ids_context, fun_test_query_with_direct_similarity_search_context,
                        fun_test_property_config,
                        fun_test_group_config, fun_test_facets_group_config, fun_test_get_document,
                        fun_test_get_es_data,
                        fun_test_get_document_by_custom_id_prop,
                        fun_test_id_properties,
                        fun_test_get_context_data, fun_test_url_shortening,
                        fun_test_element_usage,
                        fun_test_go_slim_target_classification, fun_test_in_vivo_assay_classification,
                        fun_test_drug_indications_by_phase,
                        fun_test_organism_taxonomy_target_classification, fun_test_protein_target_classification,
                        fun_test_covid_entities_records, fun_test_database_summary, fun_test_entities_records,
                        fun_test_get_all_properties, fun_test_identify_separator, fun_test_entities_join_all_tests,
                        fun_test_entities_join_from_search_by_ids, fun_test_entities_join_from_direct_similarity_search,
                        fun_test_eubopen_autocomplete, fun_test_eubopen_search, fun_test_autocomplete,
                        fun_test_properties_priorities,
                        fun_test_col_headers, fun_test_summary, fun_test_cells, fun_test_autocomplete_heatmap]:
        test_module.run_test(ARGS.server_base_path, ARGS.delayed_jobs_server_base_path)


if __name__ == "__main__":
    run()
