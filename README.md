The location of the config file can be set up with the variable CONFIG_FILE_PATH, if not set, the value is 'config.yml'

# Environment cheats

```shell
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

# To run unit tests

```shell
python -m unittest
```

# Run development server

```shell
FLASK_APP=app flask run --host=0.0.0.0 
```

