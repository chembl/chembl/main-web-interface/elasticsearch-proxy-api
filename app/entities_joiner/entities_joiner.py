"""
Module with functions to make joins of entities
"""
# pylint: disable=too-many-locals
import json
import base64
from datetime import datetime
import hashlib

from app.entities_joiner import standardisation
from app.entities_joiner import ids_loader
from app.url_shortening import url_shortener
from app.config import RUN_CONFIG
from app.usage_statistics import statistics_saver
from app.context_loader import context_loader
from app.cache.decorators import return_if_cached_results
from app import app_logging
from app.context_loader import context_injector
from app.es_data import es_mappings


class EntitiesJoinerError(Exception):
    """Base class for exceptions in this file."""


def get_cache_key(
        entity_from, entity_to, raw_es_query,
        raw_selection_description, raw_context_obj=None, is_test=False):
    """
    :param entity_from: source entity of the items
    :param entity_to: destination entity of the join
    :param raw_es_query: stringifyed query in elasticsearch for the dataset
    :param raw_selection_description: stringifyed javascript object describing de selection of items in the dataset
    :param raw_context_obj: stringifyed object describing the context of the query
    :param is_test: tells if the request is marked as from a test
    :return: a cache key corresponding to the parameters given
    """
    query_identifier = f'{entity_from}-{entity_to}-{raw_es_query}-' \
                       f'{raw_selection_description}-is_test:{is_test}-' \
                       f'raw_context_obj:{raw_context_obj}-{RUN_CONFIG.get("cache_key_suffix")}'

    query_identifier_digest = hashlib.sha256(query_identifier.encode('utf-8')).digest()
    base64_identifier_hash = base64.b64encode(query_identifier_digest).decode('utf-8')
    return base64_identifier_hash


@return_if_cached_results({
    'cache_key_generator': get_cache_key,
    'timeout': 3600
})
def get_tiny_hash_to_related_items(raw_origin_entity, raw_destination_entity, raw_es_query,
                                   raw_selection_description, raw_context_obj=None, is_test=False):
    """
    :param raw_origin_entity: text with the origin entity of the items
    :param raw_destination_entity: text with the destination entity of the join
    :param raw_es_query: stringifyed query in elasticsearch for the dataset
    :param raw_selection_description: stringifyed javascript object describing de selection of items in the dataset
    :param raw_context_obj: stringifyed object describing the context of the query
    :param is_test: tells if the request is marked as from a test
    :return: the hash to the link with the generated state
    """

    parsed_join_params = parse_join_params(raw_origin_entity, raw_destination_entity, raw_selection_description,
                                           raw_es_query,
                                           raw_context_obj)

    origin_destination = parsed_join_params['origin_destination']
    parsed_destination_entity = origin_destination['parsed_destination_entity']

    ids = parsed_join_params['ids']
    query_parameters = parsed_join_params['query_parameters']
    parsed_origin_entity = origin_destination['parsed_origin_entity']

    join_state_hash = get_new_join_state_hash(ids, query_parameters['destination_query_generator'],
                                              parsed_origin_entity,
                                              parsed_destination_entity, is_test)

    if not is_test:
        save_statistics(raw_origin_entity, raw_destination_entity, len(ids), False)
    return join_state_hash


def parse_join_params(raw_origin_entity, raw_destination_entity, raw_selection_description, raw_es_query,
                      raw_context_obj):
    """
    :param raw_origin_entity: text with the origin entity of the items
    :param raw_destination_entity: text with the destination entity of the join
    :param raw_selection_description: stringifyed javascript object describing de selection of items in the dataset
    :param raw_es_query: stringifyed query in elasticsearch for the dataset
    :param raw_context_obj: stringifyed object describing the context of the query
    :return: a dict with the parameters parsed
    """

    origin_destination = parse_origin_destination_parameters(raw_origin_entity, raw_destination_entity)
    parsed_origin_entity = origin_destination['parsed_origin_entity']
    parsed_destination_entity = origin_destination['parsed_destination_entity']

    join_parameters = parse_join_parameters(parsed_origin_entity,
                                            parsed_destination_entity,
                                            raw_selection_description)

    es_query = json.loads(raw_es_query)
    context_dict = None if raw_context_obj is None else json.loads(raw_context_obj)
    origin_index_name = get_actual_origin_index_name(join_parameters['index_name'], context_dict)
    query_parameters = parse_query_parameters(parsed_origin_entity,
                                              parsed_destination_entity)

    ids = get_ids_for_query(context_dict, join_parameters['selection_description'], es_query,
                            join_parameters['origin_property'], origin_index_name)

    return {
        'origin_destination': origin_destination,
        'ids': ids,
        'query_parameters': query_parameters,
        'parsed_origin_entity': parsed_origin_entity
    }


def get_query_to_related_items(raw_origin_entity, raw_destination_entity, raw_es_query, raw_selection_description,
                               raw_context_obj=None, is_test=False):
    """
    :param raw_origin_entity: text with the origin entity of the items
    :param raw_destination_entity: text with the destination entity of the join
    :param raw_es_query: stringifyed query in elasticsearch for the dataset
    :param raw_selection_description: stringifyed javascript object describing de selection of items in the dataset
    :param raw_context_obj: stringifyed object describing the context of the query
    :param is_test: tells if the request is marked as from a test
    :return: like get_tiny_hash_to_related_items but returns the query to be used in the join
    """

    parsed_join_params = parse_join_params(raw_origin_entity, raw_destination_entity, raw_selection_description,
                                           raw_es_query,
                                           raw_context_obj)

    app_logging.debug(f'is_test: {is_test}')
    ids = parsed_join_params['ids']
    query_parameters = parsed_join_params['query_parameters']
    destination_query_generator = query_parameters['destination_query_generator']
    query_string = destination_query_generator(ids)

    return get_join_query(query_string)


def get_ids_for_query(context_dict, selection_description, es_query, origin_property, origin_index_name):
    """
    :param context_dict: dictionary describing the context
    :param selection_description: dictionary describing the selection
    :param es_query: query for the dataset
    :param origin_property: property in the origin index that identifies the items
    :param origin_index_name: name of the origin index
    :return:
    """

    if context_dict is not None:

        context_id = context_dict.get('context_id')
        if context_id.startswith('SEARCH_BY_IDS'):
            context = context_loader.get_search_by_ids_context(context_dict)
            subset_index_name = context['subset_index_name']
            return ids_loader.load_ids_for_query(es_query, selection_description,
                                                 origin_property, subset_index_name)

        context, total_results = context_loader.get_structure_search_or_blast_context(context_dict)
        id_properties = es_mappings.get_id_properties_for_index(origin_index_name)
        context_index = context_loader.load_context_index(id_properties, context)

        es_query_with_injections = context_injector.get_es_query_with_context_injections(es_query=es_query,
                                                                                         contextual_sort_data=None,
                                                                                         id_properties=id_properties,
                                                                                         total_results=total_results,
                                                                                         context_index=context_index)
        return ids_loader.load_ids_for_query(es_query_with_injections, selection_description,
                                             origin_property, origin_index_name)

    return ids_loader.load_ids_for_query(es_query, selection_description,
                                         origin_property, origin_index_name)


def load_origin_ids_from_structure_search_context(context_dict, selection_description):
    """
    :param context_dict: context dict received from the request.
    :param selection_description: dict describing the selection mode for the join
    :return: a list of ids for the structure search and the selection description indicated.
    """

    search_results = context_loader.load_structure_search_or_blast_results(context_dict)
    ids = ids_loader.load_ids_for_structure_search(search_results, selection_description)
    return ids


def load_origin_ids_from_blast_search_context(context_dict, selection_description):
    """
    :param context_dict: context dict received from the request.
    :param selection_description: dict describing the selection mode for the join
    :return: a list of ids for the blast search and the selection description indicated.
    """

    search_results = context_loader.load_structure_search_or_blast_results(context_dict)
    ids = ids_loader.load_ids_for_structure_search(search_results, selection_description,
                                                   id_property='target_chembl_id')
    return ids


def get_actual_origin_index_name(default_index_name, context_dict):
    """
    :param default_index_name: default index name to use if not a special case.
    :param context_dict: context dict received from the request.
    :return: the actual index name to use as origin for the join. If the context requires it, it could be a different
    index. For example, in the search by IDs, the data is obtained from the resulting index, not the default index.
    More specifically, if the user performed a search by ids to compounds, the index will not be chembl_molecule, but
    the resulting index created after the search.
    """
    if context_dict is None:
        return default_index_name

    if context_dict == {}:
        return default_index_name

    context_type = context_dict['context_type']
    if context_type in ['DIRECT_SIMILARITY', 'SUBSTRUCTURE', 'BLAST']:
        return default_index_name

    context = context_loader.get_search_by_ids_context(context_dict)
    subset_index_name = context['subset_index_name']
    return subset_index_name


def parse_origin_destination_parameters(raw_origin_entity, raw_destination_entity):
    """
    :param raw_origin_entity: text with the origin entity of the items
    :param raw_destination_entity: text with the destination entity of the join
    :return: A dict with the parsed values of the origin and destination entities of the join
    """

    if raw_destination_entity == raw_origin_entity:
        raise EntitiesJoinerError(
            f'entity_to ({raw_destination_entity}) and entity_from ({raw_origin_entity}) cannot be the same!')

    origin_destination = {}

    try:
        parsed_origin_entity = standardisation.PossibleOriginEntities(raw_origin_entity)
        origin_destination['parsed_origin_entity'] = parsed_origin_entity
    except ValueError as error:
        raise EntitiesJoinerError(
            f'entity_from: {str(error)}. Possible values are '
            f'{[item.value for item in standardisation.PossibleOriginEntities]}')

    try:
        parsed_destination_entity = standardisation.PossibleDestinationEntities(raw_destination_entity)
        origin_destination['parsed_destination_entity'] = parsed_destination_entity
    except ValueError as error:
        raise EntitiesJoinerError(
            f'entity_to: {str(error)}. Possible values are '
            f'{[item.value for item in standardisation.PossibleDestinationEntities]}')

    return origin_destination


def parse_selection_description_dict(raw_selection_description):
    """
    parses the selection description from the text entered, fails if invalid
    :param raw_selection_description: stringifyed javascript object describing de selection of items in the dataset
    :return: a dict with the selection description entered
    """
    selection_description_dict = json.loads(raw_selection_description)
    try:
        parsed_selection_mode = standardisation.SelectionModes(selection_description_dict['selectionMode'])
        if parsed_selection_mode == standardisation.SelectionModes.NO_ITEMS_EXCEPT:
            if len(selection_description_dict.get('exceptions', [])) == 0:
                raise EntitiesJoinerError(
                    f'When selection mode is {parsed_selection_mode} there must be at least one exception')
    except ValueError as error:
        raise EntitiesJoinerError(
            f'selectionMode: {str(error)}. '
            f'Possible values are {[item.value for item in standardisation.SelectionModes]}'
        )

    return selection_description_dict


def parse_join_parameters(parsed_origin_entity, parsed_destination_entity, raw_selection_description):
    """
    Parses the parameters for the join, fails if they are invalid
    :param parsed_origin_entity: origin entity of the join
    :param parsed_destination_entity: destination entity of the join
    :param raw_selection_description: stringifyied javascript object describing the selection
    :return: a dict with the parsed parameters
    """
    join_parameters = {}

    index_name = standardisation.get_index_name_for_origin_entity(parsed_origin_entity)
    fail_if_null(index_name, 'index name', parsed_origin_entity,
                 parsed_destination_entity)
    join_parameters['index_name'] = index_name

    origin_property = standardisation.get_origin_property(parsed_origin_entity,
                                                          parsed_destination_entity)
    fail_if_null(origin_property, 'origin property', parsed_origin_entity,
                 'parsed_destination_entity')

    join_parameters['origin_property'] = origin_property
    selection_description = parse_selection_description_dict(raw_selection_description)
    join_parameters['selection_description'] = selection_description

    return join_parameters


def parse_query_parameters(parsed_origin_entity, parsed_destination_entity):
    """
    parses que parameters to build the join query
    :param parsed_origin_entity: origin entity of the join
    :param parsed_destination_entity: destination entity of the join
    :return: a dict with the parsed parameters
    """
    query_parameters = {}
    destination_query_generator = standardisation.get_destination_query_generator(
        parsed_origin_entity,
        parsed_destination_entity)
    fail_if_null(destination_query_generator, 'destination query generator', parsed_origin_entity,
                 parsed_destination_entity)
    query_parameters['destination_query_generator'] = destination_query_generator

    return query_parameters


def fail_if_null(value, value_name, parsed_origin_entity, parsed_destination_entity):
    """
    Fails if the value is null and uses the rest of the parameters to build an error message
    :param value: value to check
    :param value_name: name of the value for the error message
    :param parsed_origin_entity: parsed from entity
    :param parsed_destination_entity: parsed to entity
    """
    if value is None:
        raise EntitiesJoinerError(f'There is no {value_name} configured for queries from {parsed_origin_entity.value} '
                                  f'to {parsed_destination_entity.value}')


def get_new_join_state_hash(ids, destination_query_generator, parsed_origin_entity, parsed_destination_entity, is_test):
    """
    :param ids: list of its for the join
    :param destination_query_generator: function that generates the join query from a list of ids
    :param parsed_origin_entity: standardised origin entity
    :param parsed_destination_entity: standardised destination entity
    :param is_test: indicates if it is a test request
    :return: the hash of the desired state
    """
    query_string = destination_query_generator(ids)
    origin_entity_name = standardisation.get_vue_entity_name_for_entity(parsed_origin_entity, plural=True)
    destination_entity_name = standardisation.get_vue_entity_name_for_entity(parsed_destination_entity, plural=True)
    join_query = get_join_query(query_string)

    desired_state = {
        'dataset': {
            'entityID': standardisation.get_vue_entity_id_for_entity(parsed_destination_entity),
            'initialQuery': join_query,
            'facetsState': None,
            'customFiltering': query_string,
            'subsetHumanDescription': f'{destination_entity_name} related to a list of {origin_entity_name}.'
        }
    }
    b64_desired_state = base64.b64encode(json.dumps(desired_state).encode()).decode()
    return get_destination_url_hash(b64_desired_state, is_test)


def get_join_query(query_string):
    """
    :param query_string: the querystring to use to join the items
    :return: the query to use to do the join
    """
    return {
        'query': {
            'bool': {
                'must': [
                    {
                        'query_string': {
                            'query': query_string,
                        },
                    },
                ],
            },
        },
    }


def get_destination_url_hash(hashable_part, is_test=False):
    """
    :param hashable_part: of the destination url generated
    :param is_test: indicates if it is a test request
    :return: a hash generated using the hashable url part of the destination browser
    """
    shortening_response = url_shortener.shorten_url(hashable_part, is_test)
    return shortening_response['hash']


def save_statistics(raw_entity_from, raw_entity_to, num_ids, is_cached):
    """
    :param raw_entity_from: source entity of the items
    :param raw_entity_to: destination entity of the join
    :param num_ids: number of ids joined
    :param is_cached: boolean indicating if the join is cached or not
    """

    statistics_document = {
        'entity_from': raw_entity_from,
        'entity_to': raw_entity_to,
        'num_ids': num_ids,
        'request_date': datetime.utcnow().timestamp() * 1000,
        'is_cached': is_cached
    }

    index_name = RUN_CONFIG.get('usage_statistics').get('entities_join_statistics_index')
    statistics_saver.save_record_to_elasticsearch(statistics_document, index_name)
