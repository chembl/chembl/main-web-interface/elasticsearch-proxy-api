"""
Module to test the ids loader
"""
import unittest
import os
import json

from app.entities_joiner import ids_loader


def load_sample_query(filename):
    """
    :param filename: name of the file to read
    :return: the dict with the query contained in the file in the data directory of these tests
    """
    filepath = os.path.join(os.path.dirname(__file__), 'data', filename)

    with open(filepath) as query_file:
        return json.loads(query_file.read())


class TestIDsLoader(unittest.TestCase):
    """
    Class to test the ids loader module
    """

    def test_generates_query_to_get_ids_when_selecting_all(self):
        """
        test that it generates the required query when selected all items in dataset
        """
        es_query = load_sample_query('sample_query_0.json')

        selection_description = {
            "selectionMode": "allItemsExcept",
            "exceptions": []
        }

        from_property = 'drug_warning.molecule_chembl_id'

        ids_query_must_be = {
            "query": {
                "bool": {
                    "filter": [],
                    "should": [],
                    "must_not": []
                }
            },
            "_source": [from_property],
        }

        ids_query_got = ids_loader.get_ids_query(es_query, selection_description, from_property)
        self.assertDictEqual(ids_query_must_be, ids_query_got,
                             msg='The query was not generated correctly when selecting all items!')

    def test_generates_query_to_get_ids_when_selecting_all_except_some(self):
        """
        test that it generates the required query when selected all items in dataset
        """
        es_query = load_sample_query('sample_query_0.json')

        selection_description = {
            "selectionMode": "allItemsExcept",
            "exceptions": ['CHEMBL3989861', 'CHEMBL3989724', 'CHEMBL64']
        }

        from_property = 'drug_warning.molecule_chembl_id'

        ids_query_must_be = {
            "query": {
                "bool": {
                    "filter": [],
                    "should": [],
                    "must_not": [
                        {
                            "terms": {
                                '_id': selection_description['exceptions']
                            }
                        }
                    ]
                }
            },
            "_source": [from_property],
        }

        ids_query_got = ids_loader.get_ids_query(es_query, selection_description, from_property)
        self.assertDictEqual(ids_query_must_be, ids_query_got,
                             msg='The query was not generated correctly when selecting all items except some!')

    def test_generates_query_to_get_ids_when_selecting_none_except_some(self):
        """
        test that it generates the required query when selecting none except zone
        """
        es_query = load_sample_query('sample_query_0.json')

        selection_description = {
            "selectionMode": "noItemsExcept",
            "exceptions": ['CHEMBL3989861', 'CHEMBL3989724', 'CHEMBL64']
        }

        from_property = 'drug_warning.molecule_chembl_id'

        ids_query_must_be = {
            "query": {
                "bool": {
                    "filter": [{
                        "terms": {
                            '_id': selection_description['exceptions']
                        }
                    }],
                }
            },
            "_source": [from_property],
        }

        ids_query_got = ids_loader.get_ids_query(es_query, selection_description, from_property)
        self.assertDictEqual(ids_query_must_be, ids_query_got,
                             msg='The query was not generated correctly when selecting no items except some!')
