"""
Module to test the entities joiner module
"""
import unittest

from app.entities_joiner import entities_joiner


class TestEntitiesJoiner(unittest.TestCase):
    """
    Class to test the entities joiner module
    """

    def test_fails_when_from_and_to_are_equal(self):
        """
        Tests that it fails when from and to are equal
        """
        entity_from = 'CHEMBL_COMPOUNDS'
        entity_to = 'CHEMBL_COMPOUNDS'
        es_query = '{"size": 24, "from": 0, "query": {"bool": {"must": [{"query_string": {"analyze_wildcard": True, ' \
                   '"query": "*"}}], "filter": []}}, "sort": []} '
        selection_description = '{"selectionMode": "allItemsExcept","exceptions": ["CHEMBL3989861", "CHEMBL3989724", ' \
                                '"CHEMBL64"]} '

        with self.assertRaises(entities_joiner.EntitiesJoinerError):
            entities_joiner.get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                                           selection_description)

    def test_fails_when_from_is_not_on_the_possibilities(self):
        """
        Tests that it fails when from is not on the possibilities
        """

        entity_from = 'NON_EXISTENT'
        entity_to = 'CHEMBL_COMPOUNDS'
        es_query = '{"size": 24, "from": 0, "query": {"bool": {"must": [{"query_string": {"analyze_wildcard": True, ' \
                   '"query": "*"}}], "filter": []}}, "sort": []} '
        selection_description = '{"selectionMode": "allItemsExcept","exceptions": ["CHEMBL3989861", "CHEMBL3989724", ' \
                                '"CHEMBL64"]} '

        with self.assertRaises(entities_joiner.EntitiesJoinerError):
            entities_joiner.get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                                           selection_description)

    def test_fails_when_to_is_not_on_the_possibilities(self):
        """
        Tests that it fails when to is not on the possibilities
        """

        entity_from = 'CHEMBL_COMPOUNDS'
        entity_to = 'NON_EXISTENT'
        es_query = '{"size": 24, "from": 0, "query": {"bool": {"must": [{"query_string": {"analyze_wildcard": True, ' \
                   '"query": "*"}}], "filter": []}}, "sort": []} '
        selection_description = '{"selectionMode": "allItemsExcept","exceptions": ["CHEMBL3989861", "CHEMBL3989724", ' \
                                '"CHEMBL64"]} '

        with self.assertRaises(entities_joiner.EntitiesJoinerError):
            entities_joiner.get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                                           selection_description)

    def test_fails_when_selection_mode_not_in_possibilities(self):
        """
        Tests that it fails when to is not on the possibilities
        """

        entity_from = 'CHEMBL_COMPOUNDS'
        entity_to = 'CHEMBL_TARGETS'
        es_query = '{"size": 24, "from": 0, "query": {"bool": {"must": [{"query_string": {"analyze_wildcard": True, ' \
                   '"query": "*"}}], "filter": []}}, "sort": []} '
        selection_description = '{"selectionMode": "non_existent","exceptions": ["CHEMBL3989861", "CHEMBL3989724", ' \
                                '"CHEMBL64"]} '

        with self.assertRaises(entities_joiner.EntitiesJoinerError):
            entities_joiner.get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                                           selection_description)

    def test_fails_when_selecting_no_items(self):
        """
        Tests that it fails when no items are selected
        """
        entity_from = 'CHEMBL_COMPOUNDS'
        entity_to = 'CHEMBL_TARGETS'
        es_query = '{"size": 24, "from": 0, "query": {"bool": {"must": [{"query_string": {"analyze_wildcard": True, ' \
                   '"query": "*"}}], "filter": []}}, "sort": []} '
        selection_description = '{"selectionMode": "noItemsExcept","exceptions": []} '

        with self.assertRaises(entities_joiner.EntitiesJoinerError):
            entities_joiner.get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                                           selection_description)
