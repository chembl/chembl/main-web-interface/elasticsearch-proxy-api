"""
Module that helps with the standardisation of the package
"""
from enum import Enum


class PossibleOriginEntities(Enum):
    """
    Enumeration with the possible froms allowed
    """
    CHEMBL_ACTIVITIES = 'CHEMBL_ACTIVITIES'
    CHEMBL_COMPOUNDS = 'CHEMBL_COMPOUNDS'
    CHEMBL_TARGETS = 'CHEMBL_TARGETS'
    CHEMBL_ASSAYS = 'CHEMBL_ASSAYS'
    CHEMBL_DOCUMENTS = 'CHEMBL_DOCUMENTS'
    CHEMBL_CELL_LINES = 'CHEMBL_CELL_LINES'
    CHEMBL_TISSUES = 'CHEMBL_TISSUES'
    CHEMBL_DRUG_WARNINGS = 'CHEMBL_DRUG_WARNINGS'
    CHEMBL_DRUGS = 'CHEMBL_DRUGS'
    CHEMBL_DRUG_MECHANISMS = 'CHEMBL_DRUG_MECHANISMS'
    CHEMBL_DRUG_INDICATIONS = 'CHEMBL_DRUG_INDICATIONS'
    EUBOPEN_COMPOUND = 'EUBOPEN_COMPOUND'
    EUBOPEN_ASSAY = 'EUBOPEN_ASSAY'


class PossibleDestinationEntities(Enum):
    """
    Enumeration with the possible to allowed
    """
    CHEMBL_ACTIVITIES = 'CHEMBL_ACTIVITIES'
    CHEMBL_COMPOUNDS = 'CHEMBL_COMPOUNDS'
    CHEMBL_TARGETS = 'CHEMBL_TARGETS'
    CHEMBL_ASSAYS = 'CHEMBL_ASSAYS'
    CHEMBL_DOCUMENTS = 'CHEMBL_DOCUMENTS'
    CHEMBL_CELL_LINES = 'CHEMBL_CELL_LINES'
    CHEMBL_TISSUES = 'CHEMBL_TISSUES'
    CHEMBL_DRUG_WARNINGS = 'CHEMBL_DRUG_WARNINGS'
    CHEMBL_DRUGS = 'CHEMBL_DRUGS'
    CHEMBL_DRUG_MECHANISMS = 'CHEMBL_DRUG_MECHANISMS'
    CHEMBL_DRUG_INDICATIONS = 'CHEMBL_DRUG_INDICATIONS'
    EUBOPEN_ASSAY = 'EUBOPEN_ASSAY'
    EUBOPEN_COMPOUND = 'EUBOPEN_COMPOUND'


class SelectionModes(Enum):
    """
    Possible selection modes
    """
    ALL_ITEMS_EXCEPT = 'allItemsExcept'
    NO_ITEMS_EXCEPT = 'noItemsExcept'


INDEX_NAMES_FOR_ORIGIN_ENTITIES = {
    PossibleOriginEntities.CHEMBL_ACTIVITIES: 'chembl_activity',
    PossibleOriginEntities.CHEMBL_COMPOUNDS: 'chembl_molecule',
    PossibleOriginEntities.CHEMBL_DRUGS: 'chembl_molecule',
    PossibleOriginEntities.CHEMBL_DRUG_MECHANISMS: 'chembl_mechanism_by_parent_target',
    PossibleOriginEntities.CHEMBL_DRUG_INDICATIONS: 'chembl_drug_indication_by_parent',
    PossibleOriginEntities.CHEMBL_TARGETS: 'chembl_target',
    PossibleOriginEntities.CHEMBL_ASSAYS: 'chembl_assay',
    PossibleOriginEntities.CHEMBL_DOCUMENTS: 'chembl_document',
    PossibleOriginEntities.CHEMBL_CELL_LINES: 'chembl_cell_line',
    PossibleOriginEntities.CHEMBL_TISSUES: 'chembl_tissue',
    PossibleOriginEntities.CHEMBL_DRUG_WARNINGS: 'chembl_drug_warning_by_parent',
    PossibleOriginEntities.EUBOPEN_COMPOUND: 'chembl_eubopen_molecule',
    PossibleOriginEntities.EUBOPEN_ASSAY: 'chembl_eubopen_assay',
}


def get_index_name_for_origin_entity(parsed_origin_entity):
    """
    :param parsed_origin_entity: entity 'from' parsed by the PossibleEntitiesFrom enum
    :return: the index name corresponding to the from entity
    """
    index_name = INDEX_NAMES_FOR_ORIGIN_ENTITIES.get(parsed_origin_entity)

    return index_name


VUE_ENTITY_ID_FOR_ENTITIES = {
    PossibleDestinationEntities.CHEMBL_ACTIVITIES: 'Activity',
    PossibleDestinationEntities.CHEMBL_COMPOUNDS: 'Compound',
    PossibleDestinationEntities.CHEMBL_DRUGS: 'Drug',
    PossibleDestinationEntities.CHEMBL_DRUG_MECHANISMS: 'DrugMechanism',
    PossibleDestinationEntities.CHEMBL_DRUG_INDICATIONS: 'DrugIndication',
    PossibleDestinationEntities.CHEMBL_TARGETS: 'Target',
    PossibleDestinationEntities.CHEMBL_ASSAYS: 'Assay',
    PossibleDestinationEntities.CHEMBL_DOCUMENTS: 'Document',
    PossibleDestinationEntities.CHEMBL_CELL_LINES: 'CellLine',
    PossibleDestinationEntities.CHEMBL_TISSUES: 'Tissue',
    PossibleDestinationEntities.CHEMBL_DRUG_WARNINGS: 'DrugWarning',
    PossibleDestinationEntities.EUBOPEN_ASSAY: 'EubopenAssay',
}


def get_vue_entity_id_for_entity(parsed_entity):
    """
    :param parsed_entity: entity 'from' parsed by the PossibleEntitiesFrom enum
    :return: the corresponding entity id for the vue frontend.
    """
    entity_id = VUE_ENTITY_ID_FOR_ENTITIES.get(parsed_entity)

    return entity_id


VUE_ENTITY_NAMES_FOR_ENTITIES = {
    PossibleOriginEntities.CHEMBL_ACTIVITIES: {
        'singular': 'Activity',
        'plural': 'Activities'
    },
    PossibleOriginEntities.CHEMBL_COMPOUNDS: {
        'singular': 'Compound',
        'plural': 'Compounds'
    },
    PossibleOriginEntities.EUBOPEN_COMPOUND: {
        'singular': 'Compound',
        'plural': 'Compounds'
    },
    PossibleOriginEntities.CHEMBL_DRUGS: {
        'singular': 'Drug',
        'plural': 'Drugs'
    },
    PossibleOriginEntities.CHEMBL_DRUG_MECHANISMS: {
        'singular': 'Drug Mechanism',
        'plural': 'Drug Mechanisms'
    },
    PossibleOriginEntities.CHEMBL_DRUG_INDICATIONS: {
        'singular': 'Drug Indication',
        'plural': 'Drug Indications'
    },
    PossibleOriginEntities.CHEMBL_TARGETS: {
        'singular': 'Target',
        'plural': 'Targets'
    },
    PossibleOriginEntities.CHEMBL_ASSAYS: {
        'singular': 'Assay',
        'plural': 'Assays'
    },
    PossibleOriginEntities.CHEMBL_DOCUMENTS: {
        'singular': 'Document',
        'plural': 'Documents'
    },
    PossibleOriginEntities.CHEMBL_CELL_LINES: {
        'singular': 'Cell Line',
        'plural': 'Cell Lines'
    },
    PossibleOriginEntities.CHEMBL_TISSUES: {
        'singular': 'Tissue',
        'plural': 'Tissues'
    },
    PossibleOriginEntities.CHEMBL_DRUG_WARNINGS: {
        'singular': 'DrugWarning',
        'plural': 'Drug Warnings'
    },
    PossibleOriginEntities.EUBOPEN_ASSAY: {
        'singular': 'Assay',
        'plural': 'Assays'
    },
    PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
        'singular': 'Activity',
        'plural': 'Activities'
    },
    PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
        'singular': 'Compound',
        'plural': 'Compounds'
    },
    PossibleDestinationEntities.CHEMBL_DRUGS: {
        'singular': 'Drug',
        'plural': 'Drugs'
    },
    PossibleDestinationEntities.CHEMBL_DRUG_MECHANISMS: {
        'singular': 'Drug Mechanism',
        'plural': 'Drug Mechanisms'
    },
    PossibleDestinationEntities.CHEMBL_DRUG_INDICATIONS: {
        'singular': 'Drug Indication',
        'plural': 'Drug Indications'
    },
    PossibleDestinationEntities.CHEMBL_TARGETS: {
        'singular': 'Target',
        'plural': 'Targets'
    },
    PossibleDestinationEntities.CHEMBL_ASSAYS: {
        'singular': 'Assay',
        'plural': 'Assays'
    },
    PossibleDestinationEntities.CHEMBL_DOCUMENTS: {
        'singular': 'Document',
        'plural': 'Documents'
    },
    PossibleDestinationEntities.CHEMBL_CELL_LINES: {
        'singular': 'Cell Line',
        'plural': 'Cell Lines'
    },
    PossibleDestinationEntities.CHEMBL_TISSUES: {
        'singular': 'Tissue',
        'plural': 'Tissues'
    },
    PossibleDestinationEntities.CHEMBL_DRUG_WARNINGS: {
        'singular': 'DrugWarning',
        'plural': 'Drug Warnings'
    },
    PossibleDestinationEntities.EUBOPEN_ASSAY: {
        'singular': 'Assay',
        'plural': 'Assays'
    },
    PossibleDestinationEntities.EUBOPEN_COMPOUND: {
        'singular': 'Compound',
        'plural': 'Compounds'
    }
}


def get_vue_entity_name_for_entity(parsed_entity, plural):
    """
    :param parsed_entity: entity 'from' parsed by the PossibleEntitiesFrom enum
     :param plural: boolean that tells if you want the plural name or not
    :return: the corresponding entity id for the vue frontend.
    """
    names = VUE_ENTITY_NAMES_FOR_ENTITIES.get(parsed_entity)
    if plural:
        return names['plural']
    return names['singular']


def create_simple_query_generator(destination_properties):
    """
    :param destination_properties: list of properties to use to build the query
    :return: a function to be used to build the join query
    """

    def join_function(ids):
        """
        :param ids: ids of the items matches
        :return: query to use for the join
        """
        # make it a set to avoid duplicates
        ids_set = set(ids)
        clauses_groups = []
        for destination_property in destination_properties:
            ids_clauses = " OR ".join([f'"{item_id}"' for item_id in ids_set])
            clauses_group = f'{destination_property}:({ids_clauses})'
            clauses_groups.append(clauses_group)

        return ' OR '.join(clauses_groups)

    return join_function


def create_drugs_query_generator(destination_properties):
    """
    :param destination_properties: list of properties to use to build the query
    :return: a function to be used to build the join query
    """

    def join_function(ids):
        """
        :param ids: ids of the items matches
        :return: query to use for the join
        """
        # make it a set to avoid duplicates
        ids_set = set(ids)
        clauses_groups = []
        for destination_property in destination_properties:
            ids_clauses = " OR ".join([f'"{item_id}"' for item_id in ids_set])
            clauses_group = f'{destination_property}:({ids_clauses})'
            clauses_groups.append(clauses_group)

        return f"{' OR '.join(clauses_groups)} AND _metadata.drug.is_drug:true"

    return join_function


JOIN_PROPERTIES = {
    'from': {
        PossibleOriginEntities.CHEMBL_COMPOUNDS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DRUGS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_drugs_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DRUG_MECHANISMS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['mechanism_of_action.molecule_chembl_id', 'mechanism_of_action.parent_molecule_chembl_id']
                    )
                },
                PossibleDestinationEntities.CHEMBL_DRUG_INDICATIONS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['drug_indication.molecule_chembl_id', 'drug_indication.parent_molecule_chembl_id']
                    )
                },
                PossibleDestinationEntities.CHEMBL_TARGETS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_compounds.all_chembl_ids']
                    )
                },
            }
        },
        PossibleOriginEntities.EUBOPEN_COMPOUND: {
            'to': {
                PossibleDestinationEntities.EUBOPEN_ASSAY: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_compounds.all_chembl_ids'])
                },
            }
        },
        PossibleOriginEntities.EUBOPEN_ASSAY: {
            'to': {
                PossibleDestinationEntities.EUBOPEN_COMPOUND: {
                    'origin_property': 'assay_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_assays.all_chembl_ids'])
                },
            }
        },

        PossibleOriginEntities.CHEMBL_DRUGS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DRUG_MECHANISMS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['mechanism_of_action.molecule_chembl_id', 'mechanism_of_action.parent_molecule_chembl_id']
                    )
                },
                PossibleDestinationEntities.CHEMBL_DRUG_INDICATIONS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['drug_indication.molecule_chembl_id', 'drug_indication.parent_molecule_chembl_id']
                    )
                },
                PossibleDestinationEntities.CHEMBL_TARGETS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_compounds.all_chembl_ids']
                    )
                },
            }
        },
        PossibleOriginEntities.CHEMBL_DRUG_MECHANISMS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_DRUGS: {
                    'origin_property': 'parent_molecule.molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'parent_molecule.molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['molecule_chembl_id', 'molecule_hierarchy.parent_chembl_id']
                    )
                },
                PossibleDestinationEntities.CHEMBL_TARGETS: {
                    'origin_property': 'target.target_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['target_chembl_id']
                    )
                }
            }
        },
        PossibleOriginEntities.CHEMBL_DRUG_INDICATIONS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_DRUGS: {
                    'origin_property': 'parent_molecule.molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'parent_molecule.molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['molecule_chembl_id', 'molecule_hierarchy.parent_chembl_id']
                    )
                }
            }
        },
        PossibleOriginEntities.CHEMBL_ACTIVITIES: {
            'to': {
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_TARGETS: {
                    'origin_property': 'target_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['target_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_ASSAYS: {
                    'origin_property': 'assay_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['assay_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DOCUMENTS: {
                    'origin_property': 'document_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['document_chembl_id'])
                },
            }
        },
        PossibleOriginEntities.CHEMBL_TARGETS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'target_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['target_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DRUG_MECHANISMS: {
                    'origin_property': 'target_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['target.target_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'target_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_targets.all_chembl_ids'])
                },
            }
        },
        PossibleOriginEntities.CHEMBL_ASSAYS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'assay_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['assay_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'assay_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_assays.all_chembl_ids'])
                },
            },
        },
        PossibleOriginEntities.CHEMBL_DOCUMENTS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'document_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['document_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'document_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_documents.all_chembl_ids'])
                },
                PossibleDestinationEntities.CHEMBL_TARGETS: {
                    'origin_property': 'document_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_documents.all_chembl_ids'])
                },
            },
        },
        PossibleOriginEntities.CHEMBL_CELL_LINES: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'cell_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.assay_data.cell_chembl_id']
                    )
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'cell_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_cell_lines.all_chembl_ids'])
                },
            }
        },
        PossibleOriginEntities.CHEMBL_TISSUES: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'tissue_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.assay_data.tissue_chembl_id']
                    )
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'tissue_chembl_id',
                    'destination_query_generator': create_simple_query_generator(
                        ['_metadata.related_tissues.all_chembl_ids'])
                },
            }
        },
        PossibleOriginEntities.CHEMBL_DRUG_WARNINGS: {
            'to': {
                PossibleDestinationEntities.CHEMBL_ACTIVITIES: {
                    'origin_property': 'drug_warning.molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_COMPOUNDS: {
                    'origin_property': 'drug_warning.molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DRUGS: {
                    'origin_property': 'drug_warning.parent_molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DRUG_MECHANISMS: {
                    'origin_property': 'drug_warning.parent_molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                },
                PossibleDestinationEntities.CHEMBL_DRUG_INDICATIONS: {
                    'origin_property': 'drug_warning.parent_molecule_chembl_id',
                    'destination_query_generator': create_simple_query_generator(['molecule_chembl_id'])
                }
            }
        }
    }
}


def get_origin_property(parsed_origin_entity, parsed_destination_entity):
    """
    :param parsed_origin_entity: entity 'from' parsed by the PossibleEntitiesFrom enum
    :param parsed_destination_entity: entity 'to' parsed by the PossibleEntitiesTo enum
    :return: the property used in the from entity of the join
    """
    return JOIN_PROPERTIES. \
        get('from', {}). \
        get(parsed_origin_entity, {}). \
        get('to', {}). \
        get(parsed_destination_entity, {}).get('origin_property')


def get_destination_query_generator(parsed_origin_entity, parsed_destination_entity):
    """
    :param parsed_origin_entity: entity 'from' parsed by the PossibleEntitiesFrom enum
    :param parsed_destination_entity: entity 'to' parsed by the PossibleEntitiesTo enum
    :return: the property used in the from entity of the join
    """
    return JOIN_PROPERTIES. \
        get('from', {}). \
        get(parsed_origin_entity, {}). \
        get('to', {}). \
        get(parsed_destination_entity, {}).get('destination_query_generator')
