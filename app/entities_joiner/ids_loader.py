"""
Module with functions that help to load the ids for the entities joiner
"""
from app.entities_joiner import standardisation
from app.es_data import es_data
from utils import dict_property_access


def load_ids_for_query(es_query, selection_description, from_property, index_name):
    """
    :param es_query: query for the dataset
    :param selection_description: dict describing the selection
    :param from_property: property to get to do the join
    :param index_name: name of the index to query
    from_property
    :return: a list of ids for the query and the selection description indicated.
    """
    ids = []  # use a list to keep the same order as the query returns
    already_added_ids = set()
    ids_query = get_ids_query(es_query, selection_description, from_property)
    ids_scanner = es_data.get_es_scanner(index_name, ids_query)
    for doc_i in ids_scanner:
        doc_source = doc_i['_source']
        from_property_value = dict_property_access.get_property_value(doc_source, from_property)
        if from_property_value in already_added_ids:
            continue
        already_added_ids.add(from_property_value)
        ids.append(from_property_value)
    return ids


def get_ids_query(es_query, selection_description, from_property):
    """
    :param es_query: query for the dataset
    :param selection_description: dict describing the selection
    :param from_property: property to get to do the join
    :return: the query to use to get the ids depending on the selection description
    """

    selection_mode = selection_description['selectionMode']
    parsed_selection_mode = standardisation.SelectionModes(selection_mode)
    exceptions = selection_description.get('exceptions', [])

    if parsed_selection_mode == standardisation.SelectionModes.ALL_ITEMS_EXCEPT:
        if len(exceptions) == 0:
            return get_ids_query_for_all_items(es_query, from_property)
        return get_ids_query_for_all_items_except_some(es_query, from_property, exceptions)

    # Selecting none except some
    return get_ids_query_for_no_items_except_some(es_query, from_property, exceptions)


def get_ids_query_for_all_items(es_query, from_property):
    """
    :param es_query: uery for the dataset
    :param from_property: property to get to to the join
    :return: the ids query for all items
    """
    return {
        'query': es_query.get('query'),
        "_source": [from_property],
    }


def get_ids_query_for_all_items_except_some(es_query, from_property, exceptions):
    """
    :param es_query: query for the dataset
    :param from_property: property to get to to the join
    :param exceptions: selection exceptions
    :return: the ids query for all items except some
    """
    dataset_query = es_query.get('query')
    if dataset_query.get('bool') is None:
        dataset_query['bool'] = {}
    if dataset_query.get('bool').get('must_not') is None:
        dataset_query['bool']['must_not'] = []
    dataset_query['bool']['must_not'].append({
        'terms': {
            '_id': exceptions
        }
    })
    return {
        'query': dataset_query,
        "_source": [from_property],
    }


def get_ids_query_for_no_items_except_some(es_query, from_property, exceptions):
    """
    :param es_query: query for the dataset
    :param from_property: property to get to to the join
    :param exceptions: selection exceptions
    :return: the ids query for all items except some
    """
    dataset_query = es_query.get('query')
    dataset_query = {} if dataset_query is None else dataset_query
    dataset_query['bool'] = {
        'filter': [{
            'terms': {
                '_id': exceptions
            }
        }]
    }
    return {
        'query': dataset_query,
        "_source": [from_property],
    }


# --------------------------------------------------------------------------------
# For direct structure search context
# --------------------------------------------------------------------------------

def load_ids_for_structure_search(search_results, selection_description, id_property='molecule_chembl_id'):
    """
    :param id_property: property that identifies each item
    :param search_results: results obtained in the structure search
    :param selection_description: dict describing the selection
    :return: the ids according to the results and the selection description
    """
    all_ids = [result[id_property] for result in search_results]

    selection_mode = selection_description['selectionMode']
    parsed_selection_mode = standardisation.SelectionModes(selection_mode)
    exceptions = selection_description.get('exceptions', [])

    if parsed_selection_mode == standardisation.SelectionModes.ALL_ITEMS_EXCEPT:
        return list(set(all_ids) - set(exceptions))

    # Selecting none except some
    return exceptions
