"""
Common Schemas shared among all blueprints
"""
from marshmallow import Schema, fields


class CommonSchema(Schema):
    """
    Class that defines the common schema for all blueprints
    """
    # if true, means that the request is made for testing and I should save records to the usage statistics
    is_test = fields.Boolean()
