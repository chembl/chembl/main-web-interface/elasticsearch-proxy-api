"""
Utils functions for processing the schemas
"""


def parse_boolean_param(request_params, field_name):
    """
    parses a boolean parameter in a request
    :param request_params: the dict with the request parameters
    :param field_name: name of the field to parse
    :return: True if value is taken as true, False otherwise
    """
    field_value = request_params.get(field_name, False)

    if isinstance(field_value, bool):
        return field_value

    if field_value.lower() == 'true':
        return True
    if field_value.lower() == 'false':
        return False

    return False
