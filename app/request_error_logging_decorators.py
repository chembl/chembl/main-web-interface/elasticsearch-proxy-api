# pylint: disable=broad-except
"""
Decorators used in helping to register the errors of requests.
"""
from functools import wraps

from app import request_utils


def log_and_return_internal_server_error():
    """
    Logs to the an error if it presents during the execution of the function
    """

    def wrap(func):
        @wraps(func)
        def wrapped_func(*args, **kwargs):

            try:
                response = func(*args, **kwargs)
                return response
            except Exception as error:
                return request_utils.log_and_return_internal_server_error(error)

        return wrapped_func

    return wrap
