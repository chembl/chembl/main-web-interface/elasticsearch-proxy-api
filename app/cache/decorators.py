"""
Module that defines the decorators for the app cache
"""
from functools import wraps

from farmhash import FarmHash128  # pylint: disable=no-name-in-module

from app.config import RUN_CONFIG
from app import app_logging
from app.cache import cache


def return_if_cached_results(config):
    """
    Decorator to return results without executing the function if results are cached.
    After executing the function saves the results in cache.
    :param config: configuration of the cache
    """

    def wrap(func):
        @wraps(func)
        def wrapped_func(*args, **kwargs):
            cache_key_generator = config['cache_key_generator']
            base_cache_key = f'{FarmHash128(cache_key_generator(*args, **kwargs))}'

            cache_key = f'{base_cache_key}-{RUN_CONFIG.get("cache_key_suffix")}'
            app_logging.debug(f'cache_key: {cache_key}')
            cache_response = cache.fail_proof_get(key=cache_key)
            if cache_response is not None:
                app_logging.debug(f'{cache_key} was cached.')
                return cache_response

            app_logging.debug(f'{cache_key} not found in cache.')
            function_results = func(*args, **kwargs)

            timeout = config['timeout']
            cache.fail_proof_set(cache_key, function_results, timeout)
            app_logging.debug(f'Saved {cache_key} in cache. Timeout: {timeout}')

            return function_results

        return wrapped_func

    return wrap
