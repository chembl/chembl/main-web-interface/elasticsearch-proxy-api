"""
Module with utils to handle http requests
"""
import traceback

from flask import jsonify, Response

from app import app_logging


def log_error(error):
    """
    prints the error to the log
    :param error: the error to log
    """
    app_logging.error('---')
    app_logging.error(str(error))
    app_logging.error(traceback.format_exc())
    app_logging.error('---')


def internal_server_error(message):
    """
    :param message: message describing the error
    :return: a response with the code for internal server error and the message passed as parameter
    """
    response = jsonify({'message': message})
    response.status_code = 500
    return response


def log_and_return_internal_server_error(error):
    """
    prints to the logs and returns a corresponding error response.
    :param error: instance of the error caught
    :return: a response with the code for internal server error and the message passed as parameter
    """
    log_error(error)
    return internal_server_error('Internal error, please check the logs.')


def not_found_404():
    """
    :return: a response with the code for not found
    """
    return Response(status=404)


def log_and_return_not_found_404(error):
    """
    prints to the logs and returns a corresponding error response.
    :param error: instance of the error caught
    :return: a response with the code for 404 not found
    """
    log_error(error)
    return not_found_404()
