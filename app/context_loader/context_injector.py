"""
Module that implements functions to inject contexts into ES queries
"""
import copy

CONTEXT_PREFIX = '_context'


def get_es_query_with_context_injections(es_query, contextual_sort_data, id_properties, total_results,
                                         context_index):
    """
    Injects the context results into the es query, the context is a dict with the results of an external search
    (similarity, substructure, BLAST)
    :param es_query: dict with the query to send to elasticsearch
    :param contextual_sort_data: dict describing the sorting by contextual properties
    :param id_properties: property used to identity each item
    :param total_results: total number of results
    :param context_index: index with the context
    :return: the es query with the context injected to be sent to elasticsearch
    """

    injected_query = copy.deepcopy(es_query)

    if contextual_sort_data is not None:
        scores_query = get_scores_query(contextual_sort_data, id_properties, total_results, context_index)
        injected_query['query']['bool']['must'].append(scores_query)

    ids_list = list(context_index.keys())
    ids_query = get_request_for_chembl_ids(id_properties, ids_list)
    injected_query['query']['bool']['filter'].append(ids_query)

    return injected_query


def get_scores_query(contextual_sort_data, id_properties, total_results, context_index):
    """
    Returns the query with the scores for the data to sort it with the contextual properties.
    IT DOES NOT SUPPORT MULTIPLE ID PROPERTIES
    :param contextual_sort_data: dict describing the sorting by contextual properties
    :param id_properties: property used to identity each item
    :param total_results: total number of results
    :param context_index: index with the context
    """
    id_property = id_properties[0]
    contextual_sort_data_keys = contextual_sort_data.keys()
    if len(contextual_sort_data_keys) == 0:
        # if nothing is specified use the default scoring script, which is to score them according to their original
        # position in the results
        score_property = 'index'
        score_script = f'String id=doc["{id_property}"].value; ' \
                       f'return {str(total_results)} - params.scores[id]["{score_property}"];'
    else:

        raw_score_property = list(contextual_sort_data_keys)[0]
        score_property = raw_score_property.replace('{}.'.format(CONTEXT_PREFIX), '')
        sort_order = contextual_sort_data[raw_score_property]

        if sort_order == 'desc':
            score_script = "String id=doc['" + id_property + "'].value; " \
                                                             "return params.scores[id]['" + score_property + "'];"
        else:
            score_script = "String id=doc['" + id_property + "'].value; " \
                                                             "return 1 / params.scores[id]['" + score_property + "'];"

    scores_query = {
        'function_score': {
            'functions': [{
                'script_score': {
                    'script': {
                        'lang': "painless",
                        'params': {
                            'scores': context_index,
                        },
                        'source': score_script
                    }
                }
            }]
        }
    }

    return scores_query


def get_request_for_chembl_ids(id_properties, ids_list):
    """
    creates a terms query with the ids given as a parameter for the id_property given as parameter
    IT DOES NOT SUPPORT MULTIPLE ID PROPERTIES
    :param id_properties: property that identifies the items
    :param ids_list: list of ids to query
    :return: the terms query to use
    """
    id_property = id_properties[0]
    query = {
        'terms': {
            id_property: ids_list
        }
    }

    return query
