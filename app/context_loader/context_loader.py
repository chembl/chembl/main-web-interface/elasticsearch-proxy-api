"""
Module that loads contexts from results
"""
import re
import urllib.parse
import json

import requests
from farmhash import FarmHash128  # pylint: disable=no-name-in-module

from app.config import RUN_CONFIG
from app import app_logging
from app.cache.decorators import return_if_cached_results
from utils import id_properties


class ContextLoaderError(Exception):
    """Base class for exceptions in this module."""


WEB_RESULTS_SIZE_LIMIT = RUN_CONFIG.get('filter_query_max_clauses')


def get_context_url(context_dict):
    """
    returns the url for loading the context
    :param context_dict: dict describing the context
    """
    context_type = context_dict['context_type']
    external_base_url = context_dict["web_services_base_url"] if context_type == 'DIRECT_SIMILARITY' else context_dict[
        "delayed_jobs_base_url"]
    internal_base_url = get_internal_context_service_base_url(external_base_url, context_type)

    if context_type == 'DIRECT_SIMILARITY':
        smiles = context_dict['context_id']
        similarity_threshold = context_dict['similarity_threshold']

        return f'{internal_base_url}/similarity/{urllib.parse.quote(smiles)}/{similarity_threshold}.json' \
               f'?limit=1000&only=molecule_chembl_id,similarity'

    if context_type in ['TO_COMPOUNDS', 'TO_TARGETS']:
        return f'{internal_base_url}/outputs/{context_dict["context_id"]}/summary.json'

    return f'{internal_base_url}/outputs/{context_dict["context_id"]}/results.json'


def get_internal_context_service_base_url(external_url, context_type):
    """
    The context obj contains a base url of the service used to perform the job or do the search.
    For example:
    {
      ...
      "delayed_jobs_base_url":"https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs",
      ...
    }
    This means that the delayed jobs base url used to generate that context is
    https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs

    This function replaces the hostname (www.ebi.ac.uk) with for example
    'delayed-jobs.chembl-staging.svc.cluster.local:5000'

    Which is the internal hostname of the server used. So the returned url will be:

    http://delayed-jobs.chembl-staging.svc.cluster.local:5000/chembl/interface_api/delayed_jobs

    This allows to load the context internally.

    :param external_url: external url of the service used
    :param context_type: type of context used
    :return: the url replacing the hostname with an internal hostname
    """
    app_logging.info(f'---')
    app_logging.info(f'get_internal_context_service_base_url: {external_url}, context_type:{context_type}')
    host = re.search(r'[^/]+\.ebi\.ac\.uk(:\d+)?', external_url).group(0)
    host_to_map = re.sub(r':.*$', '',
                         host)  # Remove port information to avoid annoyances with k8s changing the node ports

    host_mappings = RUN_CONFIG.get('delayed_jobs', {}).get('server_mapping', {})
    default_mapping = RUN_CONFIG.get('delayed_jobs', {}).get('default_server_mapping', '')
    if context_type == 'DIRECT_SIMILARITY':
        host_mappings = RUN_CONFIG.get('web_services', {}).get('server_mapping', {})
        default_mapping = RUN_CONFIG.get('web_services', {}).get('default_server_mapping', '')

    app_logging.info(f'host_mappings: {host_mappings}')
    app_logging.info(f'host_to_map: {host_to_map}')
    mapped_host = host_mappings.get(host_to_map, default_mapping)
    mapped_base_url = external_url.replace(host, mapped_host)

    # Make sure to always use http because connection is internal
    scheme = re.search(r'^http(s)?://', mapped_base_url).group(0)
    if scheme is None:
        mapped_base_url = f'http://{mapped_base_url}'
    else:
        mapped_base_url = mapped_base_url.replace(scheme, 'http://')

    return mapped_base_url


def get_context_cache_key(context_dict):
    """
    :param context_dict: the dict describing the context
    :return: a cache key to cache the context
    """
    stable_context_dict_str = json.dumps(context_dict, sort_keys=True)
    context_dict_hash = f'{FarmHash128(stable_context_dict_str)}'

    return f'Context-{context_dict_hash}'


# ----------------------------------------------------------------------------------------------------------------------
# Structure search context
# ----------------------------------------------------------------------------------------------------------------------
@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: get_context_cache_key(kwargs.get("context_dict", args[0])),
    'timeout': RUN_CONFIG.get('es_mappings_cache_seconds')
})
def get_structure_search_or_blast_context(context_dict):
    """
    Returns the context described by the context dict
    :param context_dict: dictionary describing the context
    :return: the context loaded as an object
    """
    results = load_structure_search_or_blast_results(context_dict)

    total_results = len(results)
    if total_results > WEB_RESULTS_SIZE_LIMIT:
        results = results[0:WEB_RESULTS_SIZE_LIMIT]

    return results, total_results


def load_structure_search_or_blast_results(context_dict):
    """
    Loads the results of the structure search, takes into account if it was a direct similarity search or a delayed job.
    :param context_dict: dictionary describing the context
    :return: the similarity results obtained
    """
    context_type = context_dict['context_type']
    context_url = get_context_url(context_dict)
    app_logging.info(f'Loading context from url: {context_url}')

    if context_type == 'DIRECT_SIMILARITY':
        return load_direct_similarity_results(context_url)

    context_request = requests.get(context_url)
    if context_request.status_code != 200:
        raise ContextLoaderError('There was an error while loading the context: ' + context_request.text)
    json_response = context_request.json()
    app_logging.info(f'json_response: {json_response}')
    return json_response['search_results']


def load_direct_similarity_results(context_url):
    """
    Loads the similarity search results by going directly to the web services and not to the delayed jobs result
    :param context_url: url of the context to load
    :return: the similarity results
    """
    results = []
    more_results_to_load = True
    search_url = context_url
    while more_results_to_load:
        search_request = requests.get(search_url)
        json_response = search_request.json()

        error_message = json_response.get('error_message')
        if error_message is not None:
            raise ContextLoaderError(f'There was an error while loading the data from the web services: '
                                     f'{error_message}')

        results += json_response['molecules']
        next_page = json_response['page_meta']['next']

        if next_page is not None:
            host = re.search(r'^https?://[^/]+', context_url).group(0)
            search_url = f'{host}{next_page}'
        else:
            more_results_to_load = False

    return results


def load_context_index(id_properties_list, context):
    """
    Loads an index based on the id property of the context, for fast access
    :param id_properties_list: property used to identify each item
    :param context: context loaded
    :return:
    """
    context_index = {}

    for index_number, item in enumerate(context):
        id_value = id_properties.get_id_value(id_properties_list, item)
        context_index[id_value] = item
        context_index[id_value]['index'] = index_number

    return context_index


# ----------------------------------------------------------------------------------------------------------------------
# Search by IDs context
# ----------------------------------------------------------------------------------------------------------------------
@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: get_context_cache_key(kwargs.get("context_dict", args[0])),
    'timeout': RUN_CONFIG.get('es_mappings_cache_seconds')
})
def get_search_by_ids_context(context_dict):
    """
    Returns the search by ids context described by the context dict
    :param context_dict: dictionary describing the context
    :return: the context loaded as an object
    """
    context_url = get_context_url(context_dict)
    app_logging.debug(f'Loading context from url: {context_url}')
    context_request = requests.get(context_url)

    if context_request.status_code != 200:
        raise ContextLoaderError('There was an error while loading the context: ' + context_request.text)

    return context_request.json()
