"""
Unit tests for the priorities module.
"""
import unittest
import os
from app.search.properties_priorities.priorities import PropertiesPriorities, EntitiesPriorities


class TestPropertiesPriorities(unittest.TestCase):
    """
    Test case for the priorities module.
    """

    def setUp(self):
        """
        Set up the test case with sample CSV data and expected results.
        """
        self.csv_data = """Entity,Property,Priority
        Compound,molecule_chembl_id,1
        Compound,pref_name,1
        Compound,molecule_synonyms,1
        Compound,molecule_type,2
        Drug,drug_parent_molecule_chembl_id,1
        Drug,pref_name,1
        Drug,molecule_synonyms,1
        """
        self.expected_compound = {
            'molecule_chembl_id': 1,
            'pref_name': 1,
            'molecule_synonyms': 1,
            'molecule_type': 2
        }
        self.expected_drug = {
            'drug_parent_molecule_chembl_id': 1,
            'pref_name': 1,
            'molecule_synonyms': 1
        }
        self.test_csv_path = 'test_priorities.csv'
        with open(self.test_csv_path, 'w') as file:
            file.write(self.csv_data)

    def tearDown(self):
        """
        Clean up the test case by removing the test CSV file.
        """
        os.remove(self.test_csv_path)

    def test_get_priorities_compound(self):
        """
        Test the get_priorities function with the 'Compound' entity.
        """
        properties_priorities = PropertiesPriorities(self.test_csv_path)
        result = properties_priorities.get_priorities('Compound')
        self.assertEqual(result, self.expected_compound)

    def test_get_priorities_drug(self):
        """
        Test the get_priorities function with the 'Drug' entity.
        """
        properties_priorities = PropertiesPriorities(self.test_csv_path)
        result = properties_priorities.get_priorities('Drug')
        self.assertEqual(result, self.expected_drug)

    def test_get_priorities_nonexistent_entity(self):
        """
        Test the get_priorities function with a non-existent entity.
        """
        properties_priorities = PropertiesPriorities(self.test_csv_path)
        result = properties_priorities.get_priorities('NonExistent')
        self.assertEqual(result, {})

    def test_singleton_instance(self):
        """
        Test the singleton instance creation.
        """
        instance1 = PropertiesPriorities.get_instance(self.test_csv_path)
        instance2 = PropertiesPriorities.get_instance(self.test_csv_path)
        self.assertIs(instance1, instance2)


class TestEntitiesPriorities(unittest.TestCase):
    """
    Test case for the EntitiesPriorities module.
    """

    def setUp(self):
        """
        Set up the test case with sample CSV data and expected results.
        """
        self.csv_data = """Entity,Priority
        Compound,1
        Target,2
        Assay,3
        CellLine,4
        Tissue,5
        Document,6
        """
        self.expected_priorities = {
            'Compound': 1,
            'Target': 2,
            'Assay': 3,
            'CellLine': 4,
            'Tissue': 5,
            'Document': 6
        }
        self.test_csv_path = 'test_entities_priorities.csv'
        with open(self.test_csv_path, 'w') as file:
            file.write(self.csv_data)

    def tearDown(self):
        """
        Clean up the test case by removing the test CSV file.
        """
        os.remove(self.test_csv_path)

    def test_get_priority(self):
        """
        Test the get_priority function for various entities.
        """
        entities_priorities = EntitiesPriorities(self.test_csv_path)
        for entity, expected_priority in self.expected_priorities.items():
            result = entities_priorities.get_priority(entity)
            self.assertEqual(result, expected_priority)

    def test_get_priority_nonexistent_entity(self):
        """
        Test the get_priority function with a non-existent entity.
        """
        entities_priorities = EntitiesPriorities(self.test_csv_path)
        result = entities_priorities.get_priority('NonExistent')
        self.assertIsNone(result)

    def test_singleton_instance(self):
        """
        Test the singleton instance creation.
        """
        instance1 = EntitiesPriorities.get_instance(self.test_csv_path)
        instance2 = EntitiesPriorities.get_instance(self.test_csv_path)
        self.assertIs(instance1, instance2)


if __name__ == '__main__':
    unittest.main()
