"""
Module to handle loading and retrieving properties priorities from a CSV file.
"""
from functools import lru_cache
from collections import defaultdict

import numpy as np


class PropertiesPriorities:
    """
    Class to handle loading and retrieving properties priorities from a CSV file.
    """

    _instance = None

    def __init__(self, csv_file_path='app/search/properties_priorities/properties_priorities.csv'):
        """
        Initializes the PropertiesPriorities instance with the given CSV file path.

        :param csv_file_path: Path to the CSV file containing properties and priorities.
        """
        self.csv_file_path = csv_file_path
        self.cache = None
        self._load_data()

    def _load_data(self):
        """
        Loads data from the CSV file into the in-memory cache.
        """
        self.cache = defaultdict(dict)
        data = np.genfromtxt(self.csv_file_path, delimiter=',', dtype=None, names=True, encoding='utf-8')
        for row in data:
            entity = row['Entity']
            property_name = row['Property']
            priority = int(row['Priority'])
            self.cache[entity][property_name] = priority

    @lru_cache
    def get_priorities(self, entity):
        """
        Returns the properties and their priorities for a given entity.

        :param entity: The entity for which to get the properties and priorities.
        :return: A dictionary of properties and their priorities for the given entity.
        """
        if self.cache is None:
            self._load_data()
        return self.cache.get(entity, {})

    @classmethod
    def get_instance(cls, csv_file_path='app/search/properties_priorities/properties_priorities.csv'):
        """
        Returns a singleton instance of the PropertiesPriorities class.

        :param csv_file_path: Path to the CSV file containing properties and priorities.
        :return: A singleton instance of PropertiesPriorities.
        """
        if cls._instance is None:
            cls._instance = cls(csv_file_path)
        return cls._instance


class EntitiesPriorities:
    """
    Class to handle loading and retrieving entities priorities from a CSV file.
    """

    _instance = None

    def __init__(self, csv_file_path='app/search/properties_priorities/entities_priorities.csv'):
        """
        Initializes the EntitiesPriorities instance with the given CSV file path.

        :param csv_file_path: Path to the CSV file containing entities and priorities.
        """
        self.csv_file_path = csv_file_path
        self.cache = None
        self._load_data()

    def _load_data(self):
        """
        Loads data from the CSV file into the in-memory cache.
        """
        self.cache = {}
        data = np.genfromtxt(self.csv_file_path, delimiter=',', dtype=None, names=True, encoding='utf-8')
        for row in data:
            entity = row['Entity']
            priority = int(row['Priority'])
            self.cache[entity] = priority

    @lru_cache
    def get_priority(self, entity):
        """
        Returns the priority for a given entity.

        :param entity: The entity for which to get the priority.
        :return: The priority of the given entity.
        """
        if self.cache is None:
            self._load_data()
        return self.cache.get(entity, None)

    @lru_cache
    def get_num_entities(self):
        """
        Returns the number of entities for which there are priorities set

        :return: the number of entities for which there are priorities set
        """
        return len(self.cache)

    @classmethod
    def get_instance(cls, csv_file_path='app/search/properties_priorities/entities_priorities.csv'):
        """
        Returns a singleton instance of the EntitiesPriorities class.

        :param csv_file_path: Path to the CSV file containing entities and priorities.
        :return: A singleton instance of EntitiesPriorities.
        """
        if cls._instance is None:
            cls._instance = cls(csv_file_path)
        return cls._instance
