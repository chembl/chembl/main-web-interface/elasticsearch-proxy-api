# pylint: disable=too-many-locals
# Remove this ^^^
"""
Module that generates search queries for a term given a configuration
"""
from copy import deepcopy
from functools import cmp_to_key

from app.es_data import es_data
from app.properties_configuration import groups_configuration_manager
from app.es_data import es_mappings
from app.search import autocomplete
from app.search.properties_priorities.priorities import PropertiesPriorities, EntitiesPriorities

TERM_LENGTH_THRESHOLD = 300


def get_search_results(term, search_config):
    """
    :param term: term for which to do the search
    :param search_config: configuration to use for the autocomplete. For example:
    :return: the description of the search results, including the Es queries for each entity
    """

    entities_config = search_config.get('entities', {})

    if len(term) <= TERM_LENGTH_THRESHOLD:
        return get_results_for_shorter_terms(term, entities_config)

    get_results_for_longer_terms(term, entities_config)

    return get_results_for_longer_terms(term, entities_config)


def get_results_for_shorter_terms(term, entities_config):
    """
    Used when the terms are shorter
    :param term: term for which to do the search
    :param entities_config: configuration for the different entities involved
    :return: the description of the search results, including the Es queries for each entity
    """

    entity_keys, m_search_array, clean_queries = get_entity_keys_and_queries_for_multisearch(term, entities_config)
    responses = es_data.do_multisearch(body=m_search_array)['responses']

    results, highest_scoring_entity = get_queries_per_entity(entity_keys, responses, clean_queries)

    results["highlighted_entity"] = {
        'entity': highest_scoring_entity,
        'reason': 'Highest scoring in ES.'
    }

    return results


def get_queries_per_entity(entity_keys, responses, clean_queries):
    """
    :param entity_keys: list of entity keys in the same order as they were included in the multisearch queries
    :param responses: responses from the multisearch
    :param clean_queries: cleaned up queries to be used by the frontend
    :return: the queries and results per entity taken from the search queries built
    """

    results = {
        "entities": {}
    }

    response_index = 0
    absolute_max_score = -1
    highest_scoring_entity = None
    for entity_key in entity_keys:
        current_response = responses[response_index]

        max_score = current_response.get('hits', {}).get('max_score', 0)
        comp_score = -1 if max_score is None else max_score
        item_count = current_response['hits']['total']['value']
        clean_query = clean_queries[response_index]
        results["entities"][entity_key] = {
            "es_query": clean_query,
            "max_score": max_score,
            "item_count": item_count,
        }

        if comp_score > absolute_max_score:
            highest_scoring_entity = entity_key
            absolute_max_score = comp_score

        response_index += 1

    return results, highest_scoring_entity


def get_results_for_longer_terms(term, entities_config):
    """
    Used when the terms are longer, more like a paragraph, see TERM_LENGTH_THRESHOLD
    :param term: term for which to do the search
    :param entities_config: configuration for the different entities involved
    :return: the description of the search results, including the Es queries for each entity
    """

    keys_with_no_fields = get_entity_keys_with_no_fields_for_longer_terms(entities_config)

    entity_keys, m_search_array, clean_queries = get_entity_keys_and_queries_for_multisearch_long_terms(term,
                                                                                                        entities_config)

    responses = es_data.do_multisearch(body=m_search_array)['responses']
    results, highest_scoring_entity = get_queries_per_entity(entity_keys, responses, clean_queries)

    results["highlighted_entity"] = {
        'entity': highest_scoring_entity,
        'reason': 'Highest scoring in ES.'
    }

    for entity_key in keys_with_no_fields:
        results['entities'][entity_key] = {
            'es_query': {},
            "item_count": 0,
            "max_score": 0
        }

    return results


def get_entity_keys_with_no_fields_for_longer_terms(entities_config):
    """

    :param entities_config: configuration of the entities for search
    :return: the keys for which there are no fields configured to be used in longer terms
    """

    group_config_manager = groups_configuration_manager.get_groups_configuration_instance()

    keys_with_no_fields = []

    for entity_key, entity_config in entities_config.items():
        index_name = entity_config['index_name']
        search_properties_group = entity_config['search_properties_group']
        subgroups = 'longer_terms'
        prop_ids = group_config_manager.get_prop_ids_for_config_group(index_name, search_properties_group, subgroups)
        if len(prop_ids) == 0:
            keys_with_no_fields.append(entity_key)

    return keys_with_no_fields


def get_highlighted_entity_from_autocomplete(term):
    """
    :param term: term being searched
    :return: the highest scoring entity  from the autocomplete results, None if no suggestions.
    """
    autocomplete_results = autocomplete.get_autocomplete_results(term)

    no_suggestions = True

    for item in autocomplete_results['entity_suggestions'].items():
        num_suggestions = len(item[1]['suggestions'])
        if num_suggestions > 0:
            no_suggestions = False
            break

    if no_suggestions:
        return None

    suggestions_list = list(autocomplete_results['entity_suggestions'].items())

    def get_score_from_entity_id(entity_id):

        scores = {
            'Compound': 6,
            'EubopenCompound': 6,
            'Target': 5,
            'EubopenTarget': 5,
            'Tissue': 4,
            'CellLine': 3,
            'Assay': 2,
            'Document': 0
        }
        return scores.get(entity_id, 0)

    def compare_tuples(item1, item2):

        max_score1 = item1[1]['max_score']
        max_score2 = item2[1]['max_score']

        max_score_difference = max_score2 - max_score1

        if max_score_difference != 0:
            return max_score_difference

        avg_score1 = item1[1]['avg_score']
        avg_score2 = item2[1]['avg_score']

        avg_score_difference = avg_score2 - avg_score1

        if avg_score_difference != 0:
            return avg_score_difference

        entity_id1 = item1[0]
        entity_score1 = get_score_from_entity_id(entity_id1)

        entity_id2 = item2[0]
        entity_score2 = get_score_from_entity_id(entity_id2)

        entity_score_difference = entity_score2 - entity_score1

        return entity_score_difference

    suggestions_list.sort(key=cmp_to_key(compare_tuples))

    highlighted_entity = suggestions_list[0][0]
    return highlighted_entity


TEXT_FIELDS_BOOSTS = {
    'std_analyzed': '1.6',
    'eng_analyzed': '0.8',
    'ws_analyzed': '1.4',
    'keyword': '2',
    'lower_case_keyword': '1.5',
    'alphanumeric_lowercase_keyword': '1.3'
}

IDS_FIELDS_BOOSTS = {
    'entity_id': '2',
    'id_reference': '1.5',
    'chembl_id': '2',
    'chembl_id_reference': '1.5'
}


def get_basic_search_query(term, entity_key):
    """
    :param term: search term
    :param entity_key: entity key to search
    :return: the resulting query to apply for the search
    """

    entity_boost = get_boost_for_entity(entity_key)
    boosted_fields = get_boosted_fields(entity_key)

    query = {
        "query": {
            "bool": {
                "must": {
                    "bool": {
                        "should": [
                            {
                                "multi_match": {
                                    "boost": 600 * entity_boost,
                                    "fields": boosted_fields,
                                    "query": term,
                                    "type": "cross_fields",
                                    "operator": "or"
                                }
                            },
                            {
                                "multi_match": {
                                    "boost": 700 * entity_boost,
                                    "fields": boosted_fields,
                                    "fuzziness": "AUTO",
                                    "query": term,
                                    "minimum_should_match": "100%",
                                    "type": "best_fields",
                                    "operator": "and"
                                }
                            },
                            {
                                "multi_match": {
                                    "boost": 800 * entity_boost,
                                    "fields": boosted_fields,
                                    "query": term,
                                    "type": "cross_fields",
                                    "operator": "and"
                                }
                            },
                            {
                                "multi_match": {
                                    "boost": 900 * entity_boost,
                                    "fields": boosted_fields,
                                    "fuzziness": 0,
                                    "query": term,
                                    "minimum_should_match": "100%",
                                    "type": "best_fields",
                                    "operator": "and"
                                }
                            },
                            {
                                "multi_match": {
                                    "boost": 1000 * entity_boost,
                                    "fields": boosted_fields,
                                    "query": term,
                                    "type": "phrase",
                                }
                            },
                            {
                                "multi_match": {
                                    "boost": 1000 * entity_boost,
                                    "fields": get_generic_id_analysers_with_boosts(),
                                    "fuzziness": 0,
                                    "query": term,
                                    "type": "most_fields"
                                }
                            }

                        ]
                    }
                }

            }
        },

    }

    return query


def get_basic_search_query_for_longer_terms(term, available_fields):
    """
    :param term: search term
    :param available_fields: available fields to search
    :return: the resulting query to apply for the search
    """

    query = {
        "query": {
            "bool": {
                "should": [
                    {
                        "multi_match": {
                            "fields": get_text_fields_with_boosts(available_fields),
                            "fuzziness": "AUTO",
                            "query": term,
                        }
                    },

                ]
            }
        },

    }

    return query


def get_ids_fields_with_boosts(available_fields):
    """
    :param available_fields: available fields per property
    :return: the fields with boosts ready to be used on the query
    """
    return get_fields_with_boosts(available_fields, IDS_FIELDS_BOOSTS)


def get_text_fields_with_boosts(available_fields, only_fields=None):
    """
    :param available_fields: available fields per property
    :param only_fields: use it to limit the fields that you want to allow
    :return: the fields with boosts ready to be used on the query
    """
    return get_fields_with_boosts(available_fields, TEXT_FIELDS_BOOSTS, only_fields)


def get_fields_with_boosts(available_fields, boosts_dict, only_fields=None):
    """
    :param available_fields: available fields per property
    :param boosts_dict: boosts dict to use
    :param only_fields: use it to limit the fields that you want to allow
    :return: the fields with boosts ready to be used on the query
    """
    query_fields = []
    specific_fields = only_fields if only_fields is not None else []
    for prop_id, fields in available_fields.items():
        for field in fields:
            field_accepted = len(specific_fields) == 0 or field in specific_fields
            if field_accepted:
                field_boost = boosts_dict.get(field)
                if field_boost is not None:
                    query_fields.append(f'{prop_id}.{field}^{field_boost}')

    return query_fields


def get_text_fields_with_all_analysers(available_fields):
    """
    :param available_fields: available fields per property
    :return: the fields with all analysers this means .*
    """
    query_fields = []

    for prop_id, fields in available_fields.items():
        for field in fields:
            field_boost = TEXT_FIELDS_BOOSTS.get(field)
            if field_boost is not None:
                query_fields.append(f'{prop_id}.*')

    # make sure the items are unique
    query_fields = list(set(query_fields))

    return query_fields


def get_boost_for_entity(entity_key):
    """
    :param entity_key: entity key for which to get the boost
    :return: the corresponding boost for the entity
    """
    entities_priorities = EntitiesPriorities.get_instance()
    priority = entities_priorities.get_priority(entity_key)
    num_entities = entities_priorities.get_num_entities()
    # if priority is None, it means it has not been set, so it is the last priority
    if priority is None:
        priority = num_entities
    return num_entities - priority + 1


def get_boosted_fields(entity_key):
    """
    :param entity_key: uses the priorities module to get the fields boosted by their priority
    :return: a list of fields with their priority in the ES format
    """
    properties_priorities = PropertiesPriorities.get_instance()
    priority_dict = properties_priorities.get_priorities(entity_key)

    # if entity_key is Compound, also include the fields from Drug
    if entity_key == 'Compound':
        drug_priority_dict = properties_priorities.get_priorities('Drug')
        priority_dict.update(drug_priority_dict)

    # if there are no priorities set, return all fields with the same boost
    if len(priority_dict) == 0:
        return ['*^1']

    # add one to all priorities because 1 is the default
    max_priority = max(priority_dict.values()) + 1
    boosted_fields = [
        f"{field}^{2 ** (max_priority - priority)}"
        for field, priority in priority_dict.items()
    ]
    # add the rest of the fields with normal boost
    boosted_fields.append('*^1')
    return boosted_fields


def get_generic_text_analysers_with_boosts():
    """
    :return: the texts analysers with the corresponding boosts
    """
    fields = []
    for field_key, boost in TEXT_FIELDS_BOOSTS.items():
        fields.append(f'*.{field_key}^{boost}')
    return fields


def get_generic_id_analysers_with_boosts():
    """
    :return: the texts analysers with the corresponding boosts
    """
    fields = []
    for field_key, boost in IDS_FIELDS_BOOSTS.items():
        fields.append(f'*.{field_key}^{boost}')
    return fields


def get_entity_keys_and_queries_for_multisearch(term, entities_config):
    """
    :param term: term to search
    :param entities_config: configuration of the entities
    :return: the entity keys and search array to be used in the multisearch
    """

    # group_config_manager = groups_configuration_manager.get_groups_configuration_instance()

    entity_keys = []
    m_search_array = []
    clean_queries = []

    for entity_key, entity_config in entities_config.items():
        entity_keys.append(entity_key)
        index_name = entity_config['index_name']

        # search_properties_group = entity_config['search_properties_group']
        # props_list = group_config_manager.get_prop_ids_for_config_group(index_name, search_properties_group)
        # available_fields = es_mappings.get_available_fields_for_props_list(index_name, props_list)

        # The clean query does not have parameters like _source or track_total hits, that is up to the front end to set
        clean_query = get_basic_search_query(term, entity_key)
        search_query = deepcopy(clean_query)
        search_query["_source"] = False
        search_query["track_total_hits"] = True

        m_search_array.append({'index': index_name})
        m_search_array.append(search_query)

        clean_queries.append(clean_query)

    return entity_keys, m_search_array, clean_queries


def get_entity_keys_and_queries_for_multisearch_long_terms(term, entities_config):
    """
    :param term: term to search
    :param entities_config: configuration of the entities
    :return: the entity keys and search array to be used in the multisearch
    """

    group_config_manager = groups_configuration_manager.get_groups_configuration_instance()

    entity_keys = []
    m_search_array = []
    clean_queries = []

    for entity_key, entity_config in entities_config.items():

        index_name = entity_config['index_name']

        search_properties_group = entity_config['search_properties_group']
        subgroups = ['longer_terms']
        props_list = group_config_manager.get_prop_ids_for_config_group(index_name, search_properties_group, subgroups)
        if len(props_list) == 0:
            continue

        entity_keys.append(entity_key)
        available_fields = es_mappings.get_available_fields_for_props_list(index_name, props_list)

        clean_query = get_basic_search_query_for_longer_terms(term, available_fields)
        search_query = deepcopy(clean_query)
        search_query["_source"] = False
        search_query["track_total_hits"] = True

        m_search_array.append({'index': index_name})
        m_search_array.append(search_query)

        clean_queries.append(clean_query)

    return entity_keys, m_search_array, clean_queries
