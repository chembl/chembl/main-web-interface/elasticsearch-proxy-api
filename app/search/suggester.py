"""
Module that generates suggestions for a term given a configuration
"""
from app.es_data import es_data
from app.properties_configuration import properties_configuration_manager
from app.properties_configuration import groups_configuration_manager

from utils import dict_property_access


def get_suggestions_for_term(term, autocomplete_config):
    """
    :param term: term for which to do the suggestion
    :param autocomplete_config: configuration to use for the autocomplete. For example:
    EubopenCompound:
      index_name: 'chembl_eubopen_molecule'
      label_property: 'pref_name'
      highlight_properties_group: 'highlight_properties'
      size: 3
    EubopenTarget:
      index_name: 'chembl_eubopen_target'
      label_property: 'pref_name'
      highlight_properties_group: 'highlight_properties'
      size: 3
    :return: the results for the suggestion query
    """

    suggestions_dict = get_all_suggestions(term, autocomplete_config)

    entity_suggestions = {}
    for entity_key, entity_config in autocomplete_config.items():
        suggestions, max_score, avg_score = get_suggestions(entity_key, entity_config, suggestions_dict)
        entity_suggestions[entity_key] = {
            'suggestions': suggestions,
            'max_score': max_score,
            'avg_score': avg_score
        }

    return {
        'entity_suggestions': entity_suggestions,
    }


def get_suggestions(entity_key, entity_config, suggestions_dict):
    """
    :param entity_key: entity key for which to get suggestions
    :param entity_config: config of the entity for the suggestions
    :param suggestions_dict: dictionary of suggestions obtained from the multisearch
    :return: the suggestions found
    """
    raw_text_suggestions = suggestions_dict[entity_key]
    raw_options = raw_text_suggestions["suggest"]["autocomplete"][0]["options"]
    scores = [raw_option.get("_score", 0) for raw_option in raw_options]

    max_score = max(scores) if len(scores) > 0 else 0
    avg_score = sum(scores) / len(scores) if len(scores) > 0 else 0

    suggestions = [parse_option(option, entity_config) for option in raw_options]
    return suggestions, max_score, avg_score


def get_all_suggestions(term, autocomplete_config):
    """
    :param term: term for which to get suggestions
    :param autocomplete_config: dictionary with the configuration of the entities
    :return: all the suggestions from multisearch
    """

    entity_keys = []
    m_search_array = []

    for entity_key, entity_config in autocomplete_config.items():
        entity_keys.append(entity_key)
        index_name = entity_config['index_name']
        suggestion_query = get_suggestion_query(term, entity_config)

        m_search_array.append({'index': index_name})
        m_search_array.append(suggestion_query)

    responses = es_data.do_multisearch(body=m_search_array)['responses']

    responses_dict = {}
    response_index = 0
    for entity_key in entity_keys:
        responses_dict[entity_key] = responses[response_index]
        response_index += 1

    return responses_dict


def get_suggestion_query(term, entity_config):
    """
    :param term: term for the suggestions
    :param entity_config: configuration of the entity
    :return: the query for the suggetions for a particular entity
    """
    index_name = entity_config['index_name']
    group_config_manager = groups_configuration_manager.get_groups_configuration_instance()
    highlight_properties_group = entity_config['highlight_properties_group']
    candidate_property_paths = group_config_manager.get_prop_ids_for_config_group(index_name,
                                                                                  highlight_properties_group)
    size = entity_config['size']
    suggestion_query = {
        'size': 0,
        '_source': candidate_property_paths,
        'suggest': {
            'autocomplete': {
                'prefix': term,
                'completion': {
                    'field': '_metadata.es_completion',
                    'size': size
                }
            }
        },
        "track_total_hits": False
    }

    return suggestion_query


def parse_option(raw_option, entity_config):
    """
    :param raw_option: raw option to parse
    :param entity_config: configuration of the entity autocomplete
    :return: a simplified version of the suggestion.
    """
    label_property = entity_config['label_property']
    highlight_properties_group = entity_config['highlight_properties_group']
    index_name = entity_config['index_name']

    text = raw_option['text']

    highlighted_property_label = None
    highlighted_property_path = get_highlighted_property(raw_option['_source'], highlight_properties_group, index_name,
                                                         text)
    if highlighted_property_path is not None:
        index_name = entity_config['index_name']
        props_config_instance = properties_configuration_manager.get_property_configuration_instance()
        highlighted_property_config = props_config_instance.get_config_for_prop(index_name, highlighted_property_path)
        highlighted_property_label = highlighted_property_config['label']

    return {
        '_id': raw_option['_id'],
        'item_label': dict_property_access.get_property_value(raw_option, f'_source.{label_property}'),
        'text': text,
        'highlighted_property': highlighted_property_label
    }


def get_highlighted_property(doc_source, highlight_properties_group, index_name, text):
    """
    :param doc_source: source for the document to check
    :param highlight_properties_group: group with the properties that are candidates for highlighting
    :param index_name: name of the index, used for getting the config group
    :param text: text matched
    """
    group_config_manager = groups_configuration_manager.get_groups_configuration_instance()
    highlight_group = group_config_manager.get_config_for_group(index_name, highlight_properties_group)
    candidate_property_paths = [prop['prop_id'] for prop in highlight_group['properties']['default']]

    for property_path in candidate_property_paths:
        value = str(dict_property_access.get_property_value(doc_source, property_path))
        if text in value:
            return property_path

    return None
