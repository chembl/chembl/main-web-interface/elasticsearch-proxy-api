"""
Functions to get the autocomplete
"""
import base64

from app.cache.decorators import return_if_cached_results
from app.config import RUN_CONFIG
from app.search import suggester


def get_cache_key(term):
    """
    :param term: term for which to get autocomplete results
    :return: a cache key for the autocomplete tests
    """
    return f'autocomplete-{str(base64.b64encode(bytes(term, "utf-8")))}'


@return_if_cached_results({
    'cache_key_generator': get_cache_key,
    'timeout': 3600
})
def get_autocomplete_results(term):
    """
    :param term: term to autocomplete
    :return: the results of the autocomplete for the term indicated
    """
    autocomplete_config = RUN_CONFIG.get('search', {}).get('suggestions', {}).get('entities', {})

    return suggester.get_suggestions_for_term(term, autocomplete_config)
