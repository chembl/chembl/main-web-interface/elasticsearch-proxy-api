"""
Module that handles the free text search in EUbOPEN
"""

from app.config import RUN_CONFIG
from app.cache.decorators import return_if_cached_results
from app.search import free_text_searcher
from utils import escaping


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'ChEMBL-search-text-{args[0]}',
    'timeout': RUN_CONFIG.get('es_proxy_cache_seconds')
})
def get_search_results(term):
    """
    :param term: search term
    :return: the description of the search results, including the Es queries for each entity
    """
    search_config = RUN_CONFIG.get('search', {}).get('free_text', {})

    return free_text_searcher.get_search_results(escaping.unsanitise_slash(term), search_config)


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'EUbOPEN-search-text-{args[0]}',
    'timeout': RUN_CONFIG.get('es_proxy_cache_seconds')
})
def get_eubopen_search_results(term):
    """
    :param term: search term
    :return: the description of the search results, including the Es queries for each entity
    """
    search_config = RUN_CONFIG.get('eubopen', {}).get('search', {}).get('free_text', {})

    return free_text_searcher.get_search_results(escaping.unsanitise_slash(term), search_config)
