"""
Module with the functions to generate suggestions for eubopen
"""
from app.config import RUN_CONFIG
from app.cache.decorators import return_if_cached_results
from app.search import suggester
from utils import escaping


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'EUbOPEN-search-suggestion-{kwargs.get("term", args[0])}',
    'timeout': RUN_CONFIG.get('es_proxy_cache_seconds')
})
def get_suggestions_for_term(term):
    """
    :param term: term for which to do the suggestion
    :return: the results for the suggestion query
    """
    autocomplete_config = RUN_CONFIG.get('eubopen', {}).get('search', {}).get('suggestions', {}).get('entities', {})
    return suggester.get_suggestions_for_term(escaping.unsanitise_slash(term), autocomplete_config)
