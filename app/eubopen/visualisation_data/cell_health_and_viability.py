"""
Module that handles the obtaining the cell health and viability data for the multiplex assay data of a compound
"""
from enum import Enum

import numpy

from app.es_data import es_data


class CellHealthAndViabilityError(Exception):
    """
    Class for errors in this module
    """


class OutputFormats(Enum):
    """
    Possible output formats for this module
    """
    JSON = 'JSON'
    CSV = 'CSV'


def get_cell_viability_and_health_data(compound_id, output_format=OutputFormats.JSON):
    """
    :param compound_id: id of the compound
    :param output_format: desired format of the output
    :return: the dict with cell viability and health data of the compound with the id given
    """
    possible_formats = OutputFormats

    if output_format not in possible_formats:
        raise CellHealthAndViabilityError(
            f'The format {output_format} is not one of the possible formats ({possible_formats}).')

    response_data = process_activities(compound_id)
    if output_format is OutputFormats.JSON:
        return response_data

    csv_response = get_csv_response(response_data)

    return csv_response


def get_csv_response(response_data):
    """
    :param response_data: the dict with cell viability and health data of the compound with the id given
    :return: the data in a CSV format, as a string
    """
    # pylint:disable=R0914
    # pylint:disable=R1702
    # pylint:disable=R0912

    props_keys = list(ACTIVITY_TYPES_CONFIG.keys())
    header = ['concentration', 'cell_chembl_id', 'cell_name', 'time_point', 'assay_chembl_id', 'replicate_number',
              *props_keys, 'img_url']

    rows = []
    for concentration_key, con_data in response_data['concentrations'].items():

        for cell_chembl_id, cell_data in con_data['dataPerCell'].items():
            image_urls = cell_data['imageURLS']
            cell_name = cell_data['name']
            properties = cell_data['properties']

            # first, the data needs to be rearranged to be able to isolate rows
            data_per_time_point = {}
            for prop_key in props_keys:
                prop_data = properties[prop_key]
                for time_point_key, time_point_data in prop_data['byTimePoints'].items():
                    for replicate_assay_id, replicate_data in time_point_data['byReplicates'].items():

                        replicate_number = replicate_data['replicate_number']
                        standard_value = replicate_data.get('standard_value', '---')
                        standard_units = replicate_data.get('standard_units', '')

                        if data_per_time_point.get(time_point_key) is None:
                            data_per_time_point[time_point_key] = {}

                        if data_per_time_point[time_point_key].get(replicate_assay_id) is None:
                            data_per_time_point[time_point_key][replicate_assay_id] = {}

                        if data_per_time_point[time_point_key][replicate_assay_id].get(replicate_number) is None:
                            data_per_time_point[time_point_key][replicate_assay_id][replicate_number] = {}

                        data_per_time_point[time_point_key][replicate_assay_id][replicate_number][prop_key] = {
                            'standard_value': standard_value,
                            'standard_units': standard_units
                        }

            for time_point_key, time_point_data in data_per_time_point.items():
                for assay_chembl_id, assay_data in time_point_data.items():
                    for replicate_number, replicate_data in assay_data.items():

                        row = [concentration_key, cell_chembl_id, cell_name, time_point_key, assay_chembl_id,
                               replicate_number]

                        for prop_key in header[6:6 + len(props_keys)]:
                            prop_data = replicate_data[prop_key]
                            standard_value = prop_data['standard_value']
                            standard_value = '---' if standard_value is None else standard_value
                            standard_units = prop_data['standard_units']
                            standard_units = '' if standard_units is None else standard_units
                            full_value = f'{standard_value} {standard_units}'.strip()
                            row.append(full_value)

                        img_url = image_urls[time_point_key]['byReplicates'][assay_chembl_id]['img_url']
                        row.append(img_url)
                        rows.append(row)

    def encapsulate(text):
        return f'"{text}"'

    final_str = f'{",".join([encapsulate(head) for head in header])}\n'
    for row in rows:
        final_str += f'{",".join([encapsulate(cell) for cell in row])}\n'
    return final_str


def process_activities(compound_id):
    """
    Processes the activities corresponding to the compound with id indicated as parameter
    :param compound_id: compound id to query
    :return: the structure required for the visualisation.
    """
    # pylint:disable=R0914
    response_data = {
        'concentrations': {},
        'num_activities_processed': 0,
        'num_datapoints': 0
    }

    index_name = 'chembl_eubopen_activity'
    query = get_activities_query(compound_id)
    acts_scanner = es_data.get_es_scanner(index_name, query)

    for act_i in acts_scanner:
        act_source = act_i['_source']
        activity_id = act_i['_id']
        activity_type = act_source.get('standard_type')

        allowed_type = activity_type in ACTIVITY_TYPES_CONFIG.keys()
        if not allowed_type:
            continue

        time_point = get_time_point(act_source)
        if time_point is None:
            continue
        allowed_time_point = time_point in ALLOWED_TIMEPOINTS
        if not allowed_time_point:
            continue

        concentration = get_concentration(act_source)
        if concentration is None:
            continue
        add_concentration_if_necessary(response_data, concentration)

        cell_line_chembl_id = get_cell_line_chembl_id(act_source)
        add_cell_line_if_necessary(response_data, concentration, cell_line_chembl_id)

        assay_chembl_id = get_assay_chembl_id(act_source)
        replicate_number = get_replicate_number(act_source)

        img_url = get_img_url(act_source)

        addition_params = {
            'concentration': concentration,
            'cell_line_chembl_id': cell_line_chembl_id,
            'time_point': time_point,
            'assay_chembl_id': assay_chembl_id,
            'replicate_number': replicate_number,
            'activity_id': activity_id,
        }

        if img_url is not None:
            add_img_url(response_data, addition_params, img_url)

        activity_value = float(act_source.get('standard_value'))
        activity_units = act_source.get('standard_units')

        add_activity_value(response_data, addition_params, activity_type, activity_value, activity_units)
        response_data['num_activities_processed'] += 1

    calculate_summaries(response_data)

    return response_data


ALLOWED_TIMEPOINTS = ['0.0 hours', '12.0 hours', '24.0 hours', '48.0 hours']

ACTIVITY_TYPES_CONFIG = {
    'Control DMSO Apoptotic Cells (%)': {
        'label': 'Control DMSO Apoptotic Cells (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Control DMSO Fragmented Nuclei %': {
        'label': 'Control DMSO Fragmented Nuclei %',
        'show': False,
        'isCellHealth': True,
    },
    'Control DMSO Growth Rate': {
        'label': 'Control DMSO Growth Rate',
        'show': False,
        'isCellHealth': False,
    },
    'Control DMSO Healthy Nuclei (%)': {
        'label': 'Control DMSO Healthy Nuclei (%)',
        'show': False,
        'isCellHealth': True,
    },
    'Control DMSO Membrane Permeable-Phenotype Cells (%)': {
        'label': 'Control DMSO Membrane Permeable-Phenotype Cells (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Control DMSO Mitochondria Different-Phenotype Cells (%)': {
        'label': 'Control DMSO Mitochondria Different-Phenotype Cells (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Control DMSO Mitotic Cells (%)': {
        'label': 'Control DMSO Mitotic Cells (%)',
    },
    'Control DMSO Pyknosed Nuclei (%)': {
        'label': 'Control DMSO Pyknosed Nuclei (%)',
        'show': False,
        'isCellHealth': True,
    },
    'Control DMSO Total Cells': {
        'label': 'Control DMSO Total Cells',
        'show': False,
        'isCellHealth': False,
    },
    'Control DMSO Tubulin Phenotype Different Cells (%)': {
        'label': 'Control DMSO Tubulin Phenotype Different Cells (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Population Apoptotic (%)': {
        'label': 'Population Apoptotic (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Population Fragmented Nuclei (%)': {
        'label': 'Population Fragmented Nuclei (%)',
        'show': True,
        'isCellHealth': True,
    },
    'Population Healthy Nuclei (%)': {
        'label': 'Population Healthy Nuclei (%)',
        'show': True,
        'isCellHealth': True,
    },
    'Population Hoechst High Intensity Objects (%)': {
        'label': 'Population Hoechst High Intensity Objects (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Population Membrane Permeable-Phenotype (%)': {
        'label': 'Population Membrane Permeable-Phenotype (%)',
        'show': True,
        'isCellHealth': False,
    },
    'Population Mitochondria Different-Phenotype (%)': {
        'label': 'Population Mitochondria Different-Phenotype (%)',
        'show': True,
        'isCellHealth': False,
    },
    'Population Mitotic Cells (%)': {
        'label': 'Population Mitotic Cells (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Population Normal (%)': {
        'label': 'Population Normal (%)',
        'show': False,
        'isCellHealth': False,
    },
    'Population Pyknosed Nuclei (%)': {
        'label': 'Population Pyknosed Nuclei (%)',
        'show': True,
        'isCellHealth': True,
    },
    'Population Tubulin-Different-Phenotype (%)': {
        'label': 'Population Tubulin-Different-Phenotype (%)',
        'show': True,
        'isCellHealth': False,
    },
    'Total Cell Count': {
        'label': 'Total Cell Count',
    }
}


def get_activities_query(compound_id):
    """
    :param compound_id: compound id to query
    :return: the query to be used to get the activities
    """
    return {
        "query": {
            "terms": {
                "molecule_chembl_id": [
                    compound_id
                ]
            }
        }
    }


def get_concentration(activity_source):
    """
    :param activity_source: document source with the activity to process
    :return: the corresponding concentration
    """
    activity_properties = activity_source.get('activity_properties')
    concentration_property = next(
        (act_prop for act_prop in activity_properties if act_prop['standard_type'] == 'Compound concentration'), None)

    if concentration_property is None:
        return None

    units = concentration_property["units"].replace('uM', 'µM')
    concentration = f'{concentration_property["standard_value"]} {units}'

    return concentration


def get_time_point(activity_source):
    """
    :param activity_source: document source with the activity to process
    :return: the time point
    """
    activity_properties = activity_source.get('activity_properties')
    concentration_property = next(
        (act_prop for act_prop in activity_properties if act_prop['standard_type'] == 'Timepoint'), None)
    if concentration_property is None:
        return None
    concentration = f'{concentration_property["standard_value"]} {concentration_property["units"]}'

    return concentration


def get_replicate_number(activity_source):
    """
    :param activity_source: document source with the activity to process
    :return: the replicate number from the activity data
    """
    assay_parameters = activity_source.get('_metadata').get('assay_data').get('assay_parameters')
    if assay_parameters is None:
        return 'N/A'
    if len(assay_parameters) == 0:
        return 'N/A'
    assay_param = next(
        ass_param for ass_param in assay_parameters if ass_param['standard_type'] == 'Number of replicates')

    replicate_number = assay_param["standard_value"]

    return replicate_number


def get_img_url(activity_source):
    """
    :param activity_source: document source with the activity to process
    :return: the img url from the activity data
    """
    activity_properties = activity_source.get('activity_properties')
    concentration_property = next(
        (act_prop for act_prop in activity_properties if
         act_prop['standard_type'] in ['BioImage Archive URI', 'BioImage Archive URL']), None)

    if concentration_property is None:
        return None
    img_url = concentration_property["standard_text_value"]

    return img_url


def get_cell_line_chembl_id(activity_source):
    """
    :param activity_source: ES document source with the activity to process
    :return: the cell line chembl id related to the activity
    """
    return activity_source.get('_metadata', {}).get('assay_data', {}).get('cell_chembl_id')


def get_assay_chembl_id(activity_source):
    """
    :param activity_source: ES document source with the activity to process
    :return: the assay chembl id related to the activity
    """
    return activity_source.get('assay_chembl_id')


def add_concentration_if_necessary(response_data, concentration):
    """
    :param response_data: response data until now, will be modified
    :param concentration: concentration to add if it does not exist
    """
    if response_data['concentrations'].get(concentration) is None:
        response_data['concentrations'][concentration] = {
            'dataPerCell': {}
        }


def add_cell_line_if_necessary(response_data, concentration, cell_line_chembl_id):
    """
    :param response_data: response data until now, will be modified
    :param concentration: concentration to add if it does not exist
    :param cell_line_chembl_id: cell_line_chembl_id to add if it does not exist
    """
    if response_data['concentrations'][concentration]['dataPerCell'].get(cell_line_chembl_id) is None:
        cell_line_doc = es_data.get_es_doc('chembl_cell_line', cell_line_chembl_id)
        cell_line_name = cell_line_doc['_source']['cell_name']

        response_data['concentrations'][concentration]['dataPerCell'][cell_line_chembl_id] = {
            'name': cell_line_name,
            'properties': {},
            'imageURLS': {}
        }


def add_img_url(response_data, img_params, img_url):
    """
    Adds the imate url to the location indicated by the parameters
    :param response_data: response data until now, will be modified
    :param img_params: img params with concentration, cell_line_chembl_id, assay_chembl_id, replicate_number,
    activity_id
    :param img_url: img url to add

    """
    concentration = img_params['concentration']
    cell_line_chembl_id = img_params['cell_line_chembl_id']
    time_point = img_params['time_point']
    assay_chembl_id = img_params['assay_chembl_id']
    replicate_number = img_params['replicate_number']
    activity_id = img_params['activity_id']

    img_urls = response_data['concentrations'][concentration]['dataPerCell'][cell_line_chembl_id]['imageURLS']

    if img_urls.get(time_point) is None:
        img_urls[time_point] = {
            'byReplicates': {}
        }

    by_replicates = img_urls[time_point]['byReplicates']

    if by_replicates.get(assay_chembl_id) is None:
        by_replicates[assay_chembl_id] = {
            'replicate_number': replicate_number,
            'img_url': img_url,
            'activity_ids': []
        }

    by_replicates[assay_chembl_id]['activity_ids'].append(activity_id)


def add_activity_value(response_data, addition_params, activity_type, activity_value, activity_units):
    """
    Adds the activity value to the location specified
    :param response_data: response data until now, will be modified
    :param addition_params: img params with concentration, cell_line_chembl_id, assay_chembl_id, replicate_number,
    activity_id
    :param activity_type: type of activity to add
    :param activity_value: value of the activity to add
    :param activity_units: unist for the value to add
    """
    concentration = addition_params['concentration']
    cell_line_chembl_id = addition_params['cell_line_chembl_id']
    time_point = addition_params['time_point']
    assay_chembl_id = addition_params['assay_chembl_id']
    replicate_number = addition_params['replicate_number']
    activity_id = addition_params['activity_id']

    dest_properties = response_data['concentrations'][concentration]['dataPerCell'][cell_line_chembl_id]['properties']

    if dest_properties.get(activity_type) is None:
        dest_properties[activity_type] = {
            'config': ACTIVITY_TYPES_CONFIG.get(activity_type),
            'byTimePoints': {}
        }

    by_time_points = dest_properties[activity_type]['byTimePoints']

    if by_time_points.get(time_point) is None:
        by_time_points[time_point] = {
            'byReplicates': {},
            'summary': {}
        }

    by_replicates = by_time_points[time_point]['byReplicates']

    if by_replicates.get(assay_chembl_id) is None:
        by_replicates[assay_chembl_id] = {
            'replicate_number': replicate_number,
            'activity_id': activity_id,
            'standard_value': activity_value,
            'standard_units': activity_units
        }


def calculate_summaries(response_data):
    """
    Calculates summary values (avg, std deviation) for the activity values obtained
    :param response_data: response data build from the activites
    """
    # pylint:disable=R0914,W0612
    num_datapoints = 0
    for con_key, con_value in response_data['concentrations'].items():
        for cell_line_chembl_id, cell_line_data in con_value['dataPerCell'].items():
            for prop_key, prop_data in cell_line_data['properties'].items():
                for time_point_key, time_point_data in prop_data['byTimePoints'].items():

                    summary_destination = time_point_data['summary']
                    values = []
                    for assay_chembl_id, replicate_data in time_point_data['byReplicates'].items():
                        values.append(replicate_data['standard_value'])
                        num_datapoints += 1

                    avg = numpy.average(values)
                    std = numpy.std(values)

                    summary_destination['avg'] = avg
                    summary_destination['std'] = std

    response_data['num_datapoints'] = num_datapoints
