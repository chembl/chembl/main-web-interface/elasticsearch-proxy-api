"""
Module that gets the privacy notice data for the website
"""
import requests
from app.cache.decorators import return_if_cached_results


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'EUbOPEN-privacy_notice',
    'timeout': 3600
})
def get_privacy_notice_data():
    """
    :return: the privacy notice data for the eubopen website
    """

    notice_url = 'https://www.ebi.ac.uk/data-protection/privacy-notice/json/7011ed31-9dd7-44ef-b323-6d9d71511e65'
    return requests.get(notice_url).json()
