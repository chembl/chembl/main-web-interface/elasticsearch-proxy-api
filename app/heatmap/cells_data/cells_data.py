"""
Module that gets the cells data depending on the configuration
"""
from app.standardisation import entities
from app.heatmap.cells_data.cells_loaders.compound_target_activity_cells import CompoundTargetActivityCells, \
    CompoundTargetActivityContextualData
from app.heatmap.standartisation import HeatmapAxes


class CellsDataException(Exception):
    """
    Exception for the cells data module
    """


def get_cells_data(x_axis_ids, y_axis_ids, descriptor, pagination_params):
    """
    :param x_axis_ids: ids in the x-axis
    :param y_axis_ids: ids in the y-axis
    :param pagination_params: parameters with the positions of the current page,
    {x_items_from, x_size, y_items_from, y_size}
    :param descriptor: descriptor of the heatmap configuration
    :return: the cells data corresponding to the configuration and the ids in the x and y axis
    """
    print('get_cells_data!!')

    cells_axis_config = descriptor[HeatmapAxes.CELLS.value]

    cells_entity_id = cells_axis_config['entityID']
    parsed_cells_entity_id = entities.EntityIDs(cells_entity_id)

    # use the same loader for the contextual data for chembl and eubopen for now, they are almost the same.
    if parsed_cells_entity_id in [entities.EntityIDs.ACTIVITY, entities.EntityIDs.EUBOPEN_ACTIVITY]:
        loader = CompoundTargetActivityCells(x_axis_ids=x_axis_ids, y_axis_ids=y_axis_ids,
                                             descriptor=descriptor)
        return loader.get_quadrant_data(x_items_from=pagination_params['x_items_from'],
                                        x_size=pagination_params['x_size'],
                                        y_items_from=pagination_params['y_items_from'],
                                        y_size=pagination_params['y_size'])
    raise CellsDataException(
        f'No configuration found for getting the cells data of the entityID: {cells_entity_id}')


def get_contextual_data(x_axis_config, y_axis_config, cells_axis_config):
    """
    :param x_axis_config: configuration for the x-axis
    :param y_axis_config: configuration for the y-axis
    :param cells_axis_config: configuration of the cells axis.
    :return: the cells data corresponding to the configuration and the ids in the x and y axis
    """

    cells_entity_id = cells_axis_config['entityID']
    parsed_cells_entity_id = entities.EntityIDs(cells_entity_id)
    # use the same loader for the contextual data for chembl and eubopen for now, they are almost the same.
    if parsed_cells_entity_id in [entities.EntityIDs.ACTIVITY, entities.EntityIDs.EUBOPEN_ACTIVITY]:
        loader = CompoundTargetActivityContextualData(x_axis_config=x_axis_config, y_axis_config=y_axis_config,
                                                      cells_axis_config=cells_axis_config)
        return loader.get_contextual_data()

    raise CellsDataException(
        f'No configuration found for getting the contextual data of the entityID: {cells_entity_id}')
