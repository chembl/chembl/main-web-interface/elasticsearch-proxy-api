"""
Class that defines the base class to get the cells data
"""
import abc

from app.es_data import es_data
from app.standardisation import entities
from app.heatmap import standartisation
from app.heatmap.standartisation import HeatmapAxes


class CellsDataLoader:
    """
    base class that defines the basic structure for loading the data
    """

    def __init__(self, x_axis_ids, y_axis_ids, descriptor):
        """
        Constructor of the class
        :param x_axis_ids: x ids of the quadrant to get
        :param y_axis_ids: y ids of the quadrant to get
        :param descriptor: configuration of the heatmap
        """
        self.x_axis_ids = x_axis_ids
        self.y_axis_ids = y_axis_ids
        self.descriptor = descriptor
        self.cells_axis_config = descriptor[HeatmapAxes.CELLS.value]

        # set these in the specific implementations
        self.x_axis_id_property = None
        self.y_axis_id_property = None

    def get_index_name(self):
        """
        :return: The index name to be used in the query
        """
        cells_entity_id = self.cells_axis_config['entityID']
        index_name = entities.get_index_name(cells_entity_id)
        return index_name

    @abc.abstractmethod
    def get_quadrant_data(self, x_items_from, x_size, y_items_from, y_size):
        """
       :param x_items_from: start of the data window to retrieve on the x-axis, to calculate the correct
       position numbers.
       :param x_size: size of the data window to retrieve on the x-axis, to calculate the correct position numbers.
       :param y_items_from: start of the data window to retrieve on the y-axis, to calculate the correct
       position numbers.
       :param y_size: size of the data window to retrieve on the y-axis, to calculate the correct position numbers.
       :return: The data extracted for the corresponding quadrant
       """

    def get_query(self):
        """
        :return: The query to use to get the raw data of the quadrant
        """
        num_x_ids = len(self.x_axis_ids)
        num_y_ids = len(self.y_axis_ids)
        max_num_cells = num_x_ids * num_y_ids
        # must be greater than 0. If not, the query will fail.
        # This prevents errors for quadrants where there are no cells
        max_num_cells = max(max_num_cells, 1)

        return {
            "_source": [
                self.x_axis_id_property,
                self.y_axis_id_property
            ],
            "query": {
                "bool": {
                    "filter": [
                        {
                            "terms": {
                                self.x_axis_id_property: self.x_axis_ids
                            }
                        },
                        {
                            "terms": {
                                self.y_axis_id_property: self.y_axis_ids
                            }
                        }
                    ]
                }
            },
            "aggs": {
                "activities_per_pair": {
                    "terms": {
                        "script": {
                            "source": f"doc['{self.x_axis_id_property}'].value + '_' "
                                      f"+ doc['{self.y_axis_id_property}'].value"
                        },
                        "size": max_num_cells,
                    },
                    "aggs": {
                        "pchembl_value_avg": {
                            "avg": {
                                "field": "pchembl_value"
                            }
                        }
                    }
                }
            }
        }

    def get_raw_cells_data(self):
        """
        :return: the raw data for the corresponding cells
        """
        cells_es_response = es_data.get_es_response(self.get_index_name(), self.get_query())
        return cells_es_response


class ContextualDataLoader:
    """
    base class that defines the basic structure for loading the contextual data
    """

    def __init__(self, x_axis_config, y_axis_config, cells_axis_config):
        """
        Constructor of the class
        :param x_axis_config: configuration of the x axis
        :param y_axis_config: configuration of the y axis
        :param cells_axis_config: configuration of the cells axis
        """
        self.x_axis_config = x_axis_config
        self.y_axis_config = y_axis_config
        self.cells_axis_config = cells_axis_config

        # set up the query details for the x-axis
        self.x_axis_query = self.get_x_axis_query()
        self.x_axis_query['_source'] = [self.get_x_axis_id_property()]

        # set up the query details for the y-axis
        self.y_axis_query = self.get_y_axis_query()
        self.y_axis_query['_source'] = [self.get_y_axis_id_property()]

        self.quadrant_side_length = 1000

    def get_x_axis_index_name(self):
        """
        :return: The index name to be used in the query for the x axis
        """
        entity_id = self.x_axis_config['entityID']
        index_name = entities.get_index_name(entity_id)

        return index_name

    def get_y_axis_index_name(self):
        """
        :return: The index name to be used in the query for the y axis
        """
        entity_id = self.y_axis_config['entityID']
        index_name = entities.get_index_name(entity_id)

        return index_name

    def get_x_axis_id_property(self):
        """
        :return: The id property to be used in the query for the x axis
        """
        entity_id = self.x_axis_config['entityID']
        id_property = standartisation.get_id_property_for_cell_join(entity_id)

        return id_property

    def get_y_axis_id_property(self):
        """
        :return: The id property to be used in the query for the y axis
        """
        entity_id = self.y_axis_config['entityID']
        id_property = standartisation.get_id_property_for_cell_join(entity_id)

        return id_property

    def get_x_axis_query(self):
        """
        :return: the query to be used to get the data for the x axis
        """
        return self.x_axis_config['initialQuery']

    def get_y_axis_query(self):
        """
        :return: the query to be used to get the data for the y axis
        """
        return self.y_axis_config['initialQuery']

    def get_cells_index_name(self):
        """
        :return: The index name to be used in the query for the cells
        """
        cells_entity_id = self.cells_axis_config['entityID']
        index_name = entities.get_index_name(cells_entity_id)
        return index_name

    @abc.abstractmethod
    def get_contextual_data(self):
        """
        :return: The contextual data
        """
