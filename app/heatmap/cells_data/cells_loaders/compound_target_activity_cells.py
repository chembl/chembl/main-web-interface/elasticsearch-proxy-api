"""
Class that defines the loader for the cells of the compounds vs targets heatmap
"""
import time

from farmhash import FarmHash128  # pylint: disable=no-name-in-module
from app.standardisation import entities
from app.heatmap import standartisation
from app.heatmap.cells_data.cells_loaders.main_cell_loader import CellsDataLoader, ContextualDataLoader
from app.es_data import es_data


class CompoundTargetActivityCells(CellsDataLoader):
    """
    class that defines the basic structure for loading the data for the cells with activity data of the compounds vs
    targets heatmap.
    """

    def __init__(self, x_axis_ids, y_axis_ids, descriptor):
        """
        Constructor of the class
        :param x_axis_ids: x ids of the quadrant to get
        :param y_axis_ids: y ids of the quadrant to get
        :param descriptor: descriptor of the heatmap configuration
        """
        super().__init__(x_axis_ids, y_axis_ids, descriptor)

        x_axis_config = descriptor[standartisation.HeatmapAxes.X_AXIS.value]
        x_axis_entity_id = x_axis_config['entityID']

        # Maybe this justifies creating a new class for the eubopen heatmap
        if x_axis_entity_id in [entities.EntityIDs.COMPOUND.value, entities.EntityIDs.EUBOPEN_COMPOUND.value]:
            self.x_axis_id_property = 'molecule_chembl_id'
            self.y_axis_id_property = 'target_chembl_id'
        else:
            self.x_axis_id_property = 'target_chembl_id'
            self.y_axis_id_property = 'molecule_chembl_id'

    def get_quadrant_data(self, x_items_from, x_size, y_items_from, y_size):
        """
        :param x_items_from: start of the data window to retrieve on the x-axis, to calculate the correct
        position numbers.
        :param x_size: size of the data window to retrieve on the x-axis, to calculate the correct position numbers.
        :param y_items_from: start of the data window to retrieve on the y-axis, to calculate the correct
        position numbers.
        :param y_size: size of the data window to retrieve on the y-axis, to calculate the correct position numbers.
        :return: The data extracted for the corresponding quadrant
        """
        cells_es_response = self.get_raw_cells_data()
        buckets_list = cells_es_response['aggregations']['activities_per_pair']['buckets']
        aggs_index = {bucket['key']: bucket for bucket in buckets_list}
        cells = []
        y_index = int(y_items_from)
        for y_item in self.y_axis_ids:
            x_index = int(x_items_from)
            for x_item in self.x_axis_ids:
                cells.append({
                    'x_item': x_item,
                    'y_item': y_item,
                    'y_index': y_index,
                    'x_index': x_index,
                    'data': {
                        'num_activities': get_num_activities(x_item, y_item, aggs_index),
                        'pchembl_value_avg': get_pchembl_value_avg(x_item, y_item, aggs_index),
                    }
                })
                x_index += 1
            y_index += 1

        return {

            'cells': cells,
        }


class CompoundTargetActivityContextualData(ContextualDataLoader):
    """
    class that defines the basic structure for loading the contextual data for the cells with activity data of the
    compounds vs targets heatmap.
    """

    def get_contextual_data(self):
        """
        Uses the axes configuration to get contextual data for the heatmap described.
        :return: the contextual data for the cells with activity data of the compounds vs targets heatmap.
        """

        # Initialize the contextual data
        contextual_data = {
            'meta': {
                'quadrant_side_length': self.quadrant_side_length,
                'num_cells': 0,
                'num_quadrants': 0,
                'time_taken': 0,
                'avg_time_per_quadrant': 0,
            },
            'ranges': {
                'num_activities': {
                    'min': 0,
                    'max': 0,
                },
                'pchembl_value_avg': {
                    'min': float('inf'),
                    'max': -float('inf'),
                }
            }
        }
        start_time = time.time()

        # Consume all batches of results using the generators
        for [batch_x_ids, batch_y_ids] in self.get_cells_quadrants_ids():
            num_x_items = len(batch_x_ids)
            num_y_items = len(batch_y_ids)

            contextual_data['meta']['num_cells'] += num_x_items * num_y_items
            contextual_data['meta']['num_quadrants'] += 1

            batch_contextual_data = self.get_quadrant_contextual_data(batch_x_ids, batch_y_ids)
            update_contextual_data(contextual_data, batch_contextual_data)

        end_time = time.time()
        time_taken = end_time - start_time
        contextual_data['meta']['time_taken'] = time_taken
        num_quadrants = contextual_data['meta']['num_quadrants']
        contextual_data['meta']['avg_time_per_quadrant'] = time_taken / num_quadrants if num_quadrants > 0 else 0
        return contextual_data

    def get_x_axis_batches(self):
        """
        :return: a generator of batches of the x-axis items
        """
        x_axis_index_name = self.get_x_axis_index_name()
        x_axis_query = self.x_axis_query
        return es_data.scroll_search(x_axis_index_name, x_axis_query, scroll_size=self.quadrant_side_length)

    def get_y_axis_batches(self):
        """
        :return: a generator of batches of the y-axis items
        """
        y_axis_index_name = self.get_y_axis_index_name()
        y_axis_query = self.y_axis_query
        return es_data.scroll_search(y_axis_index_name, y_axis_query, scroll_size=self.quadrant_side_length)

    def get_cells_quadrants_ids(self):
        """
        :return: a generator of quadrants ids of the cells
        """
        x_axis_id_property = self.get_x_axis_id_property()
        y_axis_id_property = self.get_y_axis_id_property()

        for x_batch in self.get_x_axis_batches():
            batch_x_ids = [x_hit['_source'][x_axis_id_property] for x_hit in x_batch]
            num_x_items = len(batch_x_ids)
            if num_x_items == 0:
                continue
            for y_batch in self.get_y_axis_batches():
                batch_y_ids = [y_hit['_source'][y_axis_id_property] for y_hit in y_batch]
                num_y_items = len(batch_y_ids)
                if num_y_items == 0:
                    continue

                yield [batch_x_ids, batch_y_ids]

    def get_quadrant_batch_query(self, batch_x_ids, batch_y_ids):
        """
        :param batch_x_ids: x-axis ids of the current batch to get contextual data for
        :param batch_y_ids: y-axis ids of the current batch to get contextual data for
        :return: the query to get the contextual data for the quadrant described by the ids provided
        """
        x_axis_id_property = self.get_x_axis_id_property()
        y_axis_id_property = self.get_y_axis_id_property()

        num_max_cells = len(batch_x_ids) * len(batch_y_ids)

        return {
            'size': 0,  # we don't need the hits, just the aggregations
            'query': {
                'bool': {
                    'filter': [
                        {'terms': {x_axis_id_property: batch_x_ids}},
                        {'terms': {y_axis_id_property: batch_y_ids}},
                    ]
                }
            },
            "aggs": {
                "activities_per_pair": {
                    "terms": {
                        "script": {
                            "source": f"doc['{x_axis_id_property}'].value + '_' "
                                      f"+ doc['{y_axis_id_property}'].value"
                        },
                        "size": num_max_cells,
                        "order": {
                            "_count": "desc"
                        },
                    },
                    "aggs": {
                        "pchembl_value_avg": {
                            "avg": {
                                "field": "pchembl_value"
                            }
                        }
                    }
                }
            }
        }

    def get_quadrant_contextual_data(self, batch_x_ids, batch_y_ids):
        """
        :param batch_x_ids: x-axis ids of the current batch to get contextual data for
        :param batch_y_ids: y-axis ids of the current batch to get contextual data for
        :return: the contextual data for the quadrant described by the ids provided
        """
        cells_index_name = self.get_cells_index_name()
        query = self.get_quadrant_batch_query(batch_x_ids, batch_y_ids)
        es_response = es_data.get_es_response(index_name=cells_index_name, es_query=query)
        num_activities_buckets = es_response['aggregations']['activities_per_pair']['buckets']
        max_num_activities = max(bucket.get('doc_count', 0) for bucket in num_activities_buckets)
        pchembl_values = [bucket.get('pchembl_value_avg', {}).get('value', 0) for bucket in num_activities_buckets]
        min_pchembl_value_avg = get_min_pchembl_value_avg(pchembl_values)
        max_pchembl_value_avg = get_max_pchembl_value_avg(pchembl_values)

        return {
            'ranges': {
                'num_activities': {
                    'min': 0,
                    'max': max_num_activities,
                },
                'pchembl_value_avg': {
                    'min': min_pchembl_value_avg,
                    'max': max_pchembl_value_avg,
                }
            }
        }


def clean_up_numeric_values(values):
    """
    removes None values from the list
    :param values: numeric values to clean up
    :return: the list without None values
    """
    return [value for value in values if value is not None]


def get_max_pchembl_value_avg(pchembl_values):
    """
    :param pchembl_values: a list of pchembl values
    :return: the max pchembl value
    """
    clean_values = clean_up_numeric_values(pchembl_values)
    if len(clean_values) == 0:
        return None

    return max(clean_values)


def get_min_pchembl_value_avg(pchembl_values):
    """
    :param pchembl_values: a list of pchembl values
    :return: the min pchembl value
    """
    clean_values = clean_up_numeric_values(pchembl_values)
    if len(clean_values) == 0:
        return None

    return min(clean_values)


def get_fake_property_value(x_item, y_item):
    """
    :param x_item: id of the x item
    :param y_item: id of the y item
    :return: a value representing a fake property from 0 to 100, it could also be an undefined value
    """
    raw_value = FarmHash128(f'{x_item}-{y_item}') % 100
    value = None if raw_value < 20 else raw_value
    return value


def get_num_activities(x_item, y_item, aggs_index):
    """
    :param x_item: id of the x item
    :param y_item: id of the y item
    :param aggs_index: a dictionary with the aggregations of the response from elasticsearch
    :return: the number of activities for the pair of items
    """
    pair_id = f'{x_item}_{y_item}'
    return aggs_index.get(pair_id, {}).get('doc_count', 0)


def get_pchembl_value_avg(x_item, y_item, aggs_index):
    """
    :param x_item: id of the x item
    :param y_item: id of the y item
    :param aggs_index: a dictionary with the aggregations of the response from elasticsearch
    :return: the average pchembl value for the pair of items
    """
    pair_id = f'{x_item}_{y_item}'
    return aggs_index.get(pair_id, {}).get('pchembl_value_avg', {}).get('value', None)


def update_contextual_data(contextual_data, batch_contextual_data):
    """
    uses the contextual data obtained from the batch to update the contextual data of the entire heatmap
    :param contextual_data: the contextual data of the entire heatmap
    :param batch_contextual_data: the contextual data of only one batch
    """

    # iterates over the properties of contextual_data.ranges to update the max and min values
    for property_name, property_data in contextual_data['ranges'].items():
        global_min = property_data['min']
        batch_min = batch_contextual_data['ranges'][property_name]['min']
        property_data['min'] = min(global_min, batch_min) if batch_min is not None else None

        global_max = property_data['max']
        batch_max = batch_contextual_data['ranges'][property_name]['max']
        property_data['max'] = max(global_max, batch_max) if batch_max is not None else None
