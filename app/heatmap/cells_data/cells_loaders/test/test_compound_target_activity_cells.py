"""
Module to test the cells loader for compounds vs targets and activities in the cells
"""
import unittest

from app.heatmap.cells_data.cells_loaders.compound_target_activity_cells import CompoundTargetActivityCells


class CompoundTargetActivityCellsTest(unittest.TestCase):
    """
    Class to test the compounds vs targets and activities in the cells loader
    """

    def test_generates_index_name(self):
        """
        Test that it generates the correct query
        """
        x_axis_ids = ['CHEMBL941', 'CHEMBL1336', 'CHEMBL535']
        y_axis_ids = ['CHEMBL612545', 'CHEMBL364', 'CHEMBL1075138']

        descriptor = {
            'xAxis': {'entityID': 'Target'},
            'yAxis': {'entityID': 'Compound'},
            'cells':
                {
                    'entityID': 'EubopenActivity', 'properties': ['num_activities', 'fake_property']
                }
        }

        loader = CompoundTargetActivityCells(x_axis_ids=x_axis_ids, y_axis_ids=y_axis_ids,
                                             descriptor=descriptor)

        cell_data_index_name_must_be = 'chembl_eubopen_activity'
        cell_data_index_name_got = loader.get_index_name()
        self.assertEqual(cell_data_index_name_got, cell_data_index_name_must_be,
                         msg='The index name was not set correctly.')

    def test_generates_query(self):
        """
        Test that it generates the correct query
        """
        x_axis_ids = ['CHEMBL941', 'CHEMBL1336', 'CHEMBL535']
        y_axis_ids = ['CHEMBL612545', 'CHEMBL364', 'CHEMBL1075138']
        descriptor = {
            'xAxis': {'entityID': 'Compound'},
            'yAxis': {'entityID': 'Target'},
            'cells':
                {
                    'entityID': 'Activity', 'properties': ['num_activities', 'fake_property']
                }
        }

        num_x_axis_ids = len(x_axis_ids)
        num_y_axis_ids = len(y_axis_ids)
        max_num_cells = num_x_axis_ids * num_y_axis_ids

        loader = CompoundTargetActivityCells(x_axis_ids=x_axis_ids, y_axis_ids=y_axis_ids,
                                             descriptor=descriptor)

        cell_data_query_must_be = {
            '_source': ['molecule_chembl_id', 'target_chembl_id'],
            'query': {
                'bool': {
                    'filter': [
                        {'terms': {'molecule_chembl_id': ['CHEMBL941', 'CHEMBL1336', 'CHEMBL535']}},
                        {'terms': {'target_chembl_id': ['CHEMBL612545', 'CHEMBL364', 'CHEMBL1075138']}}
                    ]
                }
            },
            'aggs': {
                'activities_per_pair': {
                    'terms': {
                        'script': {'source': "doc['molecule_chembl_id'].value + '_' + doc['target_chembl_id'].value"},
                        "size": max_num_cells,
                    },
                    "aggs": {
                        "pchembl_value_avg": {
                            "avg": {
                                "field": "pchembl_value"
                            }
                        }
                    }

                },

            }
        }

        cell_data_query_got = loader.get_query()
        self.assertEqual(cell_data_query_must_be, cell_data_query_got,
                         msg='The query was not set correctly.')
