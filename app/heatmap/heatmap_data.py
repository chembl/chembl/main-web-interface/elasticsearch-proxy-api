# pylint: disable=inconsistent-return-statements,too-many-locals
"""
Module that provides the data requested for the heatmap
"""
import re

from app.es_data import es_data
from app.heatmap.standartisation import HeatmapAxes
from app.standardisation import entities
from app.cache.decorators import return_if_cached_results
from app.heatmap.cells_data import cells_data
from app.heatmap import standartisation
from app.properties_configuration import groups_configuration_manager, properties_configuration_manager
from app.config import RUN_CONFIG
from utils import dict_property_access


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'{str(args)}-heatmap_axis_summary',
    'timeout': 3600
})
def get_heatmap_axis_summary(descriptor, axis_id: HeatmapAxes):
    """
    :param descriptor: dictionary describing the heatmap
    :param axis_id: the axis for which to get the summary, one of HeatmapAxes
    :return: a dict with the heatmap axis summary (counts, etc)
    """

    axis_key = axis_id.value
    axis_config = descriptor[axis_key]
    entity_id = axis_config['entityID']
    index_name = entities.get_index_name(entity_id)
    query = axis_config['initialQuery']
    query['size'] = 0
    query['track_total_hits'] = True
    es_response = es_data.get_es_response(index_name, query)
    item_count = es_response['hits']['total']['value']

    return {
        'item_count': item_count
    }


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'{str(args)}-heatmap_autocomplete',
    'timeout': 3600
})
def get_heatmap_autocomplete_in_axis(descriptor, axis_id: HeatmapAxes, term):
    """
    takes the query of the axis indicated and performs the autocomplete with the term entered
    :param descriptor: dict descriptor of the heatmap
    :param axis_id: axis id for which to get the query. One of HeatmapAxes
    :param term: term to autocomplete
    :return: the autocomplete results
    """

    axis_key = axis_id.value
    axis_config = descriptor[axis_key]
    entity_id = axis_config['entityID']

    index_name = entities.get_index_name(entity_id)
    query = axis_config['initialQuery']
    general_autocomplete_config = RUN_CONFIG.get('search', {}).get('suggestions', {}).get('entities', {})
    entity_autocomplete_config = general_autocomplete_config.get(entity_id)
    label_property = entity_autocomplete_config.get('label_property')
    autocomplete_query, properties_to_highlight = generate_autocomplete_query(index_name, query, term,
                                                                              entity_autocomplete_config)

    suggestions = get_autocomplete_suggestions(term, index_name, autocomplete_query, properties_to_highlight,
                                               label_property)

    return {
        'suggestions': suggestions
    }


def generate_autocomplete_query(index_name, query, term, entity_autocomplete_config):
    """
    :param index_name: name of the index for which to generate the autocomplete query
    :param query: query to autocomplete
    :param term: term to autocomplete
    :param entity_autocomplete_config: config for the entity for which to autocomplete
    :return: the query with the autocomplete applied and the properties to highlight
    """
    if query.get('query') is None:
        query['query'] = {}
    if query['query'].get('bool') is None:
        query['query']['bool'] = {}
    if query['query']['bool'].get('must') is None:
        query['query']['bool']['must'] = []

    highlight_properties_group = entity_autocomplete_config['highlight_properties_group']

    group_config_manager = groups_configuration_manager.get_groups_configuration_instance()
    properties_to_highlight = group_config_manager.get_prop_ids_for_config_group(index_name,
                                                                                 highlight_properties_group)
    query['query']['bool']['must'].append({
        'query_string': {
            'query': f'{term}*'
        }
    })

    query['_source'] = properties_to_highlight
    query['size'] = 3
    query['track_total_hits'] = True

    return query, properties_to_highlight


def get_property_name(index_name, property_path):
    """
    :param index_name: name of the index to which the property belongs
    :param property_path: property id
    :return: the label corresponding to this property
    """
    group_config_manager = properties_configuration_manager.get_property_configuration_instance()
    property_config = group_config_manager.get_config_for_prop(index_name, property_path)
    highlighted_property_label = property_config['label']

    return highlighted_property_label


def get_autocomplete_suggestions(term, index_name, query, properties_to_highlight, label_property):
    """
    :param term: term to autocomplete
    :param index_name: name of the index for which to get the autocomplete results
    :param query: query to autocomplete
    :param properties_to_highlight: properties to highlight in the autocomplete results
    :param label_property: property to use as label in the autocomplete results
    :return: the autocomplete results
    """
    es_response = es_data.get_es_response(index_name, query)
    items = es_response['hits']['hits']
    suggestions = []
    for item in items:
        item_id = item['_id']
        source = item['_source']
        item_label = dict_property_access.get_property_value(source, label_property)
        text = ''
        highlighted_property = ''

        for prop_id in properties_to_highlight:

            raw_value = dict_property_access.get_property_value(source, prop_id)
            prop_value = parse_prop_value(raw_value)
            pattern = rf'\b{term}[\w\s]*\b'
            match = re.search(pattern, prop_value, re.IGNORECASE)
            if match:
                highlighted_property = prop_id
                text = match.group(0)
                break

        property_name = get_property_name(index_name, highlighted_property) if highlighted_property != '' else ''

        suggestions.append({
            '_id': item_id,
            'item_label': item_label,
            'text': text,
            'highlighted_property': property_name,
        })

    return suggestions


def parse_prop_value(prop_value):
    """
    Parses the value of a property to a string
    :param prop_value: value of the property
    :return: the parsed value to be used in the autocomplete matching
    """
    if isinstance(prop_value, list):
        return '&&&&&&&&&'.join([str(parse_prop_value(val)) for val in prop_value])
    if isinstance(prop_value, dict):
        return '&&&&&&&&&'.join(get_only_dict_values(prop_value))
    return str(prop_value)


def get_only_dict_values(my_dict):
    """
    Returns a list of values in the dictionary recursively, ignoring keys.
    :param my_dict: dict to get the values from
    :return: the values of the dict
    """
    values = []
    for val in my_dict.values():
        if isinstance(val, dict):
            values.extend(get_only_dict_values(val))
        elif isinstance(val, list):
            for item in val:
                if isinstance(item, (dict, list)):
                    values.extend(get_only_dict_values(item))
                else:
                    values.append(item)
        else:
            values.append(val)
    # convert all the items to string before returning
    values = [str(val) for val in values]
    return values


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'{str(args)}-heatmap_items',
    'timeout': 3600
})
def get_heatmap_items(descriptor, axis_id: HeatmapAxes, part_name, items_from, size):
    """
    :param descriptor: dict descriptor of the heatmap
    :param axis_id: axis id for which to ge the data. One of HeatmapAxes
    :param part_name:  name of the part to get; headers or footers
    :param items_from: start of the data window to retrieve
    :param size: size of the data window to retrieve
    :return: the items requested
    """
    axis_key = axis_id.value

    axis_config = descriptor[axis_key]
    entity_id = axis_config['entityID']
    index_name = entities.get_index_name(entity_id)
    query = axis_config['initialQuery']

    if part_name == 'headers':
        label_property = axis_config[part_name]['label_property']
        additional_properties = axis_config[part_name].get('additional_properties', [])
        source = [label_property, *additional_properties]
    else:
        label_properties = axis_config[part_name]['label_properties']
        source = label_properties

    query['_source'] = source
    query['from'] = items_from
    query['size'] = size
    es_response = es_data.get_es_response(index_name, query)
    items = es_response['hits']['hits']

    return {
        'items': items,
    }


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'{str(args)}-heatmap_cells',
    'timeout': 3600
})
def get_heatmap_cells(descriptor, x_items_from, x_size, y_items_from, y_size):
    """
    :param descriptor: dict descriptor of the heatmap
    :param x_items_from: start of the data window to retrieve on the x-axis
    :param x_size: size of the data window to retrieve on the x-axis
    :param y_items_from: start of the data window to retrieve on the y-axis
    :param y_size: size of the data window to retrieve on the y-axis
    :return: the cells requested
    """

    x_axis_config = descriptor[HeatmapAxes.X_AXIS.value]
    x_axis_ids = get_window_axis_ids(x_axis_config, x_items_from, x_size)

    y_axis_config = descriptor[HeatmapAxes.Y_AXIS.value]
    y_axis_ids = get_window_axis_ids(y_axis_config, y_items_from, y_size)

    pagination_params = {
        'x_items_from': x_items_from,
        'x_size': x_size,
        'y_items_from': y_items_from,
        'y_size': y_size
    }
    return cells_data.get_cells_data(x_axis_ids, y_axis_ids, descriptor, pagination_params)


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'{str(args)}-heatmap-contextual_data',
    'timeout': 3600
})
def get_heatmap_contextual_data(descriptor):
    """
    Usess the cells_data package to get the contextual data for the heatmap
    :param descriptor: dict descriptor of the heatmap
    :return: a dict with the contextual data requested
    """

    cells_axis_config = descriptor[HeatmapAxes.CELLS.value]
    x_axis_config = descriptor[HeatmapAxes.X_AXIS.value]
    y_axis_config = descriptor[HeatmapAxes.Y_AXIS.value]
    return cells_data.get_contextual_data(x_axis_config, y_axis_config, cells_axis_config)


def get_window_axis_ids(axis_config, items_from, size):
    """
    :param axis_config: configuration for the corresponding axis
    :param items_from: start of the data window to retrieve
    :param size: size of the data window to retrieve
    :return: the ids corresponding to the axis and the window given
    """

    entity_id = axis_config['entityID']
    query = axis_config['initialQuery']
    return get_ids_only(entity_id, query, items_from, size)


@return_if_cached_results({
    'cache_key_generator': lambda *args, **kwargs: f'{str(args)}-heatmap_ids_only',
    'timeout': 3600
})
def get_ids_only(entity_id, query, items_from, size):
    """
    provides the ids to be used on que cells query
    :param entity_id: id of the entity for which to get the ids
    :param query: base query from the heatmap description
    :param items_from: start of the data window to retrieve
    :param size: size of the data window to retrieve
    :return: the ids requested
    """
    index_name = entities.get_index_name(entity_id)

    id_property = standartisation.get_id_property_for_cell_join(entity_id)
    query['_source'] = id_property
    query['from'] = items_from
    query['size'] = size
    es_response = es_data.get_es_response(index_name, query)
    ids = [
        dict_property_access.get_property_value(item['_source'], id_property)
        for item in es_response['hits']['hits']]

    return ids
