"""
Module some standardisation functions, enum, etc., for the heatmap
"""
from enum import Enum

from app.standardisation import entities


class HeatmapAxes(Enum):
    """
    Standard heatmap axes names
    """
    X_AXIS = 'xAxis'
    Y_AXIS = 'yAxis'
    CELLS = 'cells'


ID_PROPERTIES_FOR_CELL_JOIN = {
    entities.EntityIDs.EUBOPEN_COMPOUND: 'molecule_chembl_id',
    entities.EntityIDs.EUBOPEN_TARGET: 'target_chembl_id',
    entities.EntityIDs.COMPOUND: 'molecule_chembl_id',
    entities.EntityIDs.TARGET: 'target_chembl_id'
}


def get_id_property_for_cell_join(entity):
    """
    Uses ID_PROPERTIES_FOR_CELL_JOIN to return the property to be used to join the axis with the cells
    :param entity: The entity ID of the axis
    :return: the property to be used to join the axis with the cells
    """
    # entity can be an entity id as string or an instance of EntityIDs
    return ID_PROPERTIES_FOR_CELL_JOIN.get(entity, ID_PROPERTIES_FOR_CELL_JOIN.get(entities.EntityIDs(entity)))
