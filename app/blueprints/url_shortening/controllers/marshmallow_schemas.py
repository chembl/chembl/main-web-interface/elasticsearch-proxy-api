"""
Marshmallow schemas for validating the url shortening controller
"""
from marshmallow import fields

from app.common_marshmallow.common_schemas import CommonSchema


class ShortenURLRequest(CommonSchema):
    """
    Class with the schema for shortening a URL
    """
    long_url = fields.String(required=True)


class ExpandURLRequest(CommonSchema):
    """
    Class with the schema for shortening a URL
    """
    url_hash = fields.String(required=True)
