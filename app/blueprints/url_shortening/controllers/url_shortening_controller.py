"""
URL Shortening controller
"""
from flask import Blueprint, jsonify, request

from app.request_validation.decorators import validate_form_with, validate_url_params_with
from app.blueprints.url_shortening.controllers import marshmallow_schemas
from app.blueprints.url_shortening.services import url_shortening_service
from app import request_utils
from app.common_marshmallow import utils as schema_utils
from app.usage_statistics.decorators import record_response_final_status
from app.request_error_logging_decorators import log_and_return_internal_server_error

URL_SHORTENING_BLUEPRINT = Blueprint('url_shortening', __name__)


@URL_SHORTENING_BLUEPRINT.route('/shorten_url', methods=['POST'])
@validate_form_with(marshmallow_schemas.ShortenURLRequest)
@record_response_final_status()
@log_and_return_internal_server_error()
def shorten_url():
    """
    :return: the json response with the shortened url
    """

    form_data = request.form
    long_url = form_data.get('long_url')
    is_test = schema_utils.parse_boolean_param(form_data, 'is_test')

    shortening_data = url_shortening_service.shorten_url(long_url, is_test)
    return jsonify(shortening_data)


@URL_SHORTENING_BLUEPRINT.route('/expand_url/<url_hash>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.ExpandURLRequest)
@record_response_final_status()
@log_and_return_internal_server_error()
def expand_url(url_hash):
    """
    :return: the json response with the expanded url
    """

    try:
        expansion_data = url_shortening_service.expand_url(url_hash)
        return jsonify(expansion_data)
    except url_shortening_service.URLNotFoundError as error:
        request_utils.log_and_return_not_found_404(error)
