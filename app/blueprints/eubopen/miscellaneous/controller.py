"""
Controller for miscellaneous matters in eubopen
"""
import csv

from flask import Blueprint, jsonify, send_file
from app.http_cache import http_cache_utils
from app.blueprints.eubopen.miscellaneous import services
from app.usage_statistics.decorators import record_response_final_status
from app.request_error_logging_decorators import log_and_return_internal_server_error

EUBOPEN_MISC_BLUEPRINT = Blueprint('miscellaneous', __name__)


@EUBOPEN_MISC_BLUEPRINT.route('/privacy_notice', methods=['GET'])
@record_response_final_status()
@log_and_return_internal_server_error()
def get_privacy_notice_data():
    """
    :return: the results for the privacy notice data
    """

    json_data = services.get_privacy_notice()
    http_response = http_cache_utils.get_json_response_with_http_cache_headers(json_data, hours=1)
    return http_response


@EUBOPEN_MISC_BLUEPRINT.route('/datasets_mockup', methods=['GET'])
@record_response_final_status()
@log_and_return_internal_server_error()
def datasets_mockup():
    """
    :return: the results for the datasets mockup
    """

    datasets = {
        "datasets": [
            {
                "id": "1",
                "name": "Epigenetic set",
                "description": "Description of dataset 1",
            },
            {
                "id": "2",
                "name": "GPCR set",
                "description": "Description of dataset 2",
            },
            {
                "id": "3",
                "name": "Ion Channel set",
                "description": "Description of dataset 3",
            },
            {
                "id": "4",
                "name": "Kinase set",
                "description": "Description of dataset 4",
            },
            {
                "id": "5",
                "name": "Nuclear receptor set",
                "description": "Description of dataset 5",
            },
            {
                "id": "6",
                "name": "Other targets set",
                "description": "Description of dataset 6",
            },
            {
                "id": "7",
                "name": "Protease set",
                "description": "Description of dataset 7",
            }
        ]
    }

    return jsonify(datasets)


@EUBOPEN_MISC_BLUEPRINT.route('/dataset/<set_id>', methods=['GET'])
@record_response_final_status()
@log_and_return_internal_server_error()
def dataset_mockup(set_id):
    """
    :param set_id: the id of the dataset
    :return: the results for a dataset mockup
    """
    csv_file_path = f'app/blueprints/eubopen/miscellaneous/data/{set_id}.csv'
    with open(csv_file_path, 'r') as file:
        # Read the CSV file
        rows = csv.DictReader(file)
        json_data = {
            'setItems': list(rows)
        }

        return jsonify(json_data)


@EUBOPEN_MISC_BLUEPRINT.route('/dataset/csv/<set_id>/<download_file_name>', methods=['GET'])
@record_response_final_status()
@log_and_return_internal_server_error()
def dataset_mockup_csv(set_id, download_file_name):
    """
    :param set_id: the id of the dataset
    :param download_file_name: the name of the file generated for download
    :return: the csv file for a dataset mockup
    """
    file_name = f'{set_id}.csv'
    csv_file_path = f'blueprints/eubopen/miscellaneous/data/{file_name}'
    return send_file(csv_file_path, attachment_filename=download_file_name)
