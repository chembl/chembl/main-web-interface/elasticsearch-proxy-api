"""
Services for the miscellaneous matters
"""

from app.eubopen.miscellaneous import privacy_notice


def get_privacy_notice():
    """
    :param term: term for which to do the autocomplete
    :return: the results for the autocomplete query
    """

    return privacy_notice.get_privacy_notice_data()
