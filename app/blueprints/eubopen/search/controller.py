"""
Controller for handling the requests for the eubopen search
"""
from flask import Blueprint, jsonify

from app.request_validation.decorators import validate_url_params_with
from app.blueprints.eubopen.search import marshmallow_schemas
from app.blueprints.eubopen.search import services
from app.usage_statistics.decorators import record_response_final_status
from app.http_cache.decorators import validate_http_cache
from app.request_error_logging_decorators import log_and_return_internal_server_error

EUBOPEN_SEARCH_BLUEPRINT = Blueprint('eubopen_search', __name__)


@EUBOPEN_SEARCH_BLUEPRINT.route('/autocomplete/<term>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.EubopenAutocomplete)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_autocomplete_results(term):
    """
    :param term: autocomplete term
    :return: the results for the autocomplete query
    """

    parsed_term = '*' if term == '' or term is None else term
    json_data = services.get_autocomplete_results(parsed_term)
    return jsonify(json_data)


@EUBOPEN_SEARCH_BLUEPRINT.route('/free_text/<term>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.EubopenSearch)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_free_text_search_queries(term):
    """
    :param term: search term
    :return: the queries corresponding to the search term
    """

    parsed_term = '*' if term == '' else term
    json_data = services.get_search_results(parsed_term)
    return jsonify(json_data)
