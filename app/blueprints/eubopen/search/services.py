"""
Services for the eubopen search
"""

from app.eubopen.search.suggestions import suggestions
from app.eubopen.search.free_text import free_text


def get_autocomplete_results(term):
    """
    :param term: term for which to do the autocomplete
    :return: the results for the autocomplete query
    """

    return suggestions.get_suggestions_for_term(term)


def get_search_results(term):
    """
    :param term: term that is being searched
    :return: the queries and details of the search results
    """

    return free_text.get_eubopen_search_results(term)
