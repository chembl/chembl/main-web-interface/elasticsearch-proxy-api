"""
Schemas to validate the input of the eubopen search endpoints
"""
from marshmallow import fields

from app.common_marshmallow.common_schemas import CommonSchema


class EubopenAutocomplete(CommonSchema):
    """
    Class that the defines the schema for the autocomplete in eubopen
    """
    term = fields.String(required=True)


class EubopenSearch(CommonSchema):
    """
    Class that the defines the schema for the search in eubopen
    """
    term = fields.String(required=True)
