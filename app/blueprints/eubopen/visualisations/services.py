"""
Services for the eubopen visualisations
"""
from app.visualisation_data.target_classification import eubopen_protein_class
from app.eubopen.visualisation_data import cell_health_and_viability


class VisualisationServiceError(Exception):
    """
    Base class for errors in this module
    """


def get_protein_target_classification():
    """
    :return: the json response with the protein target classification
    """
    return eubopen_protein_class.get_classification_tree()


def get_cell_viability_and_health_data(compound_id):
    """
    :param compound_id: id of the compound
    :param output_format: desired format of the output
    :return: the dict with cell viability and health data of the compound with the id given
    """
    return cell_health_and_viability.get_cell_viability_and_health_data(compound_id)


def download_cell_viability_and_health_data(compound_id):
    """
    :param compound_id: id of the compound
    :param output_format: desired format of the output
    :return: the dict with cell viability and health data of the compound with the id given
    """
    output_format = cell_health_and_viability.OutputFormats.CSV
    return cell_health_and_viability.get_cell_viability_and_health_data(compound_id, output_format)
