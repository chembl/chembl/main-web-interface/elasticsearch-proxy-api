"""
Controller for handling the requests for the eubopen visualisations
"""

from flask import Blueprint, jsonify, make_response

from app.blueprints.eubopen.visualisations import services
from app.usage_statistics.decorators import record_response_final_status
from app.http_cache.decorators import validate_http_cache
from app.request_error_logging_decorators import log_and_return_internal_server_error

EUBOPEN_VISUALISATIONS_BLUEPRINT = Blueprint('eubopen_visualisations', __name__)


@EUBOPEN_VISUALISATIONS_BLUEPRINT.route('/target_classifications/protein_classification', methods=['GET'])
@validate_http_cache()
@record_response_final_status()
@log_and_return_internal_server_error()
def get_protein_target_classification():
    """
    :return: the json response with the protein target classification
    """

    json_data = services.get_protein_target_classification()
    return jsonify(json_data)


@EUBOPEN_VISUALISATIONS_BLUEPRINT.route('/compound/cell_viability_and_health_data/<compound_id>', methods=['GET'])
@validate_http_cache()
@record_response_final_status()
@log_and_return_internal_server_error()
def get_cell_viability_and_health_data(compound_id):
    """
    :param compound_id: id of the compound
    :return: the cell viability and health data of the compound with the id given
    """

    json_data = services.get_cell_viability_and_health_data(compound_id)
    return jsonify(json_data)


@EUBOPEN_VISUALISATIONS_BLUEPRINT.route('/compound/cell_viability_and_health_data/download/<compound_id>',
                                        methods=['GET'])
@validate_http_cache()
@record_response_final_status()
@log_and_return_internal_server_error()
def download_cell_viability_and_health_data(compound_id):
    """
    :param compound_id: id of the compound
    :return: the cell viability and health data of the compound with the id given in CSV format
    """
    csv_data = services.download_cell_viability_and_health_data(compound_id)
    output = make_response(csv_data)

    output.headers["Content-Disposition"] = f'attachment; filename={compound_id}.csv'
    output.headers["Content-type"] = "text/csv"

    return output
