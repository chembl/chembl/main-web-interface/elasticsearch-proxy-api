"""
Contexts controller
"""
import json

from flask import Blueprint, jsonify, request

from app.request_validation.decorators import validate_form_with
from app.blueprints.contexts.controllers import marshmallow_schemas
from app.blueprints.contexts.services import contexts_service
from app.usage_statistics.decorators import record_response_final_status
from app.request_error_logging_decorators import log_and_return_internal_server_error

CONTEXTS_BLUEPRINT = Blueprint('contexts', __name__)


@CONTEXTS_BLUEPRINT.route('/get_context_data', methods=['POST'])
@validate_form_with(marshmallow_schemas.ContextDataRequest)
@record_response_final_status()
@log_and_return_internal_server_error()
def get_context_data():
    """
    :return: the json response with the context data
    """

    form_data = request.form

    raw_context_obj = form_data.get('context_obj')
    context_dict = json.loads(raw_context_obj)
    if isinstance(context_dict, str):
        context_dict = json.loads(context_dict)

    context_data = contexts_service.get_context_data(context_dict)
    return jsonify(context_data)
