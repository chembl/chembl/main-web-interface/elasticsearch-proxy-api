"""
Marshmallow schemas for validating the properties configuration controller
"""
from marshmallow import fields

from app.common_marshmallow.common_schemas import CommonSchema


class ContextDataRequest(CommonSchema):
    """
    Class with the schema for getting the data of a context
    """
    context_obj = fields.String(required=True)
