"""
Service that handles the requests to the entities join
"""
from app.entities_joiner import entities_joiner


class EntitiesJoinServiceError(Exception):
    """Base class for exceptions in this file."""


def get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                   selection_description, raw_context_obj, is_test=False):
    """
    :param entity_from: source entity of the items
    :param entity_to: destination entity of the join
    :param es_query: query in elasticsearch for the dataset
    :param selection_description: stringifyed javascript object describing de selection of items in the dataset
    :param raw_context_obj: stringifyed object describing the context of the query
    :param is_test: tells if the request is marked as form a test
    :return: a dict with the tiny url to the link with the generated state
    """
    try:
        tiny_hash = entities_joiner.get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                                                   selection_description,
                                                                   raw_context_obj, is_test)

        return {
            'tiny_hash': tiny_hash
        }

    except entities_joiner.EntitiesJoinerError as error:
        raise EntitiesJoinServiceError(error)


def get_query_to_related_items(entity_from, entity_to, es_query, selection_description, raw_context_obj, is_test=False):
    """
    :param entity_from: source entity of the items
    :param entity_to: destination entity of the join
    :param es_query: query in elasticsearch for the dataset
    :param selection_description: stringifyed javascript object describing de selection of items in the dataset
    :param raw_context_obj: stringifyed object describing the context of the query
    :param is_test: tells if the request is marked as form a test
    :return:  a dict with the query to do the join with the parameters given
    """

    try:
        query = entities_joiner.get_query_to_related_items(entity_from, entity_to, es_query, selection_description,
                                                           raw_context_obj, is_test)

        return {
            'query': query
        }

    except entities_joiner.EntitiesJoinerError as error:
        raise EntitiesJoinServiceError(error)
