"""
    The blueprint used for handling requests to do joins of entities
"""
from flask import Blueprint, jsonify, request

from app.request_validation.decorators import validate_form_with
from app.blueprints.entities_join.controllers import marshmallow_schemas
from app.blueprints.entities_join.services import entities_join_service
from app.common_marshmallow import utils as schema_utils
from app.usage_statistics.decorators import record_response_final_status
from app.request_error_logging_decorators import log_and_return_internal_server_error
from utils import request_parameters

ENTITIES_JOIN_BLUEPRINT = Blueprint('entities_join', __name__)


@ENTITIES_JOIN_BLUEPRINT.route('/get_link_to_related_items', methods=['POST'])
@validate_form_with(marshmallow_schemas.EntitiesJoinQueryHash)
@record_response_final_status()
@log_and_return_internal_server_error()
def get_link_to_related_items():
    """
    returns the hash of the tiny url to the related items with the parameters given
    """

    form_data = request.form

    entity_from = request_parameters.sanitise_parameter(form_data.get('entity_from'))
    entity_to = request_parameters.sanitise_parameter(form_data.get('entity_to'))
    es_query = request_parameters.sanitise_parameter(form_data.get('es_query'))
    selection_description = request_parameters.sanitise_parameter(form_data.get('selection_description'))
    context_obj = request_parameters.sanitise_parameter(form_data.get('context_obj'))
    is_test = schema_utils.parse_boolean_param(form_data, 'is_test')

    json_response = entities_join_service.get_tiny_hash_to_related_items(entity_from, entity_to, es_query,
                                                                         selection_description,
                                                                         context_obj,
                                                                         is_test)
    return jsonify(json_response)


@ENTITIES_JOIN_BLUEPRINT.route('/get_query_to_related_items', methods=['POST'])
@validate_form_with(marshmallow_schemas.EntitiesJoinQuery)
@record_response_final_status()
@log_and_return_internal_server_error()
def get_query_to_related_items():
    """
    returns the query to the join the related items with the parameters given
    """

    form_data = request.form

    entity_from = request_parameters.sanitise_parameter(form_data.get('entity_from'))
    entity_to = request_parameters.sanitise_parameter(form_data.get('entity_to'))
    es_query = request_parameters.sanitise_parameter(form_data.get('es_query'))
    selection_description = request_parameters.sanitise_parameter(form_data.get('selection_description'))
    context_obj = request_parameters.sanitise_parameter(form_data.get('context_obj'))
    is_test = schema_utils.parse_boolean_param(form_data, 'is_test')

    json_response = entities_join_service.get_query_to_related_items(
        entity_from, entity_to, es_query,
        selection_description,
        context_obj,
        is_test)
    return jsonify(json_response)
