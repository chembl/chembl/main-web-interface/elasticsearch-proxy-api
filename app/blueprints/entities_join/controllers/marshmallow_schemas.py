"""
Schemas to validate the input of entities join Endpoint
"""
from marshmallow import fields

from app.common_marshmallow.common_schemas import CommonSchema


class EntitiesJoinQueryHash(CommonSchema):
    """
    Class that defines the schema for doing an es join and getting the resulting
    """
    destination_entity_browser_state_template = fields.String(required=True)
    entity_from = fields.String(required=True)
    entity_to = fields.String(required=True)
    es_query = fields.String(required=True)
    selection_description = fields.String(required=True)
    previous_hash = fields.String()
    origin_id_property = fields.String()
    context_obj = fields.String()


class EntitiesJoinQuery(CommonSchema):
    """
    Class that defines the schema for doing an es join and getting a query
    """
    entity_from = fields.String(required=True)
    entity_to = fields.String(required=True)
    es_query = fields.String(required=True)
    selection_description = fields.String(required=True)
    origin_id_property = fields.String()
    context_obj = fields.String()
