"""
Element usage controller
"""
from flask import Blueprint, jsonify

from app.request_validation.decorators import validate_form_with
from app.blueprints.element_usage_blueprint.controllers import marshmallow_schemas
from app.usage_statistics.decorators import record_response_final_status
from app.request_error_logging_decorators import log_and_return_internal_server_error

ELEMENT_USAGE_BLUEPRINT = Blueprint('frontend_element_usage', __name__)


@ELEMENT_USAGE_BLUEPRINT.route('/register_element_usage', methods=['POST'])
@validate_form_with(marshmallow_schemas.RegisterUsageRequest)
@record_response_final_status()
@log_and_return_internal_server_error()
def register_element_usage():
    """
    :return: the json response with the operation result
    """

    # form_data = request.form
    # view_name = form_data.get('view_name')
    # view_type = form_data.get('view_type')
    # entity_name = form_data.get('entity_name')
    # operation_result = element_usage_service.register_element_usage(view_name, view_type, entity_name)
    return jsonify({'msg': 'disabled'})
