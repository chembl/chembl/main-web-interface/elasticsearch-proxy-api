"""
Module with the utils services
"""
from app.chembl_utils.auto_separator import separator_identifier


class UtilsError(Exception):
    """
    Class for errors in this module
    """


def identify_separator(type_of_ids, text):
    """
    Identifies the separator for the ids entered in text
    :param type_of_ids: type of the ids entered
    :param text: text to process
    :return: a dict with the separator identified
    """

    separator = separator_identifier.get_separator(text, type_of_ids)
    return {
        'separator': separator
    }
