"""
Schemas to validate the utils endpoints
"""
from marshmallow import Schema, fields


class IdentifySeparatorQuery(Schema):
    """
    Class that the schema for identifying a separator
    """
    type_of_ids = fields.String(required=True)
    text = fields.String(required=True)
