"""
Module with the controller for the utils endpoints
"""
from flask import Blueprint, jsonify, request

from app.request_validation.decorators import validate_form_with
from app.blueprints.utils.controllers import marshmallow_schemas
from app import app_logging
from app.blueprints.utils.services import utils_service
from app.usage_statistics.decorators import record_response_final_status
from app.request_error_logging_decorators import log_and_return_internal_server_error

UTILS_BLUEPRINT = Blueprint('utils', __name__)


@UTILS_BLUEPRINT.route('/identify_separator', methods=['POST'])
@validate_form_with(marshmallow_schemas.IdentifySeparatorQuery)
@record_response_final_status()
@log_and_return_internal_server_error()
def get_es_data():
    """
    :return: the json response with the separator identified in the text
    """
    form_data = request.form
    type_of_ids = form_data.get('type_of_ids')
    text = form_data.get('text')

    app_logging.debug(f'type_of_ids: {type_of_ids}')
    app_logging.debug(f'text: {text}')

    dict_response = utils_service.identify_separator(type_of_ids, text)
    return jsonify(dict_response)
