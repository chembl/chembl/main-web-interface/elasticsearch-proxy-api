"""
Search service
"""
from app.search import autocomplete
from app.eubopen.search.free_text import free_text
from app.search.properties_priorities.priorities import PropertiesPriorities


class SearchServiceError(Exception):
    """
    Class for errors in this module
    """


def get_autocomplete_results(term):
    """
    :param term: term to autocomplete
    :return: the results of the autocomplete for the term indicated
    """
    return autocomplete.get_autocomplete_results(term)


def get_search_results(term):
    """
    :param term: term that is being searched
    :return: the queries and details of the search results
    """
    return free_text.get_search_results(term)


def get_properties_priorities(entity_id):
    """
    Returns the properties and their priorities for a given entity.
    :param entity_id: The entity for which to get the properties and priorities.
    :return: A dictionary of properties and their priorities for the given entity.
    """

    properties_priorities = PropertiesPriorities.get_instance()
    priority_dict = properties_priorities.get_priorities(entity_id)
    return priority_dict
