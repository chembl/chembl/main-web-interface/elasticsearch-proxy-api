"""
Marshmallow schemas for validating the queries to the search endpoints
"""
from marshmallow import fields

from app.common_marshmallow.common_schemas import CommonSchema


class AutocompleteRequest(CommonSchema):
    """
    Class with the schema for the search autocomplete
    """
    term = fields.String(required=True)


class SearchRequest(CommonSchema):
    """
    Class with the schema for the search query
    """
    term = fields.String(required=True)


class PropertiesPrioritiesRequest(CommonSchema):
    """
    Class with the schema for the properties priorities
    """
    entity_id = fields.String(required=True)
