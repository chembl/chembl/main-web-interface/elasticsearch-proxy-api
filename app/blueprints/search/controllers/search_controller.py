"""
Search controller
"""
from flask import Blueprint, jsonify

from app.request_validation.decorators import validate_url_params_with
from app.blueprints.search.controllers import marshmallow_schemas
from app.usage_statistics.decorators import record_response_final_status
from app.blueprints.search.services import search_service
from app.http_cache.decorators import validate_http_cache
from app.request_error_logging_decorators import log_and_return_internal_server_error
from utils import escaping

SEARCH_BLUEPRINT = Blueprint('search', __name__)


@SEARCH_BLUEPRINT.route('/autocomplete/<term>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.AutocompleteRequest)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_autocomplete_results(term):
    """
    :param term: term for which get the autocomplete
    :return: the autocomplete results
    """
    response = jsonify(search_service.get_autocomplete_results(escaping.unsanitise_slash(term)))
    return response


@SEARCH_BLUEPRINT.route('/free_text/<term>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.SearchRequest)
@validate_http_cache()
@log_and_return_internal_server_error()
def get_free_text_search_queries(term):
    """
        :param term: term for which get the search query
        :return: the queries corresponding to the search term
        """
    response = jsonify(search_service.get_search_results(escaping.unsanitise_slash(term)))
    return response


@SEARCH_BLUEPRINT.route('/properties_priorities/<entity_id>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.PropertiesPrioritiesRequest)
@validate_http_cache()
@log_and_return_internal_server_error()
def get_properties_priorities(entity_id):
    """
    :param entity_id: entity id for which get the properties priorities
    :return: the properties priorities corresponding to the entity id
    """
    response = jsonify(search_service.get_properties_priorities(entity_id))
    return response
