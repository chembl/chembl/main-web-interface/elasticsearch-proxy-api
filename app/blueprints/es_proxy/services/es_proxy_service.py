"""
Service that handles the general requests to elasticsearch data
"""
import json

from app import app_logging
from app.es_data import es_data
from app.context_loader import context_loader
from app.config import RUN_CONFIG
from app.es_data import es_mappings
from app.context_loader import context_injector


class ESProxyServiceError(Exception):
    """Base class for exceptions in this file."""


class ESDataNotFoundError(Exception):
    """Base class for exceptions in this file."""


def get_es_data(index_name, raw_es_query, raw_context, raw_contextual_sort_data, is_test=False):
    """
    :param index_name: name of the index to query
    :param raw_es_query: stringifyed version of the query to send to elasticsearch
    :param raw_context: stringifyed version of a JSON object describing the context of the query
    :param id_property: property that identifies every item. Required when context provided
    :param raw_contextual_sort_data: description of sorting if sorting by contextual properties
    :param is_test: tells if the request comes from a test script
    :return: Returns the json response from elasticsearch and some metadata if necessary
    """
    es_query = json.loads(raw_es_query) if isinstance(raw_es_query, str) else raw_es_query
    es_query = complete_es_query(es_query)
    context_dict = json.loads(raw_context) if isinstance(raw_context, str) else raw_context

    # Here it doesn't use any context and just returns the response from elasticsearch
    if raw_context is None or context_dict == {}:
        app_logging.debug('No context detected')
        es_response = es_data.get_es_response(index_name, es_query, is_test)
        response = {
            'es_response': es_response,
        }
        return response

    # Here it uses the context to get the items with the context given
    app_logging.debug(f'Using context: {raw_context}')

    context_type = context_dict['context_type']
    if context_type in ['TO_COMPOUNDS', 'TO_TARGETS']:
        context = context_loader.get_search_by_ids_context(context_dict)
        subset_index_name = context['subset_index_name']
        es_response = es_data.get_es_response(subset_index_name, es_query, is_test)

        response = {
            'es_response': es_response,
        }
        return response

    contextual_sort_data = json.loads(raw_contextual_sort_data) if isinstance(raw_contextual_sort_data,
                                                                              str) else raw_contextual_sort_data
    contextual_sort_data = {} if contextual_sort_data is None else contextual_sort_data
    es_response, metadata = get_items_with_context(index_name, es_query, context_dict, contextual_sort_data)

    response = {
        'es_response': es_response,
        'metadata': metadata
    }
    return response


def complete_es_query(es_query):
    """
    makes sure that the query contains all the necessary fields:

    query.query.bool = {}
    query.query.bool.filter = []
    query.query.bool.should = []
    query.query.bool.must_not = []
    query.query.bool.must = []

    if the query has another structure that is not bool,it will NOT be replaced by the above structure

    :param es_query: dict containing the query to complete
    :return: the completed query
    """

    if 'query' not in es_query:
        es_query['query'] = {}

    already_contains_another_query = any(key != 'bool' for key in es_query['query'].keys())
    if already_contains_another_query:
        return es_query

    if 'bool' not in es_query['query']:
        es_query['query']['bool'] = {}
    if 'filter' not in es_query['query']['bool']:
        es_query['query']['bool']['filter'] = []
    if 'should' not in es_query['query']['bool']:
        es_query['query']['bool']['should'] = []
    if 'must_not' not in es_query['query']['bool']:
        es_query['query']['bool']['must_not'] = []
    if 'must' not in es_query['query']['bool']:
        es_query['query']['bool']['must'] = []
    return es_query


def get_es_doc(index_name, doc_id, source=None, custom_id_property=None):
    """
    :param index_name: name of the index to which the doc belongs
    :param doc_id: id of the document to search for
    :param source: list of properties to get
    :param custom_id_property: custom id property to use instead of _id
    :return: the json response from elasticsearch of the document
    """

    try:
        item_id = doc_id
        use_custom_id_property = custom_id_property is not None
        if use_custom_id_property:
            es_query = {
                "_source": source,
                "query": {
                    "terms": {
                        custom_id_property: [
                            doc_id
                        ]
                    }
                }
            }
            search_response = es_data.get_es_response(index_name, es_query)
            hits = search_response.get('hits', {}).get('hits', [])
            if len(hits) == 0:
                raise ESDataNotFoundError(f'No item found in index {index_name} with {custom_id_property} == {doc_id}')
            item_id = hits[0]['_id']

        response = es_data.get_es_doc(index_name, item_id, source)
        return response
    except es_data.ESDataNotFoundError as error:
        raise ESDataNotFoundError(str(error))


def get_items_with_context(index_name, es_query, context_dict, contextual_sort_data):
    """
    :param index_name: name of the index to query
    :param es_query: dict with the query to send to elasticsearch
    :param context_dict: context dict
    :param contextual_sort_data: dict describing the sorting by contextual properties
    :return: the items in the es_query with the context given in the context description
    """

    context, total_results = context_loader.get_structure_search_or_blast_context(context_dict)

    id_properties = es_mappings.get_id_properties_for_index(index_name)
    # create a context index so access is faster
    context_index = context_loader.load_context_index(id_properties, context)

    es_query_with_injections = context_injector.get_es_query_with_context_injections(es_query, contextual_sort_data,
                                                                                     id_properties, total_results,
                                                                                     context_index)
    es_response = es_data.get_es_response(index_name, es_query_with_injections)
    add_context_values_to_response(es_response, context_index)

    metadata = {
        'total_results': len(context_index),
        'max_results_injected': RUN_CONFIG.get('filter_query_max_clauses')
    }
    return es_response, metadata


def add_context_values_to_response(es_response, context_index):
    """
    adds the values of the context to the response of es
    :param es_response: response without the context values
    :param context_index: index of the context values
    """

    hits = es_response['hits']['hits']
    for hit in hits:
        hit_id = hit['_id']
        context_obj = context_index[hit_id]
        hit['_source'][context_injector.CONTEXT_PREFIX] = context_obj
