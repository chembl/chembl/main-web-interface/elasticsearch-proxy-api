"""
Schemas to validate the input of job status Endpoint
"""
from marshmallow import fields

from app.common_marshmallow.common_schemas import CommonSchema


class ESProxyQuery(CommonSchema):
    """
    Class that the schema for getting es data
    """
    index_name = fields.String(required=True)
    es_query = fields.String(required=True)
    context_obj = fields.String()
    contextual_sort_data = fields.String()


class ESProxyQueryB64(CommonSchema):
    """
    Class that the schema for getting es data, passing the parameters as a base64 string
    """
    b64_params = fields.String(required=True)


class ESProxyDoc(CommonSchema):
    """
    Class that the schema for getting a document by id
    """
    index_name = fields.String(required=True)
    doc_id = fields.String(required=True)
    source = fields.String()
    custom_id_property = fields.String()
