"""
    The blueprint used for handling requests to get generic es_data
"""
import base64
import json

from flask import Blueprint, jsonify, request

from app.request_validation.decorators import validate_form_with, validate_url_params_with
from app.blueprints.es_proxy.controllers import marshmallow_schemas
from app.blueprints.es_proxy.services import es_proxy_service
from app.common_marshmallow import utils as schema_utils
from app import app_logging
from app.http_cache import http_cache_utils
from app.http_cache.decorators import validate_http_cache
from app.usage_statistics.decorators import record_response_final_status
from app.request_error_logging_decorators import log_and_return_internal_server_error
from app import request_utils
from utils import request_parameters

ES_PROXY_BLUEPRINT = Blueprint('es_proxy', __name__)


@ES_PROXY_BLUEPRINT.route('/get_es_data', methods=['POST'])
@validate_form_with(marshmallow_schemas.ESProxyQuery)
@record_response_final_status()
@log_and_return_internal_server_error()
def get_es_data():
    """
    :return: the json response with the data from elasticsearch
    """
    form_data = request.form

    index_name = request_parameters.sanitise_parameter(form_data.get('index_name'))
    raw_es_query = request_parameters.sanitise_parameter(form_data.get('es_query'))
    raw_context = request_parameters.sanitise_parameter(form_data.get('context_obj'))
    raw_contextual_sort_data = request_parameters.sanitise_parameter(form_data.get('contextual_sort_data'))
    is_test = schema_utils.parse_boolean_param(form_data, 'is_test')

    app_logging.debug(f'index_name: {index_name}')
    app_logging.debug(f'raw_es_query: {raw_es_query}')
    app_logging.debug(f'raw_context: {raw_context}')
    app_logging.debug(f'raw_contextual_sort_data: {raw_contextual_sort_data}')
    app_logging.debug(f'is_test: {is_test}')

    json_response = es_proxy_service.get_es_data(
        index_name,
        raw_es_query,
        raw_context,
        raw_contextual_sort_data,
        is_test
    )

    return jsonify(json_response)


@ES_PROXY_BLUEPRINT.route('/get_es_data/<b64_params>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.ESProxyQueryB64)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_es_data_b64(b64_params):
    """
    :param b64_params: the same parameters as the POST endpoint (see get_es_data), but as a dict stringified in 64
    :return: the json response with the data from elasticsearch
    """

    unsafe_b64_params = b64_params.replace('-', '+').replace('_', '/')
    decoded_bytes = base64.b64decode(unsafe_b64_params)
    decoded_string = decoded_bytes.decode('ISO-8859-1')
    json_params = json.loads(decoded_string)
    index_name = request_parameters.sanitise_parameter(json_params.get('index_name'))
    raw_es_query = request_parameters.sanitise_parameter(json_params.get('es_query'))
    raw_context = request_parameters.sanitise_parameter(json_params.get('context_obj'))
    raw_contextual_sort_data = request_parameters.sanitise_parameter(json_params.get('contextual_sort_data'))

    json_response = es_proxy_service.get_es_data(
        index_name,
        raw_es_query,
        raw_context,
        raw_contextual_sort_data,
        is_test=False
    )
    return jsonify(json_response)


@ES_PROXY_BLUEPRINT.route('/get_es_document/<index_name>/<path:doc_id>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.ESProxyDoc)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_es_doc(index_name, doc_id):
    """
    :param index_name: name of the index to which the doc belongs
    :param doc_id: id of the document to search for
    :return: the json response with the es_doc data
    """
    try:
        source = request.args.get('source')
        custom_id_property = request.args.get('custom_id_property')
        json_response = es_proxy_service.get_es_doc(index_name, doc_id, source, custom_id_property)
        http_response = jsonify(json_response)
        http_cache_utils.add_cache_headers_to_response(http_response)
        return http_response
    except es_proxy_service.ESDataNotFoundError as error:
        return request_utils.log_and_return_not_found_404(error)
