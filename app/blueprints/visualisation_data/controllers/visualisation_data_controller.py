"""
Blueprint to handle requests asking to get visualisation data
"""
from flask import Blueprint, jsonify, request

from app.request_validation.decorators import validate_url_params_with, validate_form_with
from app.blueprints.visualisation_data.services import visualisation_data_service
from app.usage_statistics.decorators import record_response_final_status
from app.http_cache.decorators import validate_http_cache
from app.request_error_logging_decorators import log_and_return_internal_server_error
from app.blueprints.visualisation_data.controllers import marshmallow_schemas
from utils import request_parameters

VISUALISATION_DATA_BLUEPRINT = Blueprint('visualisation_data', __name__)


@VISUALISATION_DATA_BLUEPRINT.route('/target_classifications/protein_classification', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_protein_target_classification():
    """
    :return: the json response with the protein target classification
    """

    json_data = visualisation_data_service.get_protein_target_classification()
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/target_classifications/organism_taxonomy', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_organism_taxonomy_target_classification():
    """
    :return: the json response with the organism taxonomy classification
    """

    json_data = visualisation_data_service.get_organism_taxonomy_target_classification()
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/target_classifications/go_slim', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_go_slim_target_classification():
    """
    :return: the json response with the go_slim classification
    """
    json_data = visualisation_data_service.get_go_slim_target_classification()
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/assay_classifications/in_vivo', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_in_vivo_assay_classification():
    """
    :return: the json response with the in vivo assay classification
    """

    json_data = visualisation_data_service.get_in_vivo_assay_classification()
    return jsonify(json_data)

@VISUALISATION_DATA_BLUEPRINT.route('/drugs/indications_by_phase', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_drug_indications_by_phase():
    """
    :return: the json response with the drug indications by phase
    """
    json_data = visualisation_data_service.get_drug_indications_by_phase()
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/database_summary', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
def get_database_summary():
    """
    :return: the json response with the database summary
    """

    json_data = visualisation_data_service.get_database_summary()
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/entities_records', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
def get_entities_records():
    """
    :return: the json response with the entities records
    """

    json_data = visualisation_data_service.entities_records()
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/covid_entities_records', methods=['GET'])
@record_response_final_status()
@validate_http_cache()
def covid_entities_records():
    """
    :return: the json response with the covid entities records
    """

    json_data = visualisation_data_service.covid_entities_records()
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/get_id/<descriptor>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapSummaryQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_id(descriptor):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :return: an id that can be used in subsequent queries instead of passing the full descriptor
    """

    json_data = visualisation_data_service.get_heatmap_id(descriptor)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/get_id', methods=['POST'])
@validate_form_with(marshmallow_schemas.HeatmapSummaryQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_id_by_post():
    """
    :return: an id that can be used in subsequent queries instead of passing the full descriptor
    """
    form_data = request.form
    descriptor = request_parameters.sanitise_parameter(form_data.get('descriptor'))

    json_data = visualisation_data_service.get_heatmap_id(descriptor)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/get_descriptor/<heatmap_id>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapDescriptorQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_descriptor(heatmap_id):
    """
    :param heatmap_id: ID of the heatmap obtained from get_heatmap_id
    :return: the full descriptor corresponding to the heatmap id
    """

    json_data = visualisation_data_service.get_heatmap_descriptor(heatmap_id)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/<descriptor>/x_axis/summary', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapSummaryQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_summary_x_axis(descriptor):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :return: the json response with the heatmap summary for the x_axis
    """

    json_data = visualisation_data_service.get_heatmap_summary_x_axis(descriptor)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/<descriptor>/autocomplete_in_axis/<axis>/<term>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapAutocompleteQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_autocomplete_in_axis(descriptor, axis, term):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :param axis: the axis to autocomplete, x-axis or y-axis
    :param term: the term to autocomplete
    :return: the json response with the autocomplete items for the heatmap given the axis and the term
    """
    json_data = visualisation_data_service.get_heatmap_autocomplete_in_axis(descriptor, axis, term)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/<descriptor>/contextual_data', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapSummaryQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_contextual_data(descriptor):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :return: the json response with the contextual data of the heatmap
    """

    json_data = visualisation_data_service.get_heatmap_contextual_data(descriptor)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/<descriptor>/y_axis/summary', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapSummaryQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_summary_y_axis(descriptor):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :return: the json response with the heatmap summary for the y_axis
    """

    json_data = visualisation_data_service.get_heatmap_summary_y_axis(descriptor)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/<descriptor>/<axis>/<part_name>/<items_from>/<size>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapAxisHeadersQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_items(descriptor, axis, part_name, items_from, size):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :param axis: axis for which to ge the data
    :param part_name: name of the part to get; headers or footers
    :param items_from: start of the data window to retrieve
    :param size: size of the data window to retrieve
    :return: the items requested
    """

    json_data = visualisation_data_service.get_heatmap_items(descriptor, axis, part_name, items_from, size)
    return jsonify(json_data)


@VISUALISATION_DATA_BLUEPRINT.route('/heatmap/<descriptor>/cells/<x_items_from>/<x_size>/<y_items_from>/<y_size>',
                                    methods=['GET'])
@validate_url_params_with(marshmallow_schemas.HeatmapCellsQuery)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_heatmap_cells(descriptor, x_items_from, x_size, y_items_from, y_size):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :param x_items_from: start of the data window to retrieve on the x axis
    :param x_size: size of the data window to retrieve on the x axis
    :param y_items_from: start of the data window to retrieve on the y axis
    :param y_size: size of the data window to retrieve on the y axis
    :return: the cells requested
    """

    json_data = visualisation_data_service.get_heatmap_cells(descriptor, x_items_from, x_size, y_items_from, y_size)
    return jsonify(json_data)
