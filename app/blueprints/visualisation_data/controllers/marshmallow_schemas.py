"""
Marshmallow schemas for validating the Visualisation Endpoints
"""
from marshmallow import fields, validate

from app.common_marshmallow.common_schemas import CommonSchema


class HeatmapSummaryQuery(CommonSchema):
    """
    Class that the schema for getting the summary of the heatmap
    """
    descriptor = fields.String(required=True)


class HeatmapDescriptorQuery(CommonSchema):
    """
    Class that the schema for getting the summary of the heatmap
    """
    heatmap_id = fields.String(required=True)


class HeatmapAutocompleteQuery(CommonSchema):
    """
    Class that the schema for getting autocomplete results for the heatmap
    """
    descriptor = fields.String(required=True)
    axis = fields.String(required=True)
    term = fields.String(required=True)


class HeatmapAxisHeadersQuery(CommonSchema):
    """
    Class that the schema for getting the summary of the heatmap
    """
    descriptor = fields.String(required=True)
    axis = fields.String(required=True)
    part_name = fields.String(required=True)
    items_from = fields.Number(required=True, validate=validate.Range(min=0))
    size = fields.Number(required=True, validate=validate.Range(min=0))


class HeatmapCellsQuery(CommonSchema):
    """
    Class that the schema for getting the cells of the heatmap
    """
    descriptor = fields.String(required=True)
    x_items_from = fields.Number(required=True, validate=validate.Range(min=0))
    x_size = fields.Number(required=True, validate=validate.Range(min=0))
    y_items_from = fields.Number(required=True, validate=validate.Range(min=0))
    y_size = fields.Number(required=True, validate=validate.Range(min=0))
