"""
Service to handle requests asking to get visualisation data
"""
import base64
import json

from app.visualisation_data.drugs import drug_indications
from app.visualisation_data.assay_classification import in_vivo
from app.visualisation_data.target_classification import go_slim
from app.visualisation_data.target_classification import organism_taxonomy
from app.visualisation_data.target_classification import protein_class
from app.heatmap import heatmap_data
from app.cache.cache import CACHE, make_memoize_cache_key
from app.config import RUN_CONFIG
from app.es_data import es_data
from app.heatmap import standartisation
from app.url_shortening import url_shortener


class VisualisationDataServiceError(Exception):
    """
    Base class for errors in this module
    """


def get_protein_target_classification():
    """
    :return: the protein target classification tree
    """
    return protein_class.get_classification_tree()


def get_organism_taxonomy_target_classification():
    """
    :return: the organism taxonomy target classification tree
    """
    return organism_taxonomy.get_classification_tree()


def get_go_slim_target_classification():
    """
    :return: the go slim target classification tree
    """
    return go_slim.get_classification_tree()


def get_in_vivo_assay_classification():
    """
    :return: the in vivo assay classification tree
    """
    return in_vivo.get_classification_tree()


def get_drug_indications_by_phase():
    """
    :return: the drug indications by phase
    """
    return drug_indications.get_drug_indications_by_phase()


@CACHE.memoize(timeout=RUN_CONFIG.get('es_proxy_cache_seconds'), make_name=make_memoize_cache_key)
def get_database_summary():
    """
    :return: the database_summary
    """

    index_name = 'chembl_document'
    es_query = {
        "_source": False,
        "query": {
            "bool": {
                "filter": {
                    "term": {
                        "doc_type": "DATASET"
                    }
                },
                "must": {
                    "range": {
                        "_metadata.related_activities.count": {
                            "gt": 0
                        }
                    }
                },
                "must_not": {
                    "terms": {
                        "_metadata.source.src_id": [1, 7, 8, 9, 7, 8, 9, 11, 12, 13, 15, 18, 25, 26, 28, 31,
                                                    35, 37, 38,
                                                    39, 41, 42]
                    }
                }
            }
        },
        "track_total_hits": True,
    }

    es_response = es_data.get_es_response(index_name, es_query)

    return {
        'num_datasets': es_response['hits']['total']['value']
    }


# @CACHE.memoize(timeout=RUN_CONFIG.get('es_proxy_cache_seconds'))
def entities_records():
    """
    :return: the database_summary
    """
    drugs_query = {

        "term": {
            "_metadata.drug.is_drug": True
        }

    }

    return {
        'Compounds': get_entity_total_count('chembl_molecule'),
        'Drugs': get_entity_total_count('chembl_molecule', drugs_query),
        'Assays': get_entity_total_count('chembl_assay'),
        'Documents': get_entity_total_count('chembl_document'),
        'Targets': get_entity_total_count('chembl_target'),
        'Cells': get_entity_total_count('chembl_cell_line'),
        'Tissues': get_entity_total_count('chembl_tissue'),
        'Indications': get_entity_total_count('chembl_drug_indication_by_parent'),
        'Mechanisms': get_entity_total_count('chembl_mechanism_by_parent_target'),
        'Drug Warnings': get_entity_total_count('chembl_drug_warning_by_parent')
    }


@CACHE.memoize(timeout=RUN_CONFIG.get('es_proxy_cache_seconds'), make_name=make_memoize_cache_key)
def covid_entities_records():
    """
    :return: the database_summary
    """

    covid_compounds_query = {
        "query_string": {
            "query": "_metadata.compound_records.src_id:52",
        }
    }

    covid_assays_query = {
        "query_string": {
            "query": "_metadata.source.src_id:52",
        }
    }

    covid_documents_query = {
        "query_string": {
            "query": "_metadata.source.src_id:52 AND NOT document_chembl_id:CHEMBL4303081",
        }
    }

    covid_activities_query = {
        "query_string": {
            "query": "_metadata.source.src_id:52",
        }
    }

    return {
        'Compounds': get_entity_total_count('chembl_molecule', query=covid_compounds_query),
        'Assays': get_entity_total_count('chembl_assay', query=covid_assays_query),
        'Documents': get_entity_total_count('chembl_document', query=covid_documents_query),
        'Activities': get_entity_total_count('chembl_activity', query=covid_activities_query),
    }


def get_entity_total_count(index_name, query=None):
    """
    :param index_name: index to query
    :param query: query to apply
    :return: the total count of items of the given entity
    """
    total_count_query = {
        "_source": False,
        "track_total_hits": True,
    }
    if query is not None:
        total_count_query['query'] = query

    es_response = es_data.get_es_response(index_name, total_count_query)

    num_items = es_response['hits']['total']['value']

    return num_items


def get_heatmap_id(descriptor):
    """
    :param descriptor: descriptor of the heatmap in base 64
    :return: an id that can be used in subsequent queries instead of passing the full descriptor
    """
    return url_shortener.shorten_url(descriptor)


def get_heatmap_descriptor(heatmap_id):
    """
    :param heatmap_id:  ID of the heatmap obtained from get_heatmap_id
    :return: the full descriptor corresponding to the heatmap id
    """
    expanded = url_shortener.expand_url(heatmap_id)
    expires = expanded['expires']
    b64_descriptor = expanded['long_url']

    decoded_text = base64.b64decode(b64_descriptor.replace('-', '+').replace('_', '/').encode())
    descriptor = json.loads(decoded_text)

    return {
        'expires': expires,
        'descriptor': descriptor,
        'b64_descriptor': b64_descriptor
    }


def get_heatmap_summary_x_axis(raw_descriptor):
    """
    :param raw_descriptor: descriptor of the heatmap in base 64
    :return: a dict with the heatmap summary (counts, entity names, etc) for the x_axis
    """
    descriptor = parse_heatmap_descriptor(raw_descriptor)
    return heatmap_data.get_heatmap_axis_summary(descriptor, standartisation.HeatmapAxes.X_AXIS)


def get_heatmap_autocomplete_in_axis(raw_descriptor, raw_axis_id, term):
    """
    :param raw_descriptor: descriptor of the heatmap in base 64
    :param raw_axis_id: axis id for which to ge the data
    :param term: query to apply
    :return: a dict with the heatmap summary (counts, entity names, etc) for the x_axis
    """
    descriptor = parse_heatmap_descriptor(raw_descriptor)
    axis_id = parse_axis(raw_axis_id)
    return heatmap_data.get_heatmap_autocomplete_in_axis(descriptor, axis_id, term)


def get_heatmap_contextual_data(raw_descriptor):
    """
    :param raw_descriptor: descriptor of the heatmap in base 64
    :return: a dict with the contextual data of the heatmap
    """
    descriptor = parse_heatmap_descriptor(raw_descriptor)
    return heatmap_data.get_heatmap_contextual_data(descriptor)


def get_heatmap_summary_y_axis(raw_descriptor):
    """
    :param raw_descriptor: descriptor of the heatmap in base 64
    :return: a dict with the heatmap summary (counts, entity names, etc) for the y_axis
    """
    descriptor = parse_heatmap_descriptor(raw_descriptor)
    return heatmap_data.get_heatmap_axis_summary(descriptor, standartisation.HeatmapAxes.Y_AXIS)


def get_heatmap_items(raw_descriptor, raw_axis_id, part_name, items_from, size):
    """
    :param raw_descriptor: descriptor of the heatmap in base 64
    :param raw_axis_id: axis id for which to ge the data
    :param part_name: name of the part to get; headers or footers
    :param items_from: start of the data window to retrieve
    :param size: size of the data window to retrieve
    :return: the items requested
    """
    descriptor = parse_heatmap_descriptor(raw_descriptor)
    axis = parse_axis(raw_axis_id)

    return heatmap_data.get_heatmap_items(descriptor, axis, part_name, items_from, size)


def get_heatmap_cells(raw_descriptor, x_items_from, x_size, y_items_from, y_size):
    """
    :param raw_descriptor: descriptor of the heatmap in base 64
    :param x_items_from: start of the data window to retrieve on the x axis
    :param x_size: size of the data window to retrieve on the x axis
    :param y_items_from: start of the data window to retrieve on the y axis
    :param y_size: size of the data window to retrieve on the y axis
    :return: the cells requested
    """
    descriptor = parse_heatmap_descriptor(raw_descriptor)

    return heatmap_data.get_heatmap_cells(descriptor, x_items_from, x_size, y_items_from, y_size)


# ----------------------------------------------------------------------------------------------------------------------
# Utils
# ----------------------------------------------------------------------------------------------------------------------
def parse_heatmap_descriptor(raw_descriptor):
    """
    :param raw_descriptor: descriptor as a string in b64 representing the javascript object that has all the heatmap
    details
    :return: a dict with the parsed data
    """
    heatmap_descriptor = raw_descriptor
    if raw_descriptor.startswith('HEATMAP_ID:'):
        heatmap_id = raw_descriptor.replace('HEATMAP_ID:', '')
        heatmap_descriptor = url_shortener.expand_url(heatmap_id)['long_url']

    decoded_text = base64.b64decode(heatmap_descriptor.replace('-', '+').replace('_', '/').encode())
    descriptor = json.loads(decoded_text)

    return descriptor


def parse_axis(raw_axis_id):
    """
    :param raw_axis_id: the axis id passed in the url
    :return: an instance of HeatmapAxes
    """
    if raw_axis_id == 'x_axis':
        return standartisation.HeatmapAxes.X_AXIS
    if raw_axis_id == 'y_axis':
        return standartisation.HeatmapAxes.Y_AXIS
    raise VisualisationDataServiceError(f'The axis {raw_axis_id} does not exist!')
