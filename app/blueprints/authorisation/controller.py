"""
Blueprint for authorisation of special access endpoints of the system
"""
from flask import Blueprint, jsonify, request, make_response

from app.blueprints.authorisation import service

ACCESS_CONTROL_AUTH_BLUEPRINT = Blueprint('authorisation', __name__)


@ACCESS_CONTROL_AUTH_BLUEPRINT.route('/login', methods=['GET'])
def login():
    """
    Validates the login credentials and returns a token if the credentials are valid
    """
    auth = request.authorization

    if auth is None:
        return make_response('No login credentials were provided!', 400,
                             {'WWW-Authenticate': 'Basic realm="Login Required'})

    try:
        token = service.get_special_access_token(auth.username, auth.password)
        return jsonify({'token': token})
    except service.InvalidCredentialsError as error:
        return make_response(str(error), 401, {'WWW-Authenticate': 'Basic realm="Login Required'})
