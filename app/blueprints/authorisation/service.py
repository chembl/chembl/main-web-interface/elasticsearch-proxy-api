"""
This module checks the login info provided and returns a token if the credentials are valid
"""
from app.config import RUN_CONFIG
from app.config import verify_secret
from app.authorisation import token_generator


class InvalidCredentialsError(Exception):
    """Class for exceptions in this module."""


def get_special_access_token(username, password):
    """
    :param username: username provided
    :param password: password provided
    :return: the access token for endpoints that require special access
    """

    username_must_be = RUN_CONFIG.get('special_access_username')
    username_is_correct = username_must_be == username
    password_is_correct = verify_secret('special_access_password', password)

    if username_is_correct and password_is_correct:
        return token_generator.generate_special_access_token()

    raise InvalidCredentialsError('Could not verify the credentials provided!')
