"""
Marshmallow schemas for validating the properties configuration controller
"""
from marshmallow import fields

from app.common_marshmallow.common_schemas import CommonSchema


class PropertyConfigRequest(CommonSchema):
    """
    Class with the schema for getting the configuration of a property
    """
    index_name = fields.String(required=True)
    property_id = fields.String(required=True)


class GroupConfigRequest(CommonSchema):
    """
    Class with the schema for getting the configuration of a group
    """
    index_name = fields.String(required=True)
    group_name = fields.String(required=True)


class ListConfigRequest(CommonSchema):
    """
    Class with the schema for getting the configuration of a list of properties
    """
    index_name = fields.String(required=True)
    properties = fields.String(required=True)


class FacetsGroupConfigRequest(CommonSchema):
    """
    Class with the schema for getting the configuration of a facet group
    """
    index_name = fields.String(required=True)
    group_name = fields.String(required=True)


class IDPropertiesRequest(CommonSchema):
    """
    Class with the schema for getting the id properties of an index
    """
    index_name = fields.String(required=True)


class AllPropertiesRequest(CommonSchema):
    """
    Class with the schema for getting all the properties config of an index
    """
    index_name = fields.String(required=True)
