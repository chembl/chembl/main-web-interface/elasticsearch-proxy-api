"""
Properties configuration controller
"""
import urllib.parse

from flask import Blueprint, jsonify

from app.blueprints.properties_config.controllers import marshmallow_schemas
from app.request_validation.decorators import validate_url_params_with
from app.blueprints.properties_config.services import properties_config_service
from app.usage_statistics.decorators import record_response_final_status
from app.http_cache.decorators import validate_http_cache
from app.request_error_logging_decorators import log_and_return_internal_server_error

PROPERTIES_CONFIG_BLUEPRINT = Blueprint('properties_configuration', __name__)


@PROPERTIES_CONFIG_BLUEPRINT.route('/property/<index_name>/<property_id>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.PropertyConfigRequest)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_property_config(index_name, property_id):
    """
    :param index_name: name of the index to which the property belongs
    :param property_id: id of the property to check
    :return: the response for requests asking for property configurations
    """

    properties_config = properties_config_service.get_property_config(index_name, property_id)
    return jsonify(properties_config)


@PROPERTIES_CONFIG_BLUEPRINT.route('/group/<index_name>/<group_name>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.GroupConfigRequest)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_group_config(index_name, group_name):
    """
    :param index_name: name of the index to which the group belongs
    :param group_name: name of the group to check
    :return: the response for requests asking for group configurations
    """

    group_config = properties_config_service.get_group_config(index_name, group_name)
    return jsonify(group_config)


@PROPERTIES_CONFIG_BLUEPRINT.route('/facets/<index_name>/<group_name>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.FacetsGroupConfigRequest)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_facet_group_config(index_name, group_name):
    """
    :param index_name: name of the index to which the facet group belongs
    :param group_name: name of the group to check
    :return: the response for requests asking for facet group configurations
    """

    facet_group_config = properties_config_service.get_facets_group_config(index_name, group_name)
    return jsonify(facet_group_config)


@PROPERTIES_CONFIG_BLUEPRINT.route('/list/<index_name>/<properties>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.ListConfigRequest)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_list_config(index_name, properties):
    """
    :param index_name: name of the index to which the list of properties belongs
    :param properties: text list of the properties separated by comma
    :return: the response for requests asking for the configurations of a list of properties
    """

    list_config = properties_config_service.get_list_config(index_name, urllib.parse.unquote(properties))
    return jsonify(list_config)


@PROPERTIES_CONFIG_BLUEPRINT.route('/id_properties/<index_name>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.IDPropertiesRequest)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_index_id_properties(index_name):
    """
    :param index_name: name of the index for which to get the id properties
    :return: the response for requests asking for facet group configurations
    """

    id_properties = properties_config_service.get_index_properties_of_index(index_name)
    return jsonify(id_properties)


@PROPERTIES_CONFIG_BLUEPRINT.route('/all_properties/<index_name>', methods=['GET'])
@validate_url_params_with(marshmallow_schemas.AllPropertiesRequest)
@record_response_final_status()
@validate_http_cache()
@log_and_return_internal_server_error()
def get_all_configured_properties_for_index(index_name):
    """
    :param index_name: name of the index for which to get the all the properties config
    :return: the json response with the all properties configuration
    """

    all_properties_configured = properties_config_service.get_all_configured_properties_for_index(index_name)
    return jsonify(all_properties_configured)
