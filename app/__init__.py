"""
Entry file for the elasticsearch proxy app
"""

from flask import Flask
from flask_cors import CORS

from app.config import RUN_CONFIG
from app.cache.cache import CACHE
from app.blueprints.swagger_description.swagger_description_blueprint import SWAGGER_BLUEPRINT
from app.blueprints.es_proxy.controllers.es_proxy_controller import ES_PROXY_BLUEPRINT
from app.blueprints.properties_config.controllers.properties_config_controller import PROPERTIES_CONFIG_BLUEPRINT
from app.blueprints.contexts.controllers.contexts_controller import CONTEXTS_BLUEPRINT
from app.blueprints.search.controllers.search_controller import SEARCH_BLUEPRINT
from app.blueprints.url_shortening.controllers.url_shortening_controller import URL_SHORTENING_BLUEPRINT
from app.blueprints.element_usage_blueprint.controllers.element_usage_controller import ELEMENT_USAGE_BLUEPRINT
from app.blueprints.visualisation_data.controllers.visualisation_data_controller import VISUALISATION_DATA_BLUEPRINT
from app.blueprints.utils.controllers.utils_controller import UTILS_BLUEPRINT
from app.blueprints.entities_join.controllers.entities_join_controller import ENTITIES_JOIN_BLUEPRINT
from app.blueprints.eubopen.search.controller import EUBOPEN_SEARCH_BLUEPRINT
from app.blueprints.eubopen.visualisations.controller import EUBOPEN_VISUALISATIONS_BLUEPRINT
from app.blueprints.eubopen.miscellaneous.controller import EUBOPEN_MISC_BLUEPRINT
from app.blueprints.authorisation.controller import ACCESS_CONTROL_AUTH_BLUEPRINT


def create_app():
    """
    Creates the flask app
    :return: Elasticsearch proxy flask app
    """
    base_path = RUN_CONFIG.get('base_path')
    flask_app = Flask(__name__)

    enable_cors = RUN_CONFIG.get('enable_cors', False)

    if enable_cors:
        CORS(flask_app)

    with flask_app.app_context():
        CACHE.init_app(flask_app)

        # pylint: disable=protected-access
        if RUN_CONFIG.get('cache_config').get('CACHE_TYPE') == 'memcached':
            CACHE.cache._client.behaviors['tcp_nodelay'] = True
            CACHE.cache._client.behaviors['_noreply'] = True
            CACHE.cache._client.behaviors['no_block'] = True
            CACHE.cache._client.behaviors['remove_failed'] = 10
            CACHE.cache._client.behaviors['retry_timeout'] = 10
            CACHE.cache._client.behaviors['retry_timeout'] = 600

    blueprints_urls = [
        {
            'blueprint': SWAGGER_BLUEPRINT,
            'url': '/swagger'
        },
        {
            'blueprint': ES_PROXY_BLUEPRINT,
            'url': '/es_data'
        },
        {
            'blueprint': PROPERTIES_CONFIG_BLUEPRINT,
            'url': '/properties_configuration'
        },
        {
            'blueprint': CONTEXTS_BLUEPRINT,
            'url': '/contexts'
        },
        {
            'blueprint': SEARCH_BLUEPRINT,
            'url': '/search'
        },
        {
            'blueprint': URL_SHORTENING_BLUEPRINT,
            'url': '/url_shortening'
        },
        {
            'blueprint': ELEMENT_USAGE_BLUEPRINT,
            'url': '/frontend_element_usage'
        },
        {
            'blueprint': VISUALISATION_DATA_BLUEPRINT,
            'url': '/visualisations'
        },
        {
            'blueprint': UTILS_BLUEPRINT,
            'url': '/utils'
        },
        {
            'blueprint': ENTITIES_JOIN_BLUEPRINT,
            'url': '/entities_join'
        },
        {
            'blueprint': EUBOPEN_SEARCH_BLUEPRINT,
            'url': '/eubopen/search'
        },
        {
            'blueprint': EUBOPEN_VISUALISATIONS_BLUEPRINT,
            'url': '/eubopen/visualisations'
        },
        {
            'blueprint': EUBOPEN_MISC_BLUEPRINT,
            'url': '/eubopen/miscellaneous'
        },
        {
            'blueprint': ACCESS_CONTROL_AUTH_BLUEPRINT,
            'url': '/authorisation'
        }
    ]

    for blueprint_desc in blueprints_urls:
        blueprint = blueprint_desc['blueprint']
        url = blueprint_desc['url']
        flask_app.register_blueprint(blueprint, url_prefix=f'{base_path}/{url}')
        print(f'{url} LOADED')

    print(f'LOADED {len(blueprints_urls)} BLUEPRINTS')

    return flask_app


if __name__ == '__main__':
    FLASK_APP = create_app()
