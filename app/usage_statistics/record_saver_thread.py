"""
Module that provides a Thread to record save statistics without affecting the request response time
"""
import threading

from app.es_monitoring_connection import ES_MONITORING
from app import app_logging
from app.config import RUN_CONFIG


class RecordsSaverThread(threading.Thread):
    """
    Class that implement that saves a record to the statistics without affecting the response time
    """

    def __init__(self, index_name, document):
        threading.Thread.__init__(self, )
        self.index_name = index_name
        self.document = document
        app_logging.debug('Initialising thread to save statistics')

    def run(self):

        es_host = RUN_CONFIG.get('usage_statistics', {}).get('elasticsearch', {}).get('host')
        es_port = RUN_CONFIG.get('usage_statistics', {}).get('elasticsearch', {}).get('port')

        app_logging.debug(f'Sending the following record to the statistics in a separate thread: {self.document} '
                          f'index name: {self.index_name} es_host: {es_host}:{es_port}')

        result = ES_MONITORING.index(index=self.index_name, body=self.document, doc_type='_doc')
        app_logging.debug(f'Result {result}')
