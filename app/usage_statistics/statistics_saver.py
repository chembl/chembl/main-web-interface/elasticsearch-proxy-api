"""
Module that saves statistics in elasticsearch
"""
import json
from datetime import datetime

from app.config import RUN_CONFIG
from app.config import ImproperlyConfiguredError
from app import app_logging
from app.usage_statistics.old_monitoring_records_deletion import OldMonitoringRecordsDeletionThread
from app.usage_statistics import record_saver_thread


def get_index_usage_record_dict(es_index, es_full_query, es_request_digest, is_cached, request_date, run_env_type,
                                time_taken):
    """
    :param es_index: index used in the request
    :param es_full_query: full query sent requested
    :param es_request_digest: digest of the request
    :param is_cached: whether the data was cached or not
    :param request_date: timestamp for the request date
    :param run_env_type: type of run environment
    :param time_taken: time taken (seconds) to get the data
    :return: a dict to be used to save the cache statistics in the elasticsearch index
    """

    es_query = json.dumps(es_full_query.get('query', None))
    es_aggs = json.dumps(es_full_query.get('aggs', None))

    return {
        'es_index': es_index,
        'es_query': es_query,
        'es_aggs': es_aggs,
        'es_request_digest': es_request_digest,
        'is_cached': is_cached,
        'request_date': request_date,
        'run_env_type': run_env_type,
        'time_taken': time_taken
    }


def save_index_usage_record(es_index, es_full_query, es_request_digest, is_cached, time_taken):
    """
    Saves the job record in elasticsearch with the parameters given
    :param es_index: index used in the request
    :param es_full_query: full query sent requested
    :param es_request_digest: digest of the request
    :param is_cached: whether the data was cached or not
    :return: a dict to be used to save the cache statistics in the elasticsearch index
    :param time_taken: time taken (seconds) to get the data
    """

    request_date = datetime.now().timestamp() * 1000
    run_env_type = RUN_CONFIG.get('run_env')

    cache_record_dict = get_index_usage_record_dict(es_index, es_full_query, es_request_digest, is_cached, request_date,
                                                    run_env_type, time_taken)

    index_name = RUN_CONFIG.get('usage_statistics').get('cache_statistics_index')

    if index_name is None:
        raise ImproperlyConfiguredError('You must provide an index name to save job statistics in'
                                        ' job_statistics.general_statistics_index')

    save_record_to_elasticsearch(cache_record_dict, index_name)


def save_free_text_search_record(time_taken):
    """
    saves the record in the monitoring elasticsearch with the parameters given
    :param time_taken: time taken to process the search
    """

    doc = {
        'search_type': 'FREE_TEXT',
        'time_taken': time_taken,
        'host': 'es_proxy_api_k8s',
        'is_new': False,
        'request_date': datetime.utcnow().timestamp() * 1000,
        'run_env_type': RUN_CONFIG.get('run_env')
    }

    index_name = RUN_CONFIG.get('usage_statistics').get('search_statistics_index')
    save_record_to_elasticsearch(doc, index_name)


def save_similarity_search_record(search_type, time_taken):
    """
    saves the record in the monitoring elasticsearch with the parameters given
    :param search_type: search type (SIMILARITY, SUBSTRUCTURE, CONNECTIVITY)
    :param time_taken: time taken to run the search
    """
    doc = {
        'search_type': search_type,
        'time_taken': time_taken,
        'host': 'es_proxy_api_k8s',
        'is_new': False,
        'request_date': datetime.utcnow().timestamp() * 1000,
        'run_env_type': RUN_CONFIG.get('run_env')
    }

    index_name = RUN_CONFIG.get('usage_statistics').get('search_statistics_index')
    save_record_to_elasticsearch(doc, index_name)


# ----------------------------------------------------------------------------------------------------------------------
# URL shortening
# ----------------------------------------------------------------------------------------------------------------------
def record_url_was_shortened(expires, url_hash, long_url, num_urls_stored):
    """
    Records that an url was shortened.
    :param expires: when does the url expires
    :param url_hash: what is the hash of the url
    :param long_url: what is the long version of the url
    :param num_urls_stored: number of urls currently stored
    """
    doc = {
        "event": "URL_SHORTENED",
        "run_env_type": RUN_CONFIG.get('run_env'),
        "host": 'es_proxy_api_k8s',
        "request_date": datetime.utcnow().timestamp() * 1000,
        "expires": expires,
        "url_hash": url_hash,
        "long_url": long_url,
        "num_urls_stored": num_urls_stored
    }

    index_name = RUN_CONFIG.get('url_shortening').get('statistics_index_name')
    save_record_to_elasticsearch(doc, index_name)


def record_expired_urls_were_deleted():
    """
    Records that the expired urls were deleted
    """
    doc = {
        "event": "EXPIRED_URLS_DELETED",
        "run_env_type": RUN_CONFIG.get('run_env'),
        "host": 'es_proxy_api_k8s',
        "request_date": datetime.utcnow().timestamp() * 1000,
    }

    index_name = RUN_CONFIG.get('url_shortening').get('statistics_index_name')
    save_record_to_elasticsearch(doc, index_name)


def record_url_was_expanded(expires, url_hash, long_url, num_urls_stored):
    """
    Records that an url was shortened.
    :param expires: when does the url expires
    :param url_hash: what is the hash of the url
    :param long_url: what is the long version of the url
    :param num_urls_stored: number of urls currently stored
    """
    doc = {
        "event": "URL_EXPANDED",
        "run_env_type": RUN_CONFIG.get('run_env'),
        "host": 'es_proxy_api_k8s',
        "request_date": datetime.utcnow().timestamp() * 1000,
        "expires": expires,
        "url_hash": url_hash,
        "long_url": long_url,
        "num_urls_stored": num_urls_stored
    }

    index_name = RUN_CONFIG.get('url_shortening').get('statistics_index_name')
    save_record_to_elasticsearch(doc, index_name)


# ----------------------------------------------------------------------------------------------------------------------
# HTTP status
# ----------------------------------------------------------------------------------------------------------------------
def record_http_final_status(method, full_path, str_form_data, response_code, user_agent, time_taken):
    """
    records in the statistics the final status of a response
    :param method: method used in the request
    :param full_path: full para of the request with parameters
    :param str_form_data: stringified version of the form data sent as payload
    :param response_code: response code of the request
    :param user_agent: user agent of the request
    :param time_taken: time taken to process the response
    """
    doc = {
        "request_date": datetime.utcnow().timestamp() * 1000,
        "method": method,
        "full_path": full_path,
        "form_data": str_form_data,
        "response_code": response_code,
        "user_agent": user_agent,
        "time_taken": time_taken,
    }
    index_name = RUN_CONFIG.get('usage_statistics').get('http_status_index')
    save_record_to_elasticsearch(doc, index_name)


# ----------------------------------------------------------------------------------------------------------------------
# Saving records to elasticsearch
# ----------------------------------------------------------------------------------------------------------------------
def save_record_to_elasticsearch(doc, index_name):
    """
    Saves the record indicated as parameter to the index indicated as parameter
    :param doc: doc to save
    :param index_name: index where to save the dod
    """

    dry_run = RUN_CONFIG.get('usage_statistics', {}).get('dry_run', False)
    if dry_run:
        app_logging.debug(f'Not actually sending the record to the statistics index {index_name} (dry run): {doc}')
    else:
        saver_thread = record_saver_thread.RecordsSaverThread(index_name, doc)
        saver_thread.start()


# ----------------------------------------------------------------------------------------------------------------------
# Deletion of old records
# ----------------------------------------------------------------------------------------------------------------------
def trigger_deletion_of_old_monitoring_records():
    """
    Triggers the deletion of old monitoring records
    """
    deletion_thread = OldMonitoringRecordsDeletionThread()
    deletion_thread.start()
