"""
Decorators used in helping to register the final statuses of the requests.
"""
# import time

from functools import wraps


# from flask import request
#
# from app.usage_statistics import statistics_saver


def record_response_final_status():
    """
    Saves to the statistics the final status in the request and the parameters entered
    """

    def wrap(func):
        @wraps(func)
        def wrapped_func(*args, **kwargs):
            # This has been disabled!
            # start_time = time.time()
            response = func(*args, **kwargs)
            # end_time = time.time()
            # time_taken = end_time - start_time
            #
            # method = request.method
            # full_path = request.full_path
            # str_form_data = str(request.form.to_dict(flat=False))
            # response_code = str(response.status_code)
            # user_agent = request.headers.get('User-Agent')
            # statistics_saver.record_http_final_status(method, full_path, str_form_data, response_code, user_agent,
            #                                           time_taken)
            return response

        return wrapped_func

    return wrap
