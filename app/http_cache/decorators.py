"""
Decorators used in the handling of the http cache
"""
from functools import wraps
from datetime import datetime, timedelta

from flask import request, Response
from app.config import RUN_CONFIG
from app import app_logging


def validate_http_cache():
    """
    Validates the http headers in the request and returns 304 not modified when applicable.
    Also includes a Last-Modified header when applicable
    """

    def wrap(func):
        @wraps(func)
        def wrapped_func(*args, **kwargs):
            last_modified = RUN_CONFIG.get('http_cache').get('headers').get('Last-Modified')
            requested_last_modified = request.headers.get('If-Modified-Since')
            app_logging.debug(f'HTTP Cache control for {request.full_path}')
            app_logging.debug(f'If-Modified-Since: {requested_last_modified}')

            if requested_last_modified == last_modified:
                app_logging.debug('NOT MODIFIED')
                return Response(status=304)

            app_logging.debug('WAS MODIFIED')
            response = func(*args, **kwargs)
            hours_valid = RUN_CONFIG.get('http_cache').get('hours_valid')
            expires = datetime.utcnow() + timedelta(hours=hours_valid)
            response.headers.add('Expires', expires.strftime("%a, %d %b %Y %H:%M:%S GMT"))
            response.headers.add('Cache-Control', f'public,max-age={int(3600 * hours_valid)}')
            response.headers.add('Last-Modified', last_modified)
            response.add_etag()

            return response

        return wrapped_func

    return wrap
