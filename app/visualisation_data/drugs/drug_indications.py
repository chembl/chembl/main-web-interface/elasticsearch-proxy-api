"""
Module that generates the drug indications by phase tree
"""
from app.config import RUN_CONFIG
from app.cache.decorators import return_if_cached_results
from app.visualisation_data.shared.tree_generator import TargetHierarchyTreeGenerator


@return_if_cached_results({
    'cache_key_generator': lambda: f'drug_indications_by_phase-{RUN_CONFIG.get("cache_key_suffix")}',
    'timeout': int(3.154e7)
})
def get_drug_indications_by_phase():
    """
    :return: the in drug indications by phase tree.
    """
    index_name = 'chembl_molecule'
    es_query = {
        "size": 0,
        "query": {
            "term": {
                "_metadata.drug.is_drug": True
            }
        },
        "aggs": {
            "children": {
                "terms": {
                    "field": "_metadata.compound_generated.max_phase_label",
                    "size": 10
                },
                "aggs": {
                    "children": {
                        "terms": {
                            "field": "_metadata.drug_indications.efo_term",
                            "size": 20
                        }
                    }
                }
            }
        }
    }

    def generate_count_query(path_to_node):
        """
        :param path_to_node: path from the root to the node
        :return: the query to count how many items belong to the node
        """
        queries = ['_metadata.drug.is_drug:true']
        # there should be maximum 2 levels
        first_node_in_path = path_to_node[0]
        queries.append(f'_metadata.compound_generated.max_phase_label:("{first_node_in_path}")')

        if len(path_to_node) > 1:
            second_node_in_path = path_to_node[1]
            queries.append(f'_metadata.drug_indications.efo_term:("{second_node_in_path}")')

        return ' AND '.join(queries)

    tree_generator = TargetHierarchyTreeGenerator(index_name=index_name, es_query=es_query,
                                                  query_generator=generate_count_query,
                                                  count_index=index_name)

    final_tree = tree_generator.get_classification_tree()

    return final_tree
