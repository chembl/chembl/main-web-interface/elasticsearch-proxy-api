"""
Module that generates the protein target classification for eubopen
"""
from app.cache import cache
from app import app_logging
from app.visualisation_data.shared.tree_generator import TargetHierarchyTreeGenerator
from app.config import RUN_CONFIG
from utils import json_files_loading


def get_classification_tree():
    """
    :return: the protein target classification tree
    """
    cache_key = f'target_classifications_protein_class_eubopen-{RUN_CONFIG.get("cache_key_suffix")}'
    app_logging.debug(f'cache_key: {cache_key}')

    cache_response = cache.fail_proof_get(key=cache_key)

    if cache_response is not None:
        app_logging.debug('results are in cache')
        return cache_response

    index_name = 'chembl_protein_class'
    query_file_path = 'app/visualisation_data/target_classification/data/protein_class_tree_query.json'
    es_query = json_files_loading.load_json_from_path(query_file_path)

    def generate_count_query(path_to_node):

        queries = []
        level = 1
        for node in path_to_node:
            queries.append('_metadata.protein_classification.l{level}:("{class_name}")'.format(level=level,
                                                                                               class_name=node))
            level += 1

        classes_queries = ' AND '.join(queries)
        return classes_queries

    tree_generator = TargetHierarchyTreeGenerator(index_name=index_name, es_query=es_query,
                                                  query_generator=generate_count_query,
                                                  count_index='chembl_eubopen_target')

    final_tree = tree_generator.get_classification_tree()

    cache_time = int(3.154e7)
    cache.fail_proof_set(key=cache_key, value=final_tree, timeout=cache_time)

    return final_tree
