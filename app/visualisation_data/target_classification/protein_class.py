"""
Module that generates the protein target classification
"""
from app.visualisation_data.shared.tree_generator import TargetHierarchyTreeGenerator
from app.config import RUN_CONFIG
from app.cache.decorators import return_if_cached_results
from utils import json_files_loading


@return_if_cached_results({
    'cache_key_generator': lambda: f'target_classifications_protein_class-{RUN_CONFIG.get("cache_key_suffix")}',
    'timeout': int(3.154e7)
})
def get_classification_tree():
    """
    :return: the protein target classification tree
    """
    index_name = 'chembl_protein_class'
    query_file_path = 'app/visualisation_data/target_classification/data/protein_class_tree_query.json'
    es_query = json_files_loading.load_json_from_path(query_file_path)

    def generate_count_query(path_to_node):
        queries = []
        level = 1
        for node in path_to_node:
            queries.append('_metadata.protein_classification.l{level}:("{class_name}")'.format(level=level,
                                                                                               class_name=node))
            level += 1

        return ' AND '.join(queries)

    tree_generator = TargetHierarchyTreeGenerator(index_name=index_name, es_query=es_query,
                                                  query_generator=generate_count_query,
                                                  count_index='chembl_target')

    final_tree = tree_generator.get_classification_tree()

    return final_tree
