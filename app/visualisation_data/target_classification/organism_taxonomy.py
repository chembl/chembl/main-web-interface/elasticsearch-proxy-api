"""
Module that generates the organism taxonomy target classification
"""
from app.visualisation_data.shared.tree_generator import TargetHierarchyTreeGenerator
from app.config import RUN_CONFIG
from app.cache.decorators import return_if_cached_results


@return_if_cached_results({
    'cache_key_generator': lambda: f'target_classifications_organism_taxonomy-{RUN_CONFIG.get("cache_key_suffix")}',
    'timeout': int(3.154e7)
})
def get_classification_tree():
    """
    :return: the organism taxonomy target classification tree
    """

    index_name = 'chembl_organism'
    es_query = {
        "aggs": {
            "children": {
                "terms": {
                    "field": "l1",
                    "size": 100,
                    "order": {
                        "_count": "desc"
                    }
                },
                "aggs": {
                    "children": {
                        "terms": {
                            "field": "l2",
                            "size": 100,
                            "order": {
                                "_count": "desc"
                            }
                        },
                        "aggs": {
                            "children": {
                                "terms": {
                                    "field": "l3",
                                    "size": 100,
                                    "order": {
                                        "_count": "desc"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    def generate_count_query(path_to_node):
        queries = []
        level = 1
        for node in path_to_node:
            queries.append('_metadata.organism_taxonomy.l{level}:("{class_name}")'.format(level=level, class_name=node))
            level += 1

        return ' AND '.join(queries)

    tree_generator = TargetHierarchyTreeGenerator(index_name=index_name, es_query=es_query,
                                                  query_generator=generate_count_query,
                                                  count_index='chembl_target')

    final_tree = tree_generator.get_classification_tree()

    return final_tree
