"""
Module that generates the go slim target classification
"""
from app.visualisation_data.shared.tree_generator import GoSlimTreeGenerator
from app.config import RUN_CONFIG
from app.cache.decorators import return_if_cached_results


@return_if_cached_results({
    'cache_key_generator': lambda: f'target_classifications_go_slim-{RUN_CONFIG.get("cache_key_suffix")}',
    'timeout': int(3.154e7)
})
def get_classification_tree():
    """
    :return: the go slim target classification tree
    """

    tree_generator = GoSlimTreeGenerator()
    final_tree = tree_generator.get_classification_tree()

    return final_tree
