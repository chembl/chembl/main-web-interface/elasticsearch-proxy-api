"""
Module that generates the in vivo assay classification tree
"""
from app.visualisation_data.shared.tree_generator import TargetHierarchyTreeGenerator
from app.config import RUN_CONFIG
from app.cache.decorators import return_if_cached_results


@return_if_cached_results({
    'cache_key_generator': lambda: f'assay_classifications_in_vivo-{RUN_CONFIG.get("cache_key_suffix")}',
    'timeout': int(3.154e7)
})
def get_classification_tree():
    """
    :return: the in vivo classificacion tree.
    """

    index_name = 'chembl_assay_class'
    es_query = {
        "size": 0,
        "aggs": {
            "children": {
                "terms": {
                    "field": "l1",
                    "size": 1000,
                    "order": {
                        "_count": "desc"
                    }
                },
                "aggs": {
                    "children": {
                        "terms": {
                            "field": "l2",
                            "size": 1000,
                            "order": {
                                "_count": "desc"
                            }
                        },
                        "aggs": {
                            "children": {
                                "terms": {
                                    "field": "l3",
                                    "size": 1000,
                                    "order": {
                                        "_count": "desc"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    def generate_count_query(path_to_node):
        queries = []
        level = 1
        for node in path_to_node:
            queries.append('assay_classifications.l{level}:("{class_name}")'.format(level=level,
                                                                                    class_name=node))
            level += 1

        return ' AND '.join(queries)

    tree_generator = TargetHierarchyTreeGenerator(index_name=index_name, es_query=es_query,
                                                  query_generator=generate_count_query,
                                                  count_index='chembl_assay')

    final_tree = tree_generator.get_classification_tree()

    return final_tree
