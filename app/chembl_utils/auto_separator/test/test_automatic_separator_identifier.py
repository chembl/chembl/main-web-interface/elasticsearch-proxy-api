"""
Module to test the automatic separator identifier
"""
import unittest

from app.chembl_utils.auto_separator import separator_identifier


class AutomaticSeparatorTest(unittest.TestCase):
    """
    Class to test the automatic separator identifier
    """

    def test_identifies_separator_1(self):
        """
        Test that it identifies the separator correctly for the most common case
        """
        test_raw_ids = 'CHEMBL3548189,CHEMBL3041241,CHEMBL1198499,CHEMBL1616766,CHEMBL3552333,CHEMBL3040769,' \
                       'CHEMBL1179177,CHEMBL1208242,CHEMBL2105295,CHEMBL1623649,CHEMBL3990326,CHEMBL3551968,' \
                       'CHEMBL3815178,CHEMBL1624921,CHEMBL1621119,CHEMBL1183111,CHEMBL354497,CHEMBL546869,' \
                       'CHEMBL1789284,CHEMBL1739771,CHEMBL3553885,CHEMBL3140231,CHEMBL3991129'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = ','
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_2(self):
        """
        Test that it identifies the separator correctly for the most common case
        """
        test_raw_ids = 'CHEMBL3548189GLADOSCHEMBL3041241GLADOSCHEMBL1198499GLADOSCHEMBL1616766GLADOSCHEMBL3552333' \
                       'GLADOSCHEMBL3040769GLADOSCHEMBL1179177GLADOSCHEMBL1208242GLADOSCHEMBL2105295GLADOS' \
                       'CHEMBL1623649GLADOSCHEMBL3990326GLADOSCHEMBL3551968GLADOSCHEMBL3815178GLADOSCHEMBL1624921' \
                       'GLADOSCHEMBL1621119GLADOSCHEMBL1183111GLADOSCHEMBL354497GLADOSCHEMBL546869GLADOSCHEMBL1789284' \
                       'GLADOSCHEMBL1739771GLADOSCHEMBL3553885GLADOSCHEMBL3140231GLADOSCHEMBL3991129'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = 'GLADOS'
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_3(self):
        """
        Test that it identifies the separator correctly for the most common case
        """
        test_raw_ids = 'CHEMBL3548189;;;;CHEMBL3041241;;;;CHEMBL1198499;;;;CHEMBL1616766;;;;CHEMBL3552333;;;;' \
                       'CHEMBL3040769;;;;CHEMBL1179177;;;;CHEMBL1208242;;;;CHEMBL2105295;;;;CHEMBL1623649;;;;' \
                       'CHEMBL3990326;;;;CHEMBL3551968;;;;CHEMBL3815178;;;;CHEMBL1624921;;;;CHEMBL1621119;;;;' \
                       'CHEMBL1183111;;;;CHEMBL354497;;;;CHEMBL546869;;;;CHEMBL1789284;;;;CHEMBL1739771;;;;' \
                       'CHEMBL3553885;;;;CHEMBL3140231;;;;CHEMBL3991129'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = ';;;;'
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_4(self):
        """
        Test that it identifies the separator correctly for the most common case
        """
        test_raw_ids = 'CHEMBL3548189\r\nCHEMBL3041241\r\nCHEMBL1198499\r\nCHEMBL1616766\r\nCHEMBL3552333\r\n' \
                       'CHEMBL3040769\r\nCHEMBL1179177\r\nCHEMBL1208242\r\nCHEMBL2105295\r\nCHEMBL1623649\r\n' \
                       'CHEMBL3990326\r\nCHEMBL3551968\r\nCHEMBL3815178\r\nCHEMBL1624921\r\nCHEMBL1621119\r\n' \
                       'CHEMBL1183111\r\nCHEMBL354497\r\nCHEMBL546869\r\nCHEMBL1789284\r\nCHEMBL1739771\r\n' \
                       'CHEMBL3553885\r\nCHEMBL3140231\r\nCHEMBL3991129'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = '__NEW_LINE__'
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_5(self):
        """
        Test that it identifies the separator correctly for the most common case
        """
        test_raw_ids = 'CHEMBL3548189,'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = ','
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_6(self):
        """
        Test that it identifies the separator correctly for the most common case
        """
        test_raw_ids = 'CHEMBL3548189,My descent is the story of everyman'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = ',My descent is the story of everyman'
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_7(self):
        """
        Test that it identifies the separator correctly for the most common case
        """
        test_raw_ids = 'CHEMBL3548189'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = ''
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_8(self):
        """
        Test that it identifies the separator correctly when there are no ids at all
        """
        test_raw_ids = 'The warming sun returns again\n' \
                       'And melts away the snow\n' \
                       'The sea is freed from icy chains\n' \
                       'Winter is letting go\n'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = ''
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_9(self):
        """
        Test that it identifies the separator correctly when there are no ids at all
        """
        test_raw_ids = 'CHEMBL1905569CHEMBL3659411'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = ''
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_10(self):
        """
        Test that it identifies the separator correctly when there are no ids at all
        """
        test_raw_ids = 'CHEMBL1905569\tCHEMBL3659411\tCHEMBL1179177'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = '\t'
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_11(self):
        """
        Test that it identifies the separator correctly when there are no ids at all
        """
        test_raw_ids = '\ufeffCHEMBL4250919\nCHEMBL347836\nCHEMBL4247846\nCHEMBL25\nCHEMBL190\nCHEMBL59'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = '__NEW_LINE__'
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')

    def test_identifies_separator_12(self):
        """
        Test that it identifies the separator correctly when there are no ids at all
        """
        test_raw_ids = '\ufeffCHEMBL4250919\ufeff\n\u200cCHEMBL347836\u200c\ufeff\n\u200cCHEMBL4247846\ufeff\u200c\n' \
                       'CHEMBL25\u200c\ufeff\nCHEMBL190\u200c\ufeff\nCHEMBL59'
        parsed_from = 'MOLECULE_CHEMBL_IDS'
        separator_must_be = '__NEW_LINE__'
        separator_got = separator_identifier.get_separator(test_raw_ids, parsed_from)

        self.assertEqual(separator_must_be, separator_got, msg='The separator was not identified correctly!')
