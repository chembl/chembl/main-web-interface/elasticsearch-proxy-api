"""
Module that identifies the separator of ids automatically
"""
import re

SAMPLE_MAX_SIZE = 200
ID_PLACEHOLDER = 'I_AM_STILL_ALIVE'
NEW_LINE_NAME = '__NEW_LINE__'


def get_separator(raw_ids, parsed_from):
    """
    :param raw_ids: a string with the ids as entered by the user
    :param parsed_from: parsed from corresponding to the entered entity
    :return: the separator used in the string with the ids
    """

    print(f'Getting separator automatically for {parsed_from}')
    # the regex is the same for all froms for now
    id_pattern = r'CHEMBL\d+'
    # take a sample to avoid processing huge inputs
    sample = raw_ids
    if len(sample) > SAMPLE_MAX_SIZE:
        sample = sample[:SAMPLE_MAX_SIZE]

    sample = sample.replace('\ufeff', '').replace('\u200c', '')

    sample = re.sub(id_pattern, ID_PLACEHOLDER, sample, count=2)

    separator = get_separator_from_sandwich_match(sample)
    if separator is not None:
        return separator

    # if there was no match, the separator may be new lines
    separator = get_separator_from_sandwich_newlines_match(sample)
    if separator is not None:
        return separator

    # if there was no match, there may be only one id
    separator = get_separator_from_positive_lookbehind(sample)
    if separator is not None:
        return separator

    # ifnothing matches, just default to an empty string
    return ''


def get_separator_from_sandwich_match(sample):
    """
    :param sample: sample text
    :return: return the separator from sandwiching the separator, None if not match.
    """

    pattern = fr'{ID_PLACEHOLDER}(.+){ID_PLACEHOLDER}'
    match = re.match(pattern, sample)
    if match is not None:
        return match.group(1)

    return None


def get_separator_from_sandwich_newlines_match(sample):
    """
    :param sample: sample text
    :return: return the separator from sandwiching the separator using new lines, None if not match.
    """
    pattern = fr'{ID_PLACEHOLDER}(\n|\r)+{ID_PLACEHOLDER}'
    match = re.match(pattern, sample)

    if match is not None:
        return NEW_LINE_NAME

    return None


def get_separator_from_positive_lookbehind(sample):
    """
    :param sample: sample text
    :return: return the separator from doing a positive lookbehind, None if not match.
    """
    pattern = fr'(?<={ID_PLACEHOLDER})'
    match = re.search(pattern, sample)
    if match is None:
        return None

    start = match.start()
    separator = sample[start:]
    if separator == ID_PLACEHOLDER:
        return ''
    return separator
