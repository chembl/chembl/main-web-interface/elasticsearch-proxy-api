"""
Module that handles decorators used in the authorisation of different endpoints.
"""
from functools import wraps
from flask import request, jsonify

import jwt

from app.config import RUN_CONFIG


def special_access_token_required(func):
    """
    Checks that a valid special access token is provided.
    parameter. Makes the function return a 403 http error if the token is missing, 401 if is invalid.
    :param func: function to decorate
    :return: decorated function
    """

    @wraps(func)
    def decorated(*args, **kwargs):

        token = request.headers.get('X-Special-Access-Key')
        key = RUN_CONFIG.get('server_secret_key')

        if token is None:
            return jsonify({'message': 'Token is missing'}), 403

        try:
            token_data = jwt.decode(token, key, algorithms=['HS256'])
            username = token_data.get('username')
            if username != RUN_CONFIG.get('special_access_username'):
                return jsonify({'message': f'You are not authorised for this operation'}), 401
        except:  # pylint: disable=bare-except
            return jsonify({'message': 'Token is invalid'}), 401

        return func(*args, **kwargs)

    return decorated
