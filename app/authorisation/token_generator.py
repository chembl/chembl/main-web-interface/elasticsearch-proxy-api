"""
Module that handles the generation of tokens for the app
"""
import datetime

import jwt

from app.config import RUN_CONFIG

SPECIAL_ACCESS_TOKEN_HOURS_TO_LIVE = 1


def generate_special_access_token():
    """
    Generates a token that can be used to be authorised for spacial access tasks
    :return: JWT token
    """

    token_data = {
        'username': RUN_CONFIG.get('special_access_username'),
        'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=SPECIAL_ACCESS_TOKEN_HOURS_TO_LIVE)
    }

    key = RUN_CONFIG.get('server_secret_key')
    token = jwt.encode(token_data, key).decode('UTF-8')

    return token
