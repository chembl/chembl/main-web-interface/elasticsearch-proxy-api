"""
    Module tht handles the configuration of the groups of properties for the interface
"""
import os

import yaml

from app.config import RUN_CONFIG
from app.properties_configuration import properties_configuration_manager
from app.cache.decorators import return_if_cached_results


class GroupConfiguration:
    """
    Class that handles the configuration of the groups of properties for the interface
    """

    class GroupsConfigurationManagerError(Exception):
        """Base class for exceptions in the groups configuration."""

    def __init__(self, groups_file_path, sorting_file_path, property_configuration_manager):
        """
        :param groups_file_path: path of the yaml file with the groups config of the properties
        :param sorting_file_path: path of the yaml file with the sorting config of the properties
        :param property_configuration_manager: instance of the properties configuration manager
        """
        for path in [groups_file_path, sorting_file_path]:
            if not os.path.isfile(path):
                raise self.GroupsConfigurationManagerError(f'The path {path} does not exist!')

        self.groups_file_path = groups_file_path
        self.sorting_file_path = sorting_file_path
        self.property_configuration_manager = property_configuration_manager

    # ------------------------------------------------------------------------------------------------------------------
    # Getting a custom list of properties
    # ------------------------------------------------------------------------------------------------------------------
    def get_config_for_props_list(self, index_name, prop_ids):
        """
        :param index_name: name of the index
        :param prop_ids: list of ids of the properties to check
        :return: a list of configuration of the properties
        """
        configs = []

        for prop_id in prop_ids:
            configs.append(self.property_configuration_manager.get_config_for_prop(index_name, prop_id))

        return configs

    # ------------------------------------------------------------------------------------------------------------------
    # Getting a group of properties
    # ------------------------------------------------------------------------------------------------------------------
    @return_if_cached_results({
        'cache_key_generator': lambda *args, **kwargs: f'config_for_group-{args[1]}-{args[2]}',
        'timeout': RUN_CONFIG.get('es_mappings_cache_seconds')
    })
    def get_config_for_group(self, index_name, group_name):
        """
        :param index_name: name of the index
        :param group_name: group name as defined in the groups file
        :return: the configuration of the group with the following structure:
        {
            "properties": {
                "default": [...], # properties to show by default
                "optional:" [...] # properties to show as optional for the user
            }
        }
        """

        with open(self.groups_file_path, 'rt') as groups_file:
            groups_config = yaml.load(groups_file, Loader=yaml.FullLoader)

            index_groups = groups_config.get(index_name, {})
            group_config = index_groups.get(group_name)
            if group_config is None:
                raise self.GroupsConfigurationManagerError(
                    f'The group {group_name} does not exist in index {index_name}!')

            props_configs = {}

            for sub_group, props_list in group_config.items():
                props_configs[sub_group] = self.get_config_for_props_list(index_name, props_list)

            config = {'properties': props_configs}

        return config

    # ------------------------------------------------------------------------------------------------------------------
    # Getting a list of properties
    # ------------------------------------------------------------------------------------------------------------------
    @return_if_cached_results({
        'cache_key_generator': lambda *args, **kwargs: f'config_for_list-{args[1]}-{args[2]}',
        'timeout': RUN_CONFIG.get('es_mappings_cache_seconds')
    })
    def get_config_for_list(self, index_name, properties):
        """
        :param index_name: name of the index
        :param properties: text list of the properties separated by comma
        :return: the configuration of the group with the following structure:
        {
            "properties": [] # a list of dicts describing the properties
        }
        """

        props_ids = properties.split(',')
        return self.get_config_for_props_list(index_name, props_ids)

    def get_prop_ids_for_config_group(self, index_name, group_name, subgroups=None):
        """
        :param index_name: name of the index
        :param group_name: group name as defined in the groups file
        :param subgroups: specific subgroups to focus on, if not provided all subgroups are taken into account
        :return: the list of property ids that belong to the group
        """
        prop_ids = set()
        properties_config = self.get_config_for_group(index_name, group_name)

        for subgroup_key, subgroup in properties_config['properties'].items():
            if subgroups is not None:
                if subgroup_key not in subgroups:
                    continue
            for prop_config in subgroup:
                prop_ids.add(prop_config['prop_id'])

        return list(prop_ids)

    def get_prop_ids_for_config_group_by_type(self, index_name, group_name):
        """
        :param index_name: name of the index
        :param group_name: group name as defined in the groups file
        :return: the list of property ids that belong to the group
        """
        properties_config = self.get_config_for_group(index_name, group_name)
        props_by_type = {
            'all': []
        }
        for prop in properties_config['properties']['default']:
            prop_type = prop['type']
            prop_id = prop['prop_id']
            if props_by_type.get(prop_type) is None:
                props_by_type[prop_type] = []
            props_by_type[prop_type].append(prop_id)
            props_by_type['all'].append(prop_id)

        return props_by_type

    @return_if_cached_results({
        'cache_key_generator': lambda *args, **kwargs: f'configured_properties_for_{args[1]}',
        'timeout': RUN_CONFIG.get('es_mappings_cache_seconds')
    })
    def get_list_of_configured_properties(self, index_name):
        """
        :param index_name: the index to check
        :return: a list of all the configured properties among all the groups
        """
        with open(self.groups_file_path, 'rt') as groups_file:
            groups_config = yaml.load(groups_file, Loader=yaml.FullLoader)

            properties_identified = set()
            index_groups = groups_config.get(index_name, {})
            if index_groups is None:
                raise self.GroupsConfigurationManagerError(
                    f'The index {index_name} does not have a configuration set up!')
            for subgroup in index_groups.values():
                for properties_list in subgroup.values():
                    for property_id in properties_list:
                        property_config = self.property_configuration_manager.get_config_for_prop(
                            index_name,
                            property_id
                        )
                        add_property_identifier_to_set(property_config, properties_identified, property_id)

        return list(properties_identified)


def add_property_identifier_to_set(property_config, properties_set, property_id):
    """
    Adds the property id to the set of ids, the final property id can be different if it is virtual
    :param property_config: configuration of the property
    :param properties_set: set to which add the id
    :param property_id: original id of the property
    """

    is_virtual = property_config.get('is_virtual', False)
    is_contextual = property_config.get('is_contextual', False)

    if is_virtual and is_contextual:
        # Ignore virtual contextual properties
        return

    if is_virtual and not is_contextual:
        # If is virtual and non contextual, include the property on which it is based
        based_property_id = property_config.get('based_on')
        properties_set.add(based_property_id)
        return

    if not is_virtual and not is_contextual:
        # Add normal properties
        properties_set.add(property_id)
        return


def get_groups_configuration_instance():
    """
    :return: a default instance for the groups configuration
    """
    property_configuration_manager = properties_configuration_manager.get_property_configuration_instance()

    group_configuration_manager = GroupConfiguration(
        groups_file_path='app/properties_configuration/config/groups.yml',
        sorting_file_path='app/properties_configuration/config/default_sorting.yml',
        property_configuration_manager=property_configuration_manager
    )

    return group_configuration_manager
