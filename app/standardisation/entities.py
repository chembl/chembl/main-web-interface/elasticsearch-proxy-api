"""
Module that contains the standardisation for the entities names, ids and assigned indexes
"""
from enum import Enum


class EntitiesError(Exception):
    """
    Exception for the entities module
    """


class EntityIDs(Enum):
    """
    Enumeration with the possible entity ids
    """
    EUBOPEN_COMPOUND = 'EubopenCompound'
    EUBOPEN_TARGET = 'EubopenTarget'
    EUBOPEN_ACTIVITY = 'EubopenActivity'
    COMPOUND = 'Compound'
    TARGET = 'Target'
    ACTIVITY = 'Activity'
    TISSUE = 'Tissue'
    CELL_LINE = 'CellLine'
    DOCUMENT = 'Document'
    ASSAY = 'Assay'


INDEX_NAMES = {
    EntityIDs.EUBOPEN_COMPOUND: 'chembl_eubopen_molecule',
    EntityIDs.EUBOPEN_TARGET: 'chembl_eubopen_target',
    EntityIDs.EUBOPEN_ACTIVITY: 'chembl_eubopen_activity',
    EntityIDs.ACTIVITY: 'chembl_activity',
    EntityIDs.COMPOUND: 'chembl_molecule',
    EntityIDs.TARGET: 'chembl_target',
    EntityIDs.TISSUE: 'chembl_tissue',
    EntityIDs.CELL_LINE: 'chembl_cell_line',
    EntityIDs.DOCUMENT: 'chembl_document',
    EntityIDs.ASSAY: 'chembl_assay'
}


def get_index_name(entity_id):
    """
    :param entity_id: the entity id to query
    :return: the index name corresponding to the entity id
    """
    parsed_entity_id = EntityIDs(entity_id)
    index_name = INDEX_NAMES.get(parsed_entity_id)

    if index_name is None:
        raise EntitiesError(f'No index name found for the entityID: {entity_id}')

    return index_name
