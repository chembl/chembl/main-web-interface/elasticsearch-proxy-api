# pylint: disable=unexpected-keyword-arg
"""
Module that returns data from elascticsearch
"""
import json
import time
from copy import deepcopy

import elasticsearch
from elasticsearch.helpers import scan
from farmhash import FarmHash128  # pylint: disable=no-name-in-module

from app.es_connection import ES
from app.cache import cache
from app.config import RUN_CONFIG
from app import app_logging
from app.usage_statistics import statistics_saver


class ESDataNotFoundError(Exception):
    """Base class for exceptions in this file."""


def get_es_query_cache_key(index_name, es_query):
    """
    Produces a key to save the data in the cache based on the parameters given
    :param index_name: name of the index to query against
    :param es_query: dict with the query to send
    :return: the key to save the data in the cache
    """
    es_request_digest = get_es_request_digest(es_query)

    return f'{index_name}-{es_request_digest}-{RUN_CONFIG.get("cache_key_suffix")}'


def get_es_response(index_name, es_query, ignore_cache=False, is_test=False):
    """""
    :param index_name: name of the index to query against
    :param es_query: dict with the query to send
    :param ignore_cache: determines if cache must be ignored or not
    :param is_test: tells if the request comes from a test script
    :return: the dict with the response from es
    """

    cache_key = get_es_query_cache_key(index_name, es_query)
    app_logging.debug(f'cache_key: {cache_key}')

    start_time = time.time()

    if not ignore_cache:

        cache_response = cache.fail_proof_get(key=cache_key)
        if cache_response is not None:
            end_time = time.time()
            time_taken = end_time - start_time
            app_logging.debug(f'results were cached')
            if not is_test:
                record_that_response_was_cached(index_name, es_query, time_taken)
            return cache_response

    app_logging.debug(f'results were not cached')

    try:

        start_time = time.time()
        response = ES.search(index=index_name, body=es_query)
        end_time = time.time()
        time_taken = end_time - start_time

        if not is_test:
            record_that_response_not_cached(index_name, es_query, time_taken)

    except elasticsearch.exceptions.ElasticsearchException as error:
        app_logging.error(f'This query caused an error: ')
        app_logging.error(f'index_name:{index_name}')
        app_logging.error(f'es_query:')
        app_logging.error(json.dumps(es_query))
        raise error

    seconds_valid = RUN_CONFIG.get('es_proxy_cache_seconds')

    if not ignore_cache:
        cache.fail_proof_set(key=cache_key, value=response, timeout=seconds_valid)

    return response


def scroll_search(index_name, query, scroll_size=1000):
    """
    :param index_name: name of the index to query against
    :param query: dict with the query to send
    :param scroll_size: size of the scroll
    :return: the yielded batch of results to process
    """

    # Deep copy the query to avoid modifying the original query
    query_copy = deepcopy(query)
    query_copy['size'] = scroll_size

    response = ES.search(index=index_name, body=query_copy, scroll='1m')
    scroll_id = response['_scroll_id']
    hits = response['hits']['hits']

    # Yield the initial batch of results
    yield hits

    # Fetch subsequent batches using the scroll id
    while hits:
        response = ES.scroll(scroll_id=scroll_id, scroll='1m')
        hits = response['hits']['hits']
        yield hits


def get_multisearch_cache_key(body):
    """
    :param body: body of the multisearch
    :return: the key to save the data in the cache
    """

    stable_raw_body = json.dumps(body, sort_keys=True)
    return f'multisearch-{stable_raw_body}-{RUN_CONFIG.get("cache_key_suffix")}'


def do_multisearch(body):
    """
    :param body: body of the multisearch
    :return: the result of the multisearch
    """
    cache_key = get_multisearch_cache_key(body)
    app_logging.debug(f'cache_key: {cache_key}')

    start_time = time.time()
    cache_response = cache.fail_proof_get(key=cache_key)
    if cache_response is not None:
        end_time = time.time()
        time_taken = end_time - start_time
        app_logging.debug(f'results were cached')
        record_that_response_was_cached('multisearch', {'query': body}, time_taken)
        return cache_response

    app_logging.debug(f'results were not cached')

    start_time = time.time()
    result = ES.msearch(body=body)
    end_time = time.time()
    time_taken = end_time - start_time

    record_that_response_not_cached('multisearch', {'query': body}, time_taken)

    seconds_valid = RUN_CONFIG.get('es_proxy_cache_seconds')
    cache.fail_proof_set(key=cache_key, value=result, timeout=seconds_valid)

    return result


def get_es_doc(index_name, doc_id, source=None):
    """
    :param index_name: name of the intex to which the document belongs
    :param doc_id: id of the document
    :param source: list of properties to get
    :return: the dict with the response from es corresponding to the document
    """
    properties_list = [] if source is None else source.split(',')
    using_source = source is not None
    cache_key = f'document-{index_name}-{doc_id}-source:{",".join(properties_list)}-{RUN_CONFIG.get("cache_key_suffix")}'
    app_logging.debug(f'cache_key: {cache_key}')

    equivalent_query = {
        "query": {
            "ids": {
                "values": doc_id
            }
        }
    }
    if using_source:
        equivalent_query['_source'] = properties_list

    start_time = time.time()
    cache_response = cache.fail_proof_get(key=cache_key)
    if cache_response is not None:
        end_time = time.time()
        time_taken = end_time - start_time
        app_logging.debug(f'results were cached')
        record_that_response_was_cached(index_name, equivalent_query, time_taken)
        return cache_response

    app_logging.debug(f'results were not cached')

    try:
        start_time = time.time()
        source_param = properties_list if using_source else True  # True means, get all fields
        response = ES.get(index=index_name, id=doc_id, _source=source_param)  # pylint: disable=unexpected-keyword-arg
        end_time = time.time()
        time_taken = end_time - start_time

        record_that_response_not_cached(index_name, equivalent_query, time_taken)
    except elasticsearch.exceptions.NotFoundError as error:
        raise ESDataNotFoundError(repr(error))

    seconds_valid = RUN_CONFIG.get('es_proxy_cache_seconds')
    cache.fail_proof_set(key=cache_key, value=response, timeout=seconds_valid)

    return response


def save_es_doc(index_name, document, **kwargs):
    """
    Saves to elasticsearch the document at the index indicated by parameter
    :param index_name: index in which to save the document
    :param document: document to save
    :param kwargs: keyword args to pass to the index function:
    https://elasticsearch-py.readthedocs.io/en/master/api.html#elasticsearch.Elasticsearch.index
    """
    app_logging.debug(f'Saving the document {document} to the index {index_name}')
    result = ES.index(index=index_name, body=document, doc_type='_doc', **kwargs)
    app_logging.debug(f'Result {result}')


def update_es_doc(index_name, updated_fields, doc_id):
    """
    Updates the document with the document provided
    :param doc_id: id of the document to update
    :param index_name: index where to save the document
    :param updated_fields: updated fields of the document to save
    """
    app_logging.debug(f'Updating the document with id {doc_id} with this {updated_fields} on the index {index_name}')
    result = ES.update(index=index_name, body=updated_fields, doc_type='_doc', id=doc_id, retry_on_conflict=10)
    app_logging.debug(f'Result {result}')


def get_es_request_digest(es_query):
    """
    :param es_query: query sent to elasticsearch
    :return: a digest of the query
    """
    stable_raw_search_data = json.dumps(es_query, sort_keys=True)
    search_data_hash = f'{FarmHash128(stable_raw_search_data)}'

    return search_data_hash


def record_that_response_was_cached(index_name, es_query, time_taken):
    """
    Records that a response was already cached
    :param index_name: name of the index queried
    :param es_query: the query sent
    :param time_taken: time taken (seconds) to get the data
    """
    es_request_digest = get_es_request_digest(es_query)
    is_cached = True
    statistics_saver.save_index_usage_record(index_name, es_query, es_request_digest, is_cached, time_taken)


def record_that_response_not_cached(index_name, es_query, time_taken):
    """
    Records that a response was not cached
    :param index_name: name of the index queried
    :param es_query: the query sent
    :param time_taken: time taken (seconds) to get the data
    """
    es_request_digest = get_es_request_digest(es_query)
    is_cached = False
    statistics_saver.save_index_usage_record(index_name, es_query, es_request_digest, is_cached, time_taken)


def get_es_scanner(index_name, ids_query):
    """
    :param index_name: name of the index to query
    :param ids_query: query to get the ids
    :return: a elasticsearch scanner for the query and the parameters given
    """

    return scan(
        ES,
        index=index_name,
        scroll=u'1m',
        size=1000,
        request_timeout=60,
        query=ids_query
    )
